webpackJsonp([0],{

/***/ 14:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalController; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/* import { AppConfig } from './appconfig'; */
var GlobalController = /** @class */ (function () {
    function GlobalController(toast, loading) {
        this.toast = toast;
        this.loading = loading;
    }
    GlobalController.prototype.toaster = function (msg) {
        var toast = this.toast.create({
            message: msg,
            duration: 3000,
            position: 'top',
            cssClass: 'dark-trans',
            closeButtonText: 'OK',
            showCloseButton: true
        });
        toast.present();
    };
    GlobalController.prototype.loader = function () {
        var loader = this.loading.create({
            content: "Tunggu Sebentar",
            duration: 3000
        });
        loader.present();
    };
    GlobalController.prototype.getComponentInitial = function () {
        if (localStorage.getItem('component')) {
            return localStorage.getItem('component');
        }
        else {
            return "Home";
        }
    };
    GlobalController.prototype.getMonth = function (init) {
        if (typeof init == 'string') {
            var month = {
                "01": "Januari",
                "02": "Februari",
                "03": "Maret",
                "04": "April",
                "05": "Mei",
                "06": "Juni",
                "07": "Juli",
                "08": "Agustus",
                "09": "September",
                "10": "Oktober",
                "11": "November",
                "12": "Desember",
            };
            return month[init];
        }
        else {
            var month2 = [];
            month2 = [
                "Januari",
                "Februari",
                "Maret",
                "April",
                "Mei",
                "Juni",
                "Juli",
                "Agustus",
                "September",
                "Oktober",
                "November",
                "Desember",
            ];
            return month2[init];
        }
    };
    GlobalController.prototype.rangeCreator = function (month1, month2) {
        if (month1 === void 0) { month1 = 1; }
        if (month2 === void 0) { month2 = 1; }
        var range = [];
        for (var i = month1; i <= month2; i++) {
            var obj = {
                month: this.getMonth((i - 1)),
                init: i
            };
            range.push(obj);
        }
        return range;
    };
    GlobalController.prototype.setMonth = function (str) {
        var actualMonth = str + 1;
        if (actualMonth.toString().length == 1) {
            actualMonth = '0' + actualMonth.toString();
        }
        return actualMonth;
    };
    GlobalController = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */]])
    ], GlobalController);
    return GlobalController;
}());

//# sourceMappingURL=globalController.js.map

/***/ }),

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WeatherProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { api } from './config';


var WeatherProvider = /** @class */ (function () {
    function WeatherProvider(http) {
        this.http = http;
        this.apiKey = '1e4a0bdb251c64e4';
        console.log('Hello WeatherProvider Provider');
        this.url = 'http://api.wunderground.com/api/' + this.apiKey + '/conditions/q/';
    }
    WeatherProvider.prototype.getWeather = function (state, city) {
        return this.http.get(this.url + state + '/' + city + '.json').pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["map"])(this.extractData), Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["catchError"])(this.handleError));
    };
    // Private
    WeatherProvider.prototype.extractData = function (res) {
        var body = res;
        return body || {};
    };
    WeatherProvider.prototype.handleError = function (error) {
        var errMsg;
        if (error instanceof Response) {
            var err = error || '';
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].throw(errMsg);
    };
    WeatherProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], WeatherProvider);
    return WeatherProvider;
}());

//# sourceMappingURL=weather.js.map

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormUserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_data_services__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_globalController__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_user_user__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var FormUserPage = /** @class */ (function () {
    function FormUserPage(nav, navParams, formBuilder, http, popoverCtrl, data, global) {
        this.nav = nav;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.data = data;
        this.global = global;
        this.formInstance = [];
        this.dataStaff = [];
        this.id = "";
        this.role = "";
        this.getPerson();
        this.getStaff();
        this.formObj();
        this.title = ((typeof this.navParams.data === 'string') ? 'Ubah User' : 'Tambah User');
        this.nama = localStorage.getItem('name');
        this.form = this.formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            nama: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            nik: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            telp: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            pass: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            confirm_pass: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            birth: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            address: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            role: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            disposisi: ['']
        });
    }
    FormUserPage.prototype.formObj = function () {
        this.formInstance = [
            { label: 'Username', icon: 'at', type: 'email', formCName: 'username' },
            { label: 'Nama', icon: 'person', type: 'text', formCName: 'nama' },
            { label: 'NIK', icon: 'card', type: 'text', formCName: 'nik' },
            { label: 'No Handphone', icon: 'call', type: 'text', formCName: 'telp' },
            { label: 'Password', icon: 'key', type: 'password', formCName: 'pass' },
            { label: 'Confirm Password', icon: 'key', type: 'password', formCName: 'confirm_pass' }
        ];
        if (typeof this.navParams.data === 'string') {
            this.formInstance.splice(4, 2);
            console.log(this.formInstance);
        }
    };
    FormUserPage.prototype.getPerson = function () {
        var _this = this;
        var json;
        console.log(this.navParams.data);
        if (typeof this.navParams.data === 'string') {
            var url = 'http://localhost/api-v1/request/get_person/' + this.navParams.data;
            var remoteurl = 'http://bms.uphero.com/api-v1/request/get_person/' + this.navParams.data;
            this.http.get(remoteurl)
                .map(function (res) {
                //this.global.loader();
                json = res.json();
                _this.id = json.data.user_id;
                _this.form.controls['username'].setValue(json.data.username);
                _this.form.controls['nama'].setValue(json.data.nama);
                _this.form.controls['nik'].setValue(json.data.nik);
                _this.form.controls['telp'].setValue(json.data.telp);
                _this.form.controls['birth'].setValue(json.data.tgl_lahir);
                _this.form.controls['address'].setValue(json.data.alamat);
                _this.form.controls['role'].setValue(json.data.id_jabatan);
                _this.form.controls['disposisi'].setValue(json.data.id_staff);
            })
                .subscribe(function (data) { }, function (error) {
                var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
                _this.global.toaster(text);
            });
        }
    };
    FormUserPage.prototype.checkRole = function () {
        this.role = this.form.value['role'];
    };
    FormUserPage.prototype.getStaff = function () {
        var _this = this;
        var json;
        var url = 'http://localhost/api-v1/request/get_staff';
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_staff';
        this.http.get(remoteurl)
            .map(function (res) {
            json = res.json();
            _this.dataStaff = json.data;
            console.log(_this.dataStaff);
        })
            .subscribe(function (data) { }, function (error) {
            console.log(error);
        });
    };
    /* ionViewWillEnter() {
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then((val) => {
            if (val === null) {
                this.search.name = "Rio de Janeiro, Brazil"
            } else {
                this.search.name = val;
            }
        }).catch((err) => {
            console.log(err)
        });
    } */
    FormUserPage.prototype.post_user = function () {
        var _this = this;
        var url = 'http://localhost/api-v1/request/post_user' + ((typeof this.navParams.data === 'string') ? '/' + this.navParams.data : '');
        var remoteurl = 'http://bms.uphero.com/api-v1/request/post_user' + ((typeof this.navParams.data === 'string') ? '/' + this.navParams.data : '');
        this.http.post(remoteurl, JSON.stringify(this.form.value))
            .timeout(3000)
            .map(function (data) {
            _this.global.loader();
            var res = JSON.parse(data['_body']);
            if (res.status) {
                setTimeout(function () {
                    _this.global.toaster(res.message);
                }, 2000);
                setTimeout(function () {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_user_user__["a" /* userPage */]);
                }, 4000);
            }
            else {
                setTimeout(function () {
                    _this.global.toaster(res.message);
                }, 2000);
            }
        })
            .subscribe(function (data) {
        }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    FormUserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-form-user',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\form-user\form-user.html"*/'<!-- -->\n\n<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>\n\n            {{title}}\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="animated fadeIn common-bg">\n\n    \n\n    <form class="list-form" [formGroup]="form">\n\n        <ion-item *ngFor="let form of formInstance; let i=index">\n\n            <ion-label floating>\n\n                <ion-icon [name]="form.icon" item-start class="text-primary"></ion-icon>\n\n                {{form.label}}\n\n            </ion-label>\n\n            <ion-input [type]="form.type" [formControlName]="form.formCName"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>\n\n                <ion-icon name="calendar" item-start class="text-primary"></ion-icon>\n\n                Tanggal Lahir\n\n            </ion-label>\n\n            <ion-datetime displayFormat="DD/MMMM/YYYY" formControlName="birth"></ion-datetime>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>\n\n                <ion-icon name="card" item-start class="text-primary"></ion-icon>\n\n                Alamat\n\n            </ion-label>\n\n            <ion-textarea formControlName="address">{{form.role}}</ion-textarea>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>\n\n                <ion-icon name="people" item-start class="text-primary"></ion-icon>\n\n                Jabatan pada aplikasi\n\n            </ion-label>\n\n            <ion-select formControlName="role" (ionChange)="checkRole()">\n\n                <ion-option value="4">Pimpinan</ion-option>\n\n                <ion-option value="3">Staff</ion-option>\n\n                <ion-option value="2">Petugas</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n        <ion-item *ngIf="(id && id == 2) || (role && role == 2)">\n\n            <ion-label>\n\n                <ion-icon name="people" item-start class="text-primary"></ion-icon>\n\n                Staf Penilai\n\n            </ion-label>\n\n            <ion-select formControlName="disposisi">\n\n                <div *ngFor="let staff of dataStaff">\n\n                    <ion-option value="{{staff.user_id}}">{{staff.nama}}</ion-option>\n\n                </div>\n\n            </ion-select>\n\n        </ion-item>\n\n        \n\n        <button *ngIf="!id" ion-button icon-start block color="danger" tappable (click)="post_user()" [disabled]="!form.valid"><ion-icon name="send"></ion-icon>\n\n      Simpan</button>\n\n        <button *ngIf="id" ion-button icon-start block color="danger" tappable (click)="post_user()"><ion-icon name="send"></ion-icon>\n\n      Simpan</button>\n\n    </form>\n\n    \n\n</ion-content>'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\form-user\form-user.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_6__services_data_services__["a" /* DataServices */], __WEBPACK_IMPORTED_MODULE_7__app_globalController__["a" /* GlobalController */]])
    ], FormUserPage);
    return FormUserPage;
}());

//# sourceMappingURL=form-user.js.map

/***/ }),

/***/ 146:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormKinerjaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_data_services__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__notifications_notifications__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__settings_settings__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_globalController__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_nilai_nilai__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__trips_trips__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__search_location_search_location__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var FormKinerjaPage = /** @class */ (function () {
    function FormKinerjaPage(storage, navParams, nav, http, popoverCtrl, data, global) {
        this.storage = storage;
        this.navParams = navParams;
        this.nav = nav;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.data = data;
        this.global = global;
        // search condition
        this.search = {
            name: "Rio de Janeiro, Brazil",
            date: new Date().toISOString()
        };
        this.push = {};
        this.getKriteria();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        this.idPost = this.navParams.data;
    }
    FormKinerjaPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then(function (val) {
            if (val === null) {
                _this.search.name = "Rio de Janeiro, Brazil";
            }
            else {
                _this.search.name = val;
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    FormKinerjaPage.prototype.getKriteria = function () {
        var _this = this;
        var json;
        var params = this.navParams.data.replace('==', '').replace('=', '');
        var url = 'http://localhost/api-v1/request/get_kriteria/' + params;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_kriteria/' + params;
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            _this.kriteriaNilai = json.data.data;
            _this.result = json.data.result;
            if (json.data.dataEdit) {
                /* push to array ngModel */
                _this.push['K3'] = json.data.dataEdit.K3;
                _this.push['DIS'] = json.data.dataEdit.DIS;
                _this.push['PRK'] = json.data.dataEdit.PRK;
                _this.push['TMW'] = json.data.dataEdit.TMW;
                _this.push['ATT'] = json.data.dataEdit.ATT;
                var decode = atob(_this.navParams.data);
                _this.idPost = btoa(decode + '|' + json.data.dataEdit.nilai_id).replace('=', '');
            }
        })
            .subscribe(function (data) { }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    FormKinerjaPage.prototype.valueMean = function (event, criteria) {
        console.log(this.push);
        criteria.default = criteria.detail[event['_value'] - 1].keterangan;
        //criteria.result = criteria.detail[event['_value']-1].angka;
        this.result[criteria.id_kriteria] = criteria.detail[event['_value'] - 1].angka;
    };
    FormKinerjaPage.prototype.post_nilai = function () {
        var _this = this;
        if (typeof this.navParams.data === 'string') {
            var params = this.idPost.replace('==', '').replace('=', '');
            var url = 'http://localhost/api-v1/request/post_nilai/' + params + '/' + localStorage.getItem('user_id');
            var remoteurl = 'http://bms.uphero.com/api-v1/request/post_nilai/' + params + '/' + localStorage.getItem('user_id');
            this.http.post(remoteurl, JSON.stringify(this.push))
                .timeout(3000)
                .map(function (data) {
                _this.global.loader();
                var res = JSON.parse(data['_body']);
                if (res.status) {
                    setTimeout(function () {
                        _this.global.toaster(res.message);
                    }, 2000);
                    setTimeout(function () {
                        _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_10__pages_nilai_nilai__["a" /* nilaiPage */]);
                    }, 4000);
                }
                else {
                    setTimeout(function () {
                        _this.global.toaster(res.message);
                    }, 2000);
                }
            })
                .subscribe(function (data) {
            }, function (error) {
                var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
                _this.global.toaster(text);
            });
        }
        else {
            this.global.toaster("error with id is null");
        }
    };
    // go to result page
    FormKinerjaPage.prototype.doSearch = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_11__trips_trips__["a" /* TripsPage */]);
    };
    // choose place
    FormKinerjaPage.prototype.choosePlace = function (from) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_12__search_location_search_location__["a" /* SearchLocationPage */], from);
    };
    // to go account page
    FormKinerjaPage.prototype.goToAccount = function (id) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_8__settings_settings__["a" /* SettingsPage */], id);
    };
    FormKinerjaPage.prototype.presentNotifications = function (myEvent) {
        console.log(myEvent);
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_7__notifications_notifications__["a" /* NotificationsPage */]);
        popover.present({
            ev: myEvent
        });
    };
    FormKinerjaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-form-kinerja',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\form-kinerja\form-kinerja.html"*/'<!-- -->\n\n<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>\n\n            {{title}}\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="animated fadeIn common-bg">\n\n    \n\n    <ion-card *ngFor="let kriteria of kriteriaNilai">\n\n        <ion-card-header>\n\n            {{kriteria.nama_kriteria}}\n\n        </ion-card-header>\n\n        \n\n        <ion-item>\n\n            <ion-range min="1" [max]="kriteria.max_range" pin="true" [(ngModel)]="push[kriteria.const_kriteria]" (ionChange)="valueMean($event, kriteria)" color="secondary">\n\n                <ion-icon range-left name="sad"></ion-icon>\n\n                <ion-icon range-right name="happy"></ion-icon>\n\n            </ion-range>\n\n        </ion-item>\n\n\n\n        <ion-card-content>\n\n            <p>{{kriteria.default}}</p>\n\n        </ion-card-content>\n\n    </ion-card>\n\n    <button ion-button icon-start block color="secondary" tappable (click)="post_nilai()"><ion-icon name="log-in"></ion-icon>\n\n      Simpan</button>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\form-kinerja\form-kinerja.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_6__services_data_services__["a" /* DataServices */], __WEBPACK_IMPORTED_MODULE_9__app_globalController__["a" /* GlobalController */]])
    ], FormKinerjaPage);
    return FormKinerjaPage;
}());

//
//# sourceMappingURL=form-kinerja.js.map

/***/ }),

/***/ 147:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormRekapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_data_services__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__notifications_notifications__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__settings_settings__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__detail_rekap_detail_rekap__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_globalController__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__trips_trips__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__search_location_search_location__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var FormRekapPage = /** @class */ (function () {
    function FormRekapPage(storage, navParams, nav, http, popoverCtrl, data, global) {
        this.storage = storage;
        this.navParams = navParams;
        this.nav = nav;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.data = data;
        this.global = global;
        // search condition
        this.search = {
            name: "Rio de Janeiro, Brazil",
            date: new Date().toISOString()
        };
        this.formdata = {};
        this.arrUser = this.data.dataUser();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
    }
    FormRekapPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then(function (val) {
            if (val === null) {
                _this.search.name = "Rio de Janeiro, Brazil";
            }
            else {
                _this.search.name = val;
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    FormRekapPage.prototype.getKriteria = function () {
        var _this = this;
        var json;
        this.http.get('http://localhost/api-v1/request/get_kriteria')
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            _this.kriteriaNilai = json.data.data;
            _this.result = json.data.result;
        })
            .subscribe(function (data) { }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    FormRekapPage.prototype.getListOfficer = function () {
        if (this.date_from && this.date_to) {
            if (localStorage.getItem('user_id') == '2') {
                var params = btoa(this.date_from + '|' + this.date_to + '|' + localStorage.getItem('user_id')).replace('=', '');
            }
            else {
                var params = btoa(this.date_from + '|' + this.date_to + '|' + this.navParams.data).replace('=', '');
            }
            this.nav.push(__WEBPACK_IMPORTED_MODULE_9__detail_rekap_detail_rekap__["a" /* DetailRekapPage */], params);
        }
        else {
            this.global.toaster("Please Choose date first");
        }
    };
    FormRekapPage.prototype.post_nilai = function () {
        var _this = this;
        if (typeof this.navParams.data === 'string') {
            var url = 'http://localhost/api-v1/request/post_nilai/' + this.navParams.data;
            var remoteurl = 'http://bms.uphero.com/api-v1/request/post_nilai/' + this.navParams.data;
            this.http.post(remoteurl, JSON.stringify(this.result))
                .timeout(3000)
                .map(function (data) {
                _this.global.loader();
                var res = JSON.parse(data['_body']);
            })
                .subscribe(function (data) {
            }, function (error) {
                var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
                _this.global.toaster(text);
            });
        }
        else {
            this.global.toaster("error with id is null");
        }
    };
    // go to result page
    FormRekapPage.prototype.doSearch = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_11__trips_trips__["a" /* TripsPage */]);
    };
    // choose place
    FormRekapPage.prototype.choosePlace = function (from) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_12__search_location_search_location__["a" /* SearchLocationPage */], from);
    };
    // to go account page
    FormRekapPage.prototype.goToAccount = function (id) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_8__settings_settings__["a" /* SettingsPage */], id);
    };
    FormRekapPage.prototype.presentNotifications = function (myEvent) {
        console.log(myEvent);
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_7__notifications_notifications__["a" /* NotificationsPage */]);
        popover.present({
            ev: myEvent
        });
    };
    FormRekapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-form-rekap',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\form-rekap\form-rekap.html"*/'<!-- -->\n\n<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>\n\n            {{title}}\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="animated fadeIn common-bg">\n\n    <ion-card>\n\n        <ion-item>\n\n            \n\n            <ion-label floating>\n\n                <ion-icon name="calendar" item-start class="text-primary"></ion-icon>\n\n                Dari Bulan\n\n            </ion-label>\n\n            <ion-datetime displayFormat="MMMM YYYY" [(ngModel)]="date_from"></ion-datetime>\n\n        </ion-item>\n\n        <ion-item>\n\n            s/d\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>\n\n                <ion-icon name="calendar" item-end class="text-primary"></ion-icon>\n\n                Sampai Bulan\n\n            </ion-label>\n\n            <ion-datetime displayFormat="MMMM YYYY" [(ngModel)]="date_to"></ion-datetime>\n\n        </ion-item>\n\n    </ion-card>\n\n    \n\n    <button ion-button icon-start block color="secondary" tappable (click)="getListOfficer()">\n\n        Lanjut \n\n        <ion-icon name="arrow-forward"></ion-icon>\n\n      </button>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\form-rekap\form-rekap.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_6__services_data_services__["a" /* DataServices */], __WEBPACK_IMPORTED_MODULE_10__app_globalController__["a" /* GlobalController */]])
    ], FormRekapPage);
    return FormRekapPage;
}());

//
//# sourceMappingURL=form-rekap.js.map

/***/ }),

/***/ 148:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPetugasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__detail_info_detail_info__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_globalController__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ListPetugasPage = /** @class */ (function () {
    function ListPetugasPage(storage, navParams, nav, http, popoverCtrl, global) {
        this.storage = storage;
        this.navParams = navParams;
        this.nav = nav;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.global = global;
        this.page = 1;
        this.perPage = 0;
        this.totalData = 0;
        this.totalPage = 0;
        this.officerList = [];
        this.getOfficers();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        this.param = JSON.parse(atob(this.navParams.data));
        var date = this.param.date_nilai.split('-');
        this.month = this.global.getMonth(date[1]);
    }
    ListPetugasPage.prototype.getOfficers = function (q) {
        var _this = this;
        if (q === void 0) { q = ''; }
        var json;
        var params = this.navParams.data.replace(new RegExp('=', 'g'), "");
        var url = 'http://localhost/api-v1/request/get_officers_param/' + params + '/' + q;
        //var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers_param/' + params +'/'+ q;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers_param_page?param=' + params + '&query=' + q + '&id_staff=' + localStorage.getItem('user_id');
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            _this.officerList = json.data.data;
            _this.totalData = json.data.total_rows;
            _this.totalPage = json.data.total_page;
        })
            .subscribe(function (data) { }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    ListPetugasPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var json;
        this.page = this.page + 1;
        var params = this.navParams.data.replace(new RegExp('=', 'g'), "");
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers_param_page?param=' + params + '&page=' + this.page + '&id_staff=' + localStorage.getItem('user_id');
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            //console.log(json.data.total_page);
            for (var i = 0; i < json.data.data.length; i++) {
                _this.officerList.push(json.data.data[i]);
            }
            console.log(_this.page);
            console.log(_this.totalPage);
            _this.totalData = json.data.total_rows;
            _this.totalPage = json.data.total_page;
            setTimeout(function () {
                console.log('Async operation has ended');
                infiniteScroll.complete();
            }, 2000);
        })
            .subscribe(function (data) { }, function (error) {
            //this.global.toaster(error);
            console.log(error);
        });
    };
    ListPetugasPage.prototype.searchTerm = function () {
        console.log(this.q);
        if (this.q) {
            this.getOfficers(this.q);
        }
        else {
            this.getOfficers();
        }
    };
    // to go account page
    ListPetugasPage.prototype.goToView = function (id) {
        this.param.id = id;
        var params = btoa(JSON.stringify(this.param)).replace(new RegExp('=', 'g'), "");
        this.nav.push(__WEBPACK_IMPORTED_MODULE_6__detail_info_detail_info__["a" /* DetailInfoPage */], params);
    };
    ListPetugasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list-petugas',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\list-petugas\list-petugas.html"*/'<!-- -->\n\n<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>\n\n            {{title}}\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="animated fadeIn common-bg">\n\n\n\n    <ion-list>\n\n        <ion-card>\n\n            <ion-item>\n\n                <ion-icon name="search" item-start class="text-primary"></ion-icon>\n\n                <ion-input type="search" [(ngModel)]="q" (ionChange)="searchTerm()"></ion-input>\n\n            </ion-item>\n\n        </ion-card>\n\n        <ion-card>\n\n            <ion-item>\n\n                <h5>Peringkat Penilaian Kinerja Bulan {{month}}</h5>\n\n            </ion-item>\n\n        </ion-card>\n\n        <ion-card no-margin margin-bottom class="full-width" *ngFor="let user of officerList; let i = index" (click)="goToView(user.user_id)">\n\n            <ion-item>\n\n                <h2><strong>{{i+1}}. {{user.nama}}</strong></h2>\n\n                <h4>{{user.nik}}</h4>\n\n                <h5>Contact Person: {{user.telp}}</h5>\n\n                <span><ion-icon name="pie"></ion-icon> <small>Penilaian Bulan {{month}}: </small><strong>{{(user.total)?user.total:\'-\'}}</strong></span>\n\n                <h5><ion-icon name="person"></ion-icon> <small>Penilai: </small><strong>{{(user.nama_staff)?user.nama_staff:\'-\'}}</strong></h5>\n\n                <button ion-button clear item-end>Detail</button>\n\n            </ion-item>\n\n        </ion-card>\n\n    </ion-list>\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)" *ngIf="page < totalPage">\n\n        <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data..."></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\list-petugas\list-petugas.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_7__app_globalController__["a" /* GlobalController */]])
    ], ListPetugasPage);
    return ListPetugasPage;
}());

//
//# sourceMappingURL=list-petugas.js.map

/***/ }),

/***/ 158:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 158;

/***/ }),

/***/ 202:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 202;

/***/ }),

/***/ 25:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataServices; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DataServices = /** @class */ (function () {
    function DataServices() {
    }
    DataServices.prototype.dataUser = function () {
        return [
            { id: 1, name: 'Bima Jaya', sex: 'L', address: 'Jl. Merdeka no 17' },
            { id: 2, name: 'Andre Taulany', sex: 'L', address: 'Jl. Maju mundur no 13' }
        ];
    };
    DataServices = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], DataServices);
    return DataServices;
}());

//# sourceMappingURL=data-services.js.map

/***/ }),

/***/ 264:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TripDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_trip_service__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__checkout_trip_checkout_trip__ = __webpack_require__(265);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TripDetailPage = /** @class */ (function () {
    function TripDetailPage(nav, tripService) {
        this.nav = nav;
        this.tripService = tripService;
        // number of adult
        this.adults = 2;
        // number of children
        this.children = 0;
        // set sample data
        this.trip = tripService.getItem(1);
    }
    // minus adult when click minus button
    TripDetailPage.prototype.minusAdult = function () {
        this.adults--;
    };
    // plus adult when click plus button
    TripDetailPage.prototype.plusAdult = function () {
        this.adults++;
    };
    // minus children when click minus button
    TripDetailPage.prototype.minusChildren = function () {
        this.children--;
    };
    // plus children when click plus button
    TripDetailPage.prototype.plusChildren = function () {
        this.children++;
    };
    // go to checkout page
    TripDetailPage.prototype.checkout = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_3__checkout_trip_checkout_trip__["a" /* CheckoutTripPage */]);
    };
    TripDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-trip-detail',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\trip-detail\trip-detail.html"*/'<!-- -->\n\n<ion-header>\n\n\n\n  <ion-navbar  color="primary">\n\n    <ion-title>\n\n      <span ion-text>{{ trip.name }}</span>\n\n    </ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content class="common-bg">\n\n  <!--slides-->\n\n  <ion-slides class="to-top" pager>\n\n    <ion-slide *ngFor="let image of trip.images">\n\n      <img [src]="image" alt="">\n\n    </ion-slide>\n\n  </ion-slides>\n\n\n\n  <!--services-->\n\n  <ion-grid class="border-bottom dark-bg">\n\n    <ion-row>\n\n      <ion-col text-center>\n\n        <div class="text-sm">\n\n          <div>\n\n            <ion-icon name="time" class="text-white"></ion-icon>\n\n            <span ion-text color="light">{{ trip.time }}</span>\n\n            <ion-icon name="checkbox-outline" margin-left class="text-white" *ngIf="trip.free_cancellation"></ion-icon>\n\n            <span ion-text color="light" *ngIf="trip.free_cancellation">Free cancellation</span>\n\n            <ion-icon name="list-box" margin-left class="text-white" *ngIf="trip.electric_voucher"></ion-icon>\n\n            <span ion-text color="light" *ngIf="trip.electric_voucher">Electronic voucher</span>\n\n          </div>\n\n        </div>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n  <!--high light-->\n\n  <div class="border-bottom" padding>\n\n    <span ion-text color="dark" class="bold">HIGHLIGHT</span>\n\n    <ul class="highlight">\n\n      <li *ngFor="let highlight of trip.highlights">\n\n        <ion-icon name="checkmark" class="text-green"></ion-icon>\n\n        <span ion-text color="primary">{{ highlight }}</span>\n\n      </li>\n\n    </ul>\n\n  </div>\n\n\n\n  <!--booking form-->\n\n  <div class="booking-form card round" margin>\n\n    <div class="border-bottom" padding>\n\n      <h5>{{ trip.sub_name }}</h5>\n\n\n\n      <!--choose guest-->\n\n      <ion-grid class="filters" no-padding margin-top>\n\n        <ion-row>\n\n          <ion-col class="adult" width-70>\n\n            <span ion-text color="primary"><strong>{{ trip.price_adult | currency:\'USD\':true }}</strong> Adults</span>\n\n          </ion-col>\n\n          <ion-col width-10 text-center>\n\n            <ion-icon name="remove-circle" class="text-2x" tappable (click)="minusAdult()" [hidden]="adults < 2"\n\n                      color="secondary"></ion-icon>\n\n          </ion-col>\n\n          <ion-col width-10 text-center>{{ adults }}</ion-col>\n\n          <ion-col width-10 text-center>\n\n            <ion-icon name="add-circle" class="text-2x" tappable (click)="plusAdult()" color="secondary"></ion-icon>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row margin-top>\n\n          <ion-col width-70>\n\n            <span ion-text color="primary"><strong>{{ trip.price_child | currency:\'USD\':true }}</strong> Child (0-12 years)</span>\n\n          </ion-col>\n\n          <ion-col width-10 text-center>\n\n            <ion-icon name="remove-circle" class="text-2x" tappable (click)="minusChildren()" [hidden]="children < 1"\n\n                      color="secondary"></ion-icon>\n\n          </ion-col>\n\n          <ion-col width-10 text-center>{{ children }}</ion-col>\n\n          <ion-col width-10 text-center>\n\n            <ion-icon name="add-circle" class="text-2x" tappable (click)="plusChildren()" color="secondary"></ion-icon>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n    </div>\n\n    <div padding class="form-bottom">\n\n<!--       <span ion-text color="dark" class="bold">{{ adults }} Adults</span> -->\n\n      <!--booking button-->\n\n      <button ion-button class="pull-right" color="secondary" tappable (click)="checkout()">Book Now {{ adults * trip.price_adult +\n\n        children * trip.price_child | currency:\'USD\':true }}\n\n      </button>\n\n      <div class="clear"></div>\n\n    </div>\n\n  </div>\n\n\n\n  <!--description-->\n\n  <div class="border-bottom" padding>\n\n    <span ion-text color="primary" class="bold">DESCRIPTION</span>\n\n    <p ion-text>{{ trip.description }}</p>\n\n  </div>\n\n\n\n  <!--address-->\n\n  <div class="border-bottom" padding>\n\n    <span ion-text color="primary" class="bold">LOCATION</span>\n\n    <p ion-text>{{ trip.location }}</p>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\trip-detail\trip-detail.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_trip_service__["a" /* TripService */]])
    ], TripDetailPage);
    return TripDetailPage;
}());

//# sourceMappingURL=trip-detail.js.map

/***/ }),

/***/ 265:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckoutTripPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_trip_service__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(63);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CheckoutTripPage = /** @class */ (function () {
    function CheckoutTripPage(nav, tripService, loadingCtrl, toastCtrl) {
        this.nav = nav;
        this.tripService = tripService;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        // number of adults
        this.adults = 2;
        // date
        this.date = new Date();
        this.paymethods = 'creditcard';
        // set sample data
        this.trip = tripService.getItem(1);
    }
    // process send button
    CheckoutTripPage.prototype.send = function () {
        var _this = this;
        // send booking info
        var loader = this.loadingCtrl.create({
            content: "Please wait..."
        });
        // show message
        var toast = this.toastCtrl.create({
            showCloseButton: true,
            cssClass: 'profile-bg',
            message: 'Book Activity Success!',
            duration: 3000,
            position: 'bottom'
        });
        loader.present();
        setTimeout(function () {
            loader.dismiss();
            toast.present();
            // back to home page
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
        }, 3000);
    };
    CheckoutTripPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-checkout-trip',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\checkout-trip\checkout-trip.html"*/'<!-- -->\n\n<ion-header>\n\n\n\n  <ion-navbar color="primary">\n\n    <ion-title>Activity Checkout</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding class="checkout-trip common-bg">\n\n  <!--trip information-->\n\n  <div class="trip-info card round">\n\n    <div class="trip-image border-bottom" [ngStyle]="{\'background-image\': \'url(\' + trip.thumb + \')\'}"></div>\n\n    <ion-grid padding>\n\n      <ion-row>\n\n        <ion-col width-66>\n\n          <h5 ion-text color="primary">{{ trip.name }}</h5>\n\n          <div>\n\n            <span class="bold">{{ trip.sub_name }}</span>\n\n            <br/>\n\n            <span ion-text color="dark">{{ adults }} Adults</span>\n\n          </div>\n\n          <div margin-top>\n\n            <span ion-text color="dark">{{ date | date: \'EEE, MMM dd\' }}</span>\n\n            <br/>\n\n            <span ion-text>{{ trip.location }}</span>\n\n          </div>\n\n          <div margin-top>\n\n            <ion-icon name="checkmark" class="text-green" *ngIf="trip.free_cancellation"></ion-icon>\n\n            <span ion-text *ngIf="trip.free_cancellation">Free cancellation</span>\n\n          </div>\n\n        </ion-col>\n\n        <ion-col col-4>\n\n          <span ion-text>Total with Tax</span>\n\n          <h5 ion-text color="primary" class="bold" no-margin>{{ trip.price_adult * adults | currency:\'USD\':true }}</h5>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </div>\n\n\n\n    <!--more info-->\n\n  <h5>Guest Details</h5>\n\n  <div class="card round" margin-top>\n\n\n\n    <ion-list no-margin>\n\n      <ion-item class="primary-bg">\n\n        <ion-avatar item-start>\n\n          <img src="assets/img/avatar.jpeg">\n\n        </ion-avatar>\n\n        <h2 ion-text class="text-white bold">João Firmino</h2>\n\n        <p ion-text class="text-secondary bold">User</p>\n\n      </ion-item>\n\n    </ion-list>\n\n\n\n    <div padding>\n\n      <h5 ion-text color="secondary">Other Guests</h5>\n\n\n\n      <ion-item no-padding>\n\n        <ion-label color="dark" stacked>Adult 1 Name:</ion-label>\n\n        <ion-input type="text" placeholder="Ex. Joe Doe" value=""></ion-input>\n\n      </ion-item>\n\n      <ion-item no-padding>\n\n        <ion-label color="dark" stacked>Child 1 Name:</ion-label>\n\n        <ion-input type="text" placeholder="Ex. Joe Doe" value=""></ion-input>\n\n      </ion-item>\n\n    </div>\n\n  </div>\n\n\n\n  <!--payment info-->\n\n  <h5>Payment Methods</h5>\n\n  <ion-segment color="secondary" [(ngModel)]="paymethods">\n\n    <ion-segment-button value="creditcard" >\n\n      Credit card\n\n    </ion-segment-button>\n\n    <ion-segment-button value="paypal">\n\n      PayPal\n\n    </ion-segment-button>\n\n  </ion-segment>\n\n\n\n  <div class="card round" margin-top margin-bottom>\n\n\n\n    <div [ngSwitch]="paymethods">\n\n      <ion-grid *ngSwitchCase="\'creditcard\'" padding>\n\n        <ion-row>\n\n          <ion-col no-padding text-center>\n\n            <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZpZXdCb3g9IjAgMCA0OCA0OCIgdmVyc2lvbj0iMS4xIiB3aWR0aD0iMzJweCIgaGVpZ2h0PSIzMnB4Ij48ZyBpZD0ic3VyZmFjZTEiPjxwYXRoIHN0eWxlPSIgZmlsbDojMTU2NUMwOyIgZD0iTSA0NSAzNSBDIDQ1IDM3LjIxMDkzOCA0My4yMTA5MzggMzkgNDEgMzkgTCA3IDM5IEMgNC43ODkwNjMgMzkgMyAzNy4yMTA5MzggMyAzNSBMIDMgMTMgQyAzIDEwLjc4OTA2MyA0Ljc4OTA2MyA5IDcgOSBMIDQxIDkgQyA0My4yMTA5MzggOSA0NSAxMC43ODkwNjMgNDUgMTMgWiAiLz48cGF0aCBzdHlsZT0iIGZpbGw6I0ZGRkZGRjsiIGQ9Ik0gMTUuMTg3NSAxOSBMIDEyLjU1ODU5NCAyNi44MzIwMzEgQyAxMi41NTg1OTQgMjYuODMyMDMxIDExLjg5NDUzMSAyMy41MTk1MzEgMTEuODI4MTI1IDIzLjEwMTU2MyBDIDEwLjMzMjAzMSAxOS42OTE0MDYgOC4xMjUgMTkuODgyODEzIDguMTI1IDE5Ljg4MjgxMyBMIDEwLjcyNjU2MyAzMCBMIDEwLjcyNjU2MyAyOS45OTYwOTQgTCAxMy44ODY3MTkgMjkuOTk2MDk0IEwgMTguMjU3ODEzIDE5IFogIi8+PHBhdGggc3R5bGU9IiBmaWxsOiNGRkZGRkY7IiBkPSJNIDE3LjY4NzUgMzAgTCAyMC41NTg1OTQgMzAgTCAyMi4yOTY4NzUgMTkgTCAxOS4zOTA2MjUgMTkgWiAiLz48cGF0aCBzdHlsZT0iIGZpbGw6I0ZGRkZGRjsiIGQ9Ik0gMzguMDA3ODEzIDE5IEwgMzQuOTg4MjgxIDE5IEwgMzAuMjc3MzQ0IDMwIEwgMzMuMTI4OTA2IDMwIEwgMzMuNzE4NzUgMjguNDI5Njg4IEwgMzcuMzEyNSAyOC40Mjk2ODggTCAzNy42MTcxODggMzAgTCA0MC4yMzA0NjkgMzAgWiBNIDM0LjUxMTcxOSAyNi4zMjgxMjUgTCAzNi4wNzQyMTkgMjIuMTcxODc1IEwgMzYuODk0NTMxIDI2LjMyODEyNSBaICIvPjxwYXRoIHN0eWxlPSIgZmlsbDojRkZGRkZGOyIgZD0iTSAyNi4zNjcxODggMjIuMjA3MDMxIEMgMjYuMzY3MTg4IDIxLjYwMTU2MyAyNi44NjcxODggMjEuMTQ4NDM4IDI4LjI5Njg3NSAyMS4xNDg0MzggQyAyOS4yMjI2NTYgMjEuMTQ4NDM4IDMwLjI4NTE1NiAyMS44MjQyMTkgMzAuMjg1MTU2IDIxLjgyNDIxOSBMIDMwLjc1MzkwNiAxOS41MTU2MjUgQyAzMC43NTM5MDYgMTkuNTE1NjI1IDI5LjM5NDUzMSAxOSAyOC4wNjI1IDE5IEMgMjUuMDQyOTY5IDE5IDIzLjQ4NDM3NSAyMC40NDE0MDYgMjMuNDg0Mzc1IDIyLjI2OTUzMSBDIDIzLjQ4NDM3NSAyNS41NzgxMjUgMjcuNDY0ODQ0IDI1LjEyNSAyNy40NjQ4NDQgMjYuODIwMzEzIEMgMjcuNDY0ODQ0IDI3LjExMzI4MSAyNy4yMzQzNzUgMjcuNzg1MTU2IDI1LjU3NDIxOSAyNy43ODUxNTYgQyAyMy45MTQwNjMgMjcuNzg1MTU2IDIyLjgxNjQwNiAyNy4xNzU3ODEgMjIuODE2NDA2IDI3LjE3NTc4MSBMIDIyLjMyMDMxMyAyOS4zOTQ1MzEgQyAyMi4zMjAzMTMgMjkuMzk0NTMxIDIzLjM4NjcxOSAzMCAyNS40Mzc1IDMwIEMgMjcuNDk2MDk0IDMwIDMwLjM1NTQ2OSAyOC40NjA5MzggMzAuMzU1NDY5IDI2LjI0NjA5NCBDIDMwLjM1NTQ2OSAyMy41ODU5MzggMjYuMzY3MTg4IDIzLjM5NDUzMSAyNi4zNjcxODggMjIuMjA3MDMxIFogIi8+PHBhdGggc3R5bGU9IiBmaWxsOiNGRkMxMDc7IiBkPSJNIDEyLjIxMDkzOCAyNC45NDUzMTMgTCAxMS4yNDYwOTQgMjAuMTk1MzEzIEMgMTEuMjQ2MDk0IDIwLjE5NTMxMyAxMC44MDg1OTQgMTkuMTY3OTY5IDkuNjcxODc1IDE5LjE2Nzk2OSBDIDguNTM1MTU2IDE5LjE2Nzk2OSA1LjIzNDM3NSAxOS4xNjc5NjkgNS4yMzQzNzUgMTkuMTY3OTY5IEMgNS4yMzQzNzUgMTkuMTY3OTY5IDEwLjg5NDUzMSAyMC44Mzk4NDQgMTIuMjEwOTM4IDI0Ljk0NTMxMyBaICIvPjwvZz48L3N2Zz4=" alt="Visa" />\n\n            <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZpZXdCb3g9IjAgMCA0OCA0OCIgdmVyc2lvbj0iMS4xIiB3aWR0aD0iMzJweCIgaGVpZ2h0PSIzMnB4Ij48ZyBpZD0ic3VyZmFjZTEiPjxwYXRoIHN0eWxlPSIgZmlsbDojM0Y1MUI1OyIgZD0iTSA0NSAzNSBDIDQ1IDM3LjIxMDkzOCA0My4yMTA5MzggMzkgNDEgMzkgTCA3IDM5IEMgNC43ODkwNjMgMzkgMyAzNy4yMTA5MzggMyAzNSBMIDMgMTMgQyAzIDEwLjc4OTA2MyA0Ljc4OTA2MyA5IDcgOSBMIDQxIDkgQyA0My4yMTA5MzggOSA0NSAxMC43ODkwNjMgNDUgMTMgWiAiLz48cGF0aCBzdHlsZT0iIGZpbGw6I0ZGQzEwNzsiIGQ9Ik0gNDAgMjQgQyA0MCAyOS41MjM0MzggMzUuNTIzNDM4IDM0IDMwIDM0IEMgMjQuNDc2NTYzIDM0IDIwIDI5LjUyMzQzOCAyMCAyNCBDIDIwIDE4LjQ3NjU2MyAyNC40NzY1NjMgMTQgMzAgMTQgQyAzNS41MjM0MzggMTQgNDAgMTguNDc2NTYzIDQwIDI0IFogIi8+PHBhdGggc3R5bGU9IiBmaWxsOiNGRjNEMDA7IiBkPSJNIDIyLjAxNTYyNSAzMCBDIDIxLjU1MDc4MSAyOS4zODI4MTMgMjEuMTUyMzQ0IDI4LjcxNDg0NCAyMC44Mzk4NDQgMjggTCAyNi4xNjQwNjMgMjggQyAyNi40NDE0MDYgMjcuMzYzMjgxIDI2LjY2MDE1NiAyNi42OTUzMTMgMjYuODAwNzgxIDI2IEwgMjAuMjAzMTI1IDI2IEMgMjAuMDcwMzEzIDI1LjM1NTQ2OSAyMCAyNC42ODc1IDIwIDI0IEwgMjcgMjQgQyAyNyAyMy4zMTI1IDI2LjkyOTY4OCAyMi42NDQ1MzEgMjYuODAwNzgxIDIyIEwgMjAuMTk5MjE5IDIyIEMgMjAuMzQzNzUgMjEuMzA0Njg4IDIwLjU1ODU5NCAyMC42MzY3MTkgMjAuODM5ODQ0IDIwIEwgMjYuMTY0MDYzIDIwIEMgMjUuODUxNTYzIDE5LjI4NTE1NiAyNS40NTMxMjUgMTguNjE3MTg4IDI0Ljk4ODI4MSAxOCBMIDIyLjAxNTYyNSAxOCBDIDIyLjQ0OTIxOSAxNy40MjE4NzUgMjIuOTQ1MzEzIDE2Ljg3ODkwNiAyMy40OTYwOTQgMTYuNDA2MjUgQyAyMS43NDYwOTQgMTQuOTEwMTU2IDE5LjQ4MDQ2OSAxNCAxNyAxNCBDIDExLjQ3NjU2MyAxNCA3IDE4LjQ3NjU2MyA3IDI0IEMgNyAyOS41MjM0MzggMTEuNDc2NTYzIDM0IDE3IDM0IEMgMjAuMjY5NTMxIDM0IDIzLjE2MDE1NiAzMi40MjU3ODEgMjQuOTg0Mzc1IDMwIFogIi8+PC9nPjwvc3ZnPg==" alt="mastercard">\n\n            <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZpZXdCb3g9IjAgMCA0OCA0OCIgdmVyc2lvbj0iMS4xIiB3aWR0aD0iMzJweCIgaGVpZ2h0PSIzMnB4Ij48ZyBpZD0ic3VyZmFjZTEiPjxwYXRoIHN0eWxlPSIgZmlsbDojRTFFN0VBOyIgZD0iTSA0NSAzNSBDIDQ1IDM3LjE5OTIxOSA0My4xOTkyMTkgMzkgNDEgMzkgTCA3IDM5IEMgNC44MDA3ODEgMzkgMyAzNy4xOTkyMTkgMyAzNSBMIDMgMTMgQyAzIDEwLjgwMDc4MSA0LjgwMDc4MSA5IDcgOSBMIDQxIDkgQyA0My4xOTkyMTkgOSA0NSAxMC44MDA3ODEgNDUgMTMgWiAiLz48cGF0aCBzdHlsZT0iIGZpbGw6I0ZGNkQwMDsiIGQ9Ik0gNDUgMzUgQyA0NSAzNy4xOTkyMTkgNDMuMTk5MjE5IDM5IDQxIDM5IEwgMTYgMzkgQyAxNiAzOSAzOS42MDE1NjMgMzUuMTk5MjE5IDQ1IDI0IFogTSAyMiAyNCBDIDIyIDI1LjY5OTIxOSAyMy4zMDA3ODEgMjcgMjUgMjcgQyAyNi42OTkyMTkgMjcgMjggMjUuNjk5MjE5IDI4IDI0IEMgMjggMjIuMzAwNzgxIDI2LjY5OTIxOSAyMSAyNSAyMSBDIDIzLjMwMDc4MSAyMSAyMiAyMi4zMDA3ODEgMjIgMjQgWiAiLz48cGF0aCBzdHlsZT0iICIgZD0iTSAxMS4xOTkyMTkgMjEgTCAxMi4zMDA3ODEgMjEgTCAxMi4zMDA3ODEgMjcgTCAxMS4xOTkyMTkgMjcgWiBNIDE3LjE5OTIxOSAyNCBDIDE3LjE5OTIxOSAyNS42OTkyMTkgMTguNSAyNyAyMC4xOTkyMTkgMjcgQyAyMC42OTkyMTkgMjcgMjEuMTAxNTYzIDI2Ljg5ODQzOCAyMS42MDE1NjMgMjYuNjk5MjE5IEwgMjEuNjAxNTYzIDI1LjM5ODQzOCBDIDIxLjE5OTIxOSAyNS44MDA3ODEgMjAuODAwNzgxIDI2IDIwLjE5OTIxOSAyNiBDIDE5LjEwMTU2MyAyNiAxOC4zMDA3ODEgMjUuMTk5MjE5IDE4LjMwMDc4MSAyNCBDIDE4LjMwMDc4MSAyMi44OTg0MzggMTkuMTAxNTYzIDIyIDIwLjE5OTIxOSAyMiBDIDIwLjY5OTIxOSAyMiAyMS4xMDE1NjMgMjIuMTk5MjE5IDIxLjYwMTU2MyAyMi42MDE1NjMgTCAyMS42MDE1NjMgMjEuMzAwNzgxIEMgMjEuMTAxNTYzIDIxLjEwMTU2MyAyMC42OTkyMTkgMjAuODk4NDM4IDIwLjE5OTIxOSAyMC44OTg0MzggQyAxOC41IDIxIDE3LjE5OTIxOSAyMi4zOTg0MzggMTcuMTk5MjE5IDI0IFogTSAzMC42MDE1NjMgMjQuODk4NDM4IEwgMjkgMjEgTCAyNy44MDA3ODEgMjEgTCAzMC4zMDA3ODEgMjcgTCAzMC44OTg0MzggMjcgTCAzMy4zOTg0MzggMjEgTCAzMi4xOTkyMTkgMjEgWiBNIDMzLjg5ODQzOCAyNyBMIDM3LjEwMTU2MyAyNyBMIDM3LjEwMTU2MyAyNiBMIDM1IDI2IEwgMzUgMjQuMzk4NDM4IEwgMzcgMjQuMzk4NDM4IEwgMzcgMjMuMzk4NDM4IEwgMzUgMjMuMzk4NDM4IEwgMzUgMjIgTCAzNy4xMDE1NjMgMjIgTCAzNy4xMDE1NjMgMjEgTCAzMy44OTg0MzggMjEgWiBNIDQxLjUgMjIuODAwNzgxIEMgNDEuNSAyMS42OTkyMTkgNDAuODAwNzgxIDIxIDM5LjUgMjEgTCAzNy44MDA3ODEgMjEgTCAzNy44MDA3ODEgMjcgTCAzOC44OTg0MzggMjcgTCAzOC44OTg0MzggMjQuNjAxNTYzIEwgMzkgMjQuNjAxNTYzIEwgNDAuNjAxNTYzIDI3IEwgNDIgMjcgTCA0MC4xOTkyMTkgMjQuNSBDIDQxIDI0LjMwMDc4MSA0MS41IDIzLjY5OTIxOSA0MS41IDIyLjgwMDc4MSBaIE0gMzkuMTk5MjE5IDIzLjgwMDc4MSBMIDM4Ljg5ODQzOCAyMy44MDA3ODEgTCAzOC44OTg0MzggMjIgTCAzOS4xOTkyMTkgMjIgQyAzOS44OTg0MzggMjIgNDAuMzAwNzgxIDIyLjMwMDc4MSA0MC4zMDA3ODEgMjIuODk4NDM4IEMgNDAuMzAwNzgxIDIzLjM5ODQzOCA0MCAyMy44MDA3ODEgMzkuMTk5MjE5IDIzLjgwMDc4MSBaIE0gNy42OTkyMTkgMjEgTCA2IDIxIEwgNiAyNyBMIDcuNjAxNTYzIDI3IEMgMTAuMTAxNTYzIDI3IDEwLjY5OTIxOSAyNC44OTg0MzggMTAuNjk5MjE5IDI0IEMgMTAuODAwNzgxIDIyLjE5OTIxOSA5LjUgMjEgNy42OTkyMTkgMjEgWiBNIDcuMzk4NDM4IDI2IEwgNy4xMDE1NjMgMjYgTCA3LjEwMTU2MyAyMiBMIDcuNSAyMiBDIDkgMjIgOS42MDE1NjMgMjMgOS42MDE1NjMgMjQgQyA5LjYwMTU2MyAyNC4zOTg0MzggOS41IDI2IDcuMzk4NDM4IDI2IFogTSAxNS4zMDA3ODEgMjMuMzAwNzgxIEMgMTQuNjAxNTYzIDIzIDE0LjM5ODQzOCAyMi44OTg0MzggMTQuMzk4NDM4IDIyLjYwMTU2MyBDIDE0LjM5ODQzOCAyMi4xOTkyMTkgMTQuODAwNzgxIDIyIDE1LjE5OTIxOSAyMiBDIDE1LjUgMjIgMTUuODAwNzgxIDIyLjEwMTU2MyAxNi4xMDE1NjMgMjIuNSBMIDE2LjY5OTIxOSAyMS42OTkyMTkgQyAxNi4xOTkyMTkgMjEuMTk5MjE5IDE1LjY5OTIxOSAyMSAxNSAyMSBDIDE0IDIxIDEzLjE5OTIxOSAyMS42OTkyMTkgMTMuMTk5MjE5IDIyLjY5OTIxOSBDIDEzLjE5OTIxOSAyMy41IDEzLjYwMTU2MyAyMy44OTg0MzggMTQuNjAxNTYzIDI0LjMwMDc4MSBDIDE1LjE5OTIxOSAyNC41IDE1LjY5OTIxOSAyNC42OTkyMTkgMTUuNjk5MjE5IDI1LjE5OTIxOSBDIDE1LjY5OTIxOSAyNS42OTkyMTkgMTUuMzAwNzgxIDI2IDE0LjgwMDc4MSAyNiBDIDE0LjMwMDc4MSAyNiAxMy44MDA3ODEgMjUuNjk5MjE5IDEzLjYwMTU2MyAyNS4xOTkyMTkgTCAxMi44OTg0MzggMjUuODk4NDM4IEMgMTMuMzk4NDM4IDI2LjY5OTIxOSAxNCAyNyAxNC44OTg0MzggMjcgQyAxNi4xMDE1NjMgMjcgMTYuODk4NDM4IDI2LjE5OTIxOSAxNi44OTg0MzggMjUuMTAxNTYzIEMgMTYuODk4NDM4IDI0LjE5OTIxOSAxNi41IDIzLjgwMDc4MSAxNS4zMDA3ODEgMjMuMzAwNzgxIFogIi8+PC9nPjwvc3ZnPg==" alt="discover">\n\n            <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAyNTIgMjUyIiB3aWR0aD0iMzJweCIgaGVpZ2h0PSIzMnB4Ij48ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9Im5vbnplcm8iIHN0cm9rZT0ibm9uZSIgc3Ryb2tlLXdpZHRoPSIxIiBzdHJva2UtbGluZWNhcD0iYnV0dCIgc3Ryb2tlLWxpbmVqb2luPSJtaXRlciIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBzdHJva2UtZGFzaGFycmF5PSIiIHN0cm9rZS1kYXNob2Zmc2V0PSIwIiBmb250LWZhbWlseT0ibm9uZSIgZm9udC13ZWlnaHQ9Im5vbmUiIGZvbnQtc2l6ZT0ibm9uZSIgdGV4dC1hbmNob3I9Im5vbmUiIHN0eWxlPSJtaXgtYmxlbmQtbW9kZTogbm9ybWFsIj48cGF0aCBkPSJNMCwyNTJ2LTI1MmgyNTJ2MjUyeiIgZmlsbD0ibm9uZSIvPjxnPjxnIGlkPSJzdXJmYWNlMSI+PHBhdGggZD0iTTIzNi4yNSwxODMuNzVjMCwxMS42MDc0MiAtOS4zOTI1OCwyMSAtMjEsMjFoLTE3OC41Yy0xMS42MDc0MiwwIC0yMSwtOS4zOTI1OCAtMjEsLTIxdi0xMTUuNWMwLC0xMS42MDc0MiA5LjM5MjU4LC0yMSAyMSwtMjFoMTc4LjVjMTEuNjA3NDIsMCAyMSw5LjM5MjU4IDIxLDIxeiIgZmlsbD0iIzE2YTA4NSIvPjxwYXRoIGQ9Ik0xMTYuODMzMDEsMTA1bC0xMS4wOTQ3MywyNC41ODg4N2wtMTEuMDMzMiwtMjQuNTg4ODdoLTE0LjE1MDM5djM1LjMxNDQ2bC0xNS43NzA1MSwtMzUuMzE0NDZoLTExLjkzNTU1bC0xNi4wOTg2MywzNi42NDc0Nmg5LjUzNjEzbDMuNTA2ODQsLTguMTgyNjJoMTguMDI2MzdsMy41ODg4Nyw4LjE4MjYyaDE4LjE5MDQzdi0yNy4yMTM4N2wxMi4wNTg1OSwyNy4yMTM4N2g4LjIwMzEzbDEyLjM0NTcxLC0yNi43NDIxOXYyNi43NDIxOWg5LjA0Mzk0di0zNi42NDc0NnpNNTMuMjE3NzcsMTI1LjU0ODgzbDUuMzczMDQsLTEyLjc5Njg3bDUuNTk4NjQsMTIuNzk2ODh6IiBmaWxsPSIjZmZmZmZmIi8+PHBhdGggZD0iTTE5OC44ODQ3NywxMjIuOTIzODNsMTYuMzY1MjMsLTE3LjgyMTI5aC0xMS42NDg0NGwtMTAuNDU4OTgsMTEuMzYxMzNsLTEwLjEzMDg2LC0xMS40NjM4N2gtMzYuMDExNzJ2MzYuNjQ3NDZoMzQuODQyNzdsMTAuOTcxNjgsLTEyLjEyMDEybDEwLjcwNTA4LDEyLjIyMjY2aDExLjYwNzQyek0xNzcuMDY0NDYsMTMzLjk1NzAzaC0yMS4wNDEwMnYtNy4yMzkyNmgyMC4xMzg2N3YtNi45NTIxNWgtMjAuMTM4Njd2LTYuODcwMTJsMjIuMjA5OTYsMC4wNjE1Mmw4LjkwMDM5LDkuOTY2OHoiIGZpbGw9IiNmZmZmZmYiLz48L2c+PC9nPjwvZz48L3N2Zz4=" alt="Amex">\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-item no-padding>\n\n              <ion-input type="text" placeholder="Card Holder"></ion-input>\n\n<!--               <ion-icon name="person" item-end no-margin></ion-icon> -->\n\n            </ion-item>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-item no-padding>\n\n              <ion-input placeholder="Card Number" type="number"></ion-input>\n\n            </ion-item>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-4>\n\n            <ion-item no-padding>\n\n              <ion-select placeholder="MM" class="max-width full-width">\n\n                <ion-option value="01">01</ion-option>\n\n                <ion-option value="02">02</ion-option>\n\n                <ion-option value="03">03</ion-option>\n\n                <ion-option value="04">04</ion-option>\n\n                <ion-option value="05">05</ion-option>\n\n                <ion-option value="06">06</ion-option>\n\n                <ion-option value="07">07</ion-option>\n\n                <ion-option value="08">08</ion-option>\n\n                <ion-option value="09">09</ion-option>\n\n                <ion-option value="10">10</ion-option>\n\n                <ion-option value="11">11</ion-option>\n\n                <ion-option value="12">12</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n          </ion-col>\n\n          <ion-col col-4>\n\n            <ion-item no-padding>\n\n              <ion-select placeholder="YY" class="max-width full-width">\n\n                <ion-option value="19">19</ion-option>\n\n                <ion-option value="20">20</ion-option>\n\n                <ion-option value="21">21</ion-option>\n\n                <ion-option value="22">22</ion-option>\n\n                <ion-option value="23">23</ion-option>\n\n                <ion-option value="24">24</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n          </ion-col>\n\n          <ion-col col-4>\n\n            <ion-item no-padding>\n\n              <ion-input placeholder="CVV" type="number"></ion-input>\n\n            </ion-item>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n\n\n      <ion-grid *ngSwitchCase="\'paypal\'" padding>\n\n        <ion-row>\n\n          <ion-col no-padding text-center>\n\n            <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHZpZXdCb3g9IjAgMCA0OCA0OCIgdmVyc2lvbj0iMS4xIiB3aWR0aD0iMzJweCIgaGVpZ2h0PSIzMnB4Ij48ZyBpZD0ic3VyZmFjZTEiPjxwYXRoIHN0eWxlPSIgZmlsbDojMTU2NUMwOyIgZD0iTSAxOC42OTkyMTkgMTMuNzY1NjI1IEwgMTguNzAzMTI1IDEzLjc2OTUzMSBDIDE4LjgwODU5NCAxMy4zMjQyMTkgMTkuMTg3NSAxMyAxOS42NjAxNTYgMTMgTCAzMy4xMzI4MTMgMTMgQyAzMy4xNDg0MzggMTMgMzMuMTY0MDYzIDEyLjk5MjE4OCAzMy4xODM1OTQgMTIuOTkyMTg4IEMgMzIuODk0NTMxIDguMjE0ODQ0IDI4Ljg4NjcxOSA2IDI1LjM1MTU2MyA2IEwgMTEuODc4OTA2IDYgQyAxMS40MDIzNDQgNiAxMS4wMjczNDQgNi4zMzU5MzggMTAuOTIxODc1IDYuNzc3MzQ0IEwgMTAuOTE3OTY5IDYuNzczNDM4IEwgNS4wMjczNDQgMzMuODEyNSBMIDUuMDQyOTY5IDMzLjgxMjUgQyA1LjAyNzM0NCAzMy44Nzg5MDYgNS4wMDM5MDYgMzMuOTM3NSA1LjAwMzkwNiAzNC4wMDc4MTMgQyA1LjAwMzkwNiAzNC41NjI1IDUuNDQ5MjE5IDM1IDYuMDAzOTA2IDM1IEwgMTQuMDc0MjE5IDM1IFogIi8+PHBhdGggc3R5bGU9IiBmaWxsOiMwMzlCRTU7IiBkPSJNIDMzLjE4MzU5NCAxMi45OTIxODggQyAzMy4yMzQzNzUgMTMuODcxMDk0IDMzLjE3OTY4OCAxNC44MjQyMTkgMzIuOTUzMTI1IDE1Ljg3NSBDIDMxLjY3MTg3NSAyMS44NzEwOTQgMjcuMDQyOTY5IDI0Ljk5MjE4OCAyMS4zMjAzMTMgMjQuOTkyMTg4IEMgMjEuMzIwMzEzIDI0Ljk5MjE4OCAxNy44NDc2NTYgMjQuOTkyMTg4IDE3LjAwNzgxMyAyNC45OTIxODggQyAxNi40ODQzNzUgMjQuOTkyMTg4IDE2LjIzODI4MSAyNS4yOTY4NzUgMTYuMTI1IDI1LjUzMTI1IEwgMTQuMzg2NzE5IDMzLjU3ODEyNSBMIDE0LjA4MjAzMSAzNS4wMDc4MTMgTCAxNC4wNzQyMTkgMzUuMDA3ODEzIEwgMTIuODEyNSA0MC44MDQ2ODggTCAxMi44MjQyMTkgNDAuODA0Njg4IEMgMTIuODEyNSA0MC44NzEwOTQgMTIuNzg1MTU2IDQwLjkyOTY4OCAxMi43ODUxNTYgNDEgQyAxMi43ODUxNTYgNDEuNTU0Njg4IDEzLjIzNDM3NSA0MiAxMy43ODUxNTYgNDIgTCAyMS4xMTcxODggNDIgTCAyMS4xMzI4MTMgNDEuOTg4MjgxIEMgMjEuNjA1NDY5IDQxLjk4NDM3NSAyMS45ODA0NjkgNDEuNjQ0NTMxIDIyLjA3ODEyNSA0MS4yMDMxMjUgTCAyMi4wOTM3NSA0MS4xODc1IEwgMjMuOTA2MjUgMzIuNzY5NTMxIEMgMjMuOTA2MjUgMzIuNzY5NTMxIDI0LjAzMTI1IDMxLjk2ODc1IDI0Ljg3ODkwNiAzMS45Njg3NSBDIDI1LjcyMjY1NiAzMS45Njg3NSAyOS4wNTQ2ODggMzEuOTY4NzUgMjkuMDU0Njg4IDMxLjk2ODc1IEMgMzQuNzc3MzQ0IDMxLjk2ODc1IDM5LjQ1NzAzMSAyOC44NjMyODEgNDAuNzM4MjgxIDIyLjg2NzE4OCBDIDQyLjE3OTY4OCAxNi4xMDU0NjkgMzcuMzU5Mzc1IDEzLjAxOTUzMSAzMy4xODM1OTQgMTIuOTkyMTg4IFogIi8+PHBhdGggc3R5bGU9IiBmaWxsOiMyODM1OTM7IiBkPSJNIDE5LjY2MDE1NiAxMyBDIDE5LjE4NzUgMTMgMTguODA4NTk0IDEzLjMyNDIxOSAxOC43MDMxMjUgMTMuNzY5NTMxIEwgMTguNjk5MjE5IDEzLjc2NTYyNSBMIDE2LjEyNSAyNS41MzEyNSBDIDE2LjIzODI4MSAyNS4yOTY4NzUgMTYuNDg0Mzc1IDI0Ljk5MjE4OCAxNy4wMDM5MDYgMjQuOTkyMTg4IEMgMTcuODQ3NjU2IDI0Ljk5MjE4OCAyMS4yMzgyODEgMjQuOTkyMTg4IDIxLjIzODI4MSAyNC45OTIxODggQyAyNi45NjQ4NDQgMjQuOTkyMTg4IDMxLjY3MTg3NSAyMS44NzEwOTQgMzIuOTUzMTI1IDE1Ljg3ODkwNiBDIDMzLjE3OTY4OCAxNC44MjQyMTkgMzMuMjM0Mzc1IDEzLjg3MTA5NCAzMy4xODM1OTQgMTIuOTk2MDk0IEMgMzMuMTY0MDYzIDEyLjk5MjE4OCAzMy4xNDg0MzggMTMgMzMuMTMyODEzIDEzIFogIi8+PC9nPjwvc3ZnPg==" alt="paypal">\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-item no-padding>\n\n              <ion-input type="mail" placeholder="E-mail"></ion-input>\n\n            </ion-item>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col>\n\n            <ion-item no-padding>\n\n              <ion-input placeholder="Password" type="password"></ion-input>\n\n            </ion-item>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n    </div>\n\n\n\n  </div>\n\n\n\n  <!--submit button-->\n\n  <button ion-button class="round" color="primary" margin-top full tappable (click)="send()">SEND</button>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\checkout-trip\checkout-trip.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_trip_service__["a" /* TripService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], CheckoutTripPage);
    return CheckoutTripPage;
}());

//# sourceMappingURL=checkout-trip.js.map

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChooseDatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_data_services__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__notifications_notifications__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__settings_settings__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__form_kinerja_form_kinerja__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_globalController__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__trips_trips__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__search_location_search_location__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var ChooseDatePage = /** @class */ (function () {
    function ChooseDatePage(storage, navParams, nav, http, popoverCtrl, data, global) {
        this.storage = storage;
        this.navParams = navParams;
        this.nav = nav;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.data = data;
        this.global = global;
        // search condition
        this.search = {
            name: "Rio de Janeiro, Brazil",
            date: new Date().toISOString()
        };
        this.arrUser = this.data.dataUser();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        var btoaaa = btoa(this.navParams.data);
    }
    ChooseDatePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then(function (val) {
            if (val === null) {
                _this.search.name = "Rio de Janeiro, Brazil";
            }
            else {
                _this.search.name = val;
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    ChooseDatePage.prototype.getKriteria = function () {
        var _this = this;
        var json;
        var url = 'http://localhost/api-v1/request/get_kriteria';
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_kriteria';
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            _this.kriteriaNilai = json.data.data;
            _this.result = json.data.result;
        })
            .subscribe(function (data) { }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    ChooseDatePage.prototype.getFormKriteria = function () {
        if (this.date_nilai) {
            var init = btoa(this.navParams.data + '|' + this.date_nilai);
            this.nav.push(__WEBPACK_IMPORTED_MODULE_9__form_kinerja_form_kinerja__["a" /* FormKinerjaPage */], init);
        }
        else {
            this.global.toaster("Please Choose date first");
        }
    };
    ChooseDatePage.prototype.post_nilai = function () {
        var _this = this;
        if (typeof this.navParams.data === 'string') {
            var url = 'http://localhost/api-v1/request/post_nilai/' + this.navParams.data;
            var remoteurl = 'http://bms.uphero.com/api-v1/request/post_nilai/' + this.navParams.data;
            this.http.post(remoteurl, JSON.stringify(this.result))
                .timeout(3000)
                .map(function (data) {
                _this.global.loader();
                var res = JSON.parse(data['_body']);
            })
                .subscribe(function (data) {
            }, function (error) {
                var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
                _this.global.toaster(text);
            });
        }
        else {
            this.global.toaster("error with id is null");
        }
    };
    // go to result page
    ChooseDatePage.prototype.doSearch = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_11__trips_trips__["a" /* TripsPage */]);
    };
    // choose place
    ChooseDatePage.prototype.choosePlace = function (from) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_12__search_location_search_location__["a" /* SearchLocationPage */], from);
    };
    // to go account page
    ChooseDatePage.prototype.goToAccount = function (id) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_8__settings_settings__["a" /* SettingsPage */], id);
    };
    ChooseDatePage.prototype.presentNotifications = function (myEvent) {
        console.log(myEvent);
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_7__notifications_notifications__["a" /* NotificationsPage */]);
        popover.present({
            ev: myEvent
        });
    };
    ChooseDatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-choose-date',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\choose-date\choose-date.html"*/'<!-- -->\n\n<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>\n\n            {{title}}\n\n        </ion-title>\n\n        <!-- <ion-buttons end>\n\n            <button ion-button tappable (click)="presentNotifications($event)">\n\n                <ion-icon name="person"></ion-icon>\n\n            </button>\n\n        </ion-buttons> -->\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="animated fadeIn common-bg">\n\n    <ion-card>\n\n        <ion-item>\n\n            <ion-label floating>\n\n                <ion-icon name="calendar" item-start class="text-primary"></ion-icon>\n\n                Pilih Bulan Penilaian\n\n            </ion-label>\n\n            <ion-datetime displayFormat="MMMM YYYY" [(ngModel)]="date_nilai"></ion-datetime>\n\n        </ion-item>\n\n    </ion-card>\n\n    \n\n    <button ion-button icon-start block color="secondary" tappable (click)="getFormKriteria()">\n\n        Lanjut \n\n        <ion-icon name="arrow-forward"></ion-icon>\n\n      </button>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\choose-date\choose-date.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_6__services_data_services__["a" /* DataServices */], __WEBPACK_IMPORTED_MODULE_10__app_globalController__["a" /* GlobalController */]])
    ], ChooseDatePage);
    return ChooseDatePage;
}());

//
//# sourceMappingURL=choose-date.js.map

/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_data_services__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_user_user__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_nilai_nilai__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_detail_info_detail_info__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_globalController__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/* import { NotificationsPage } from "../notifications/notifications";
import { SettingsPage } from "../settings/settings"; */

/* import { TripsPage } from "../trips/trips";
import { SearchLocationPage } from "../search-location/search-location"; */
var DashboardPage = /** @class */ (function () {
    function DashboardPage(storage, nav, http, popoverCtrl, data, global) {
        this.storage = storage;
        this.nav = nav;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.data = data;
        this.global = global;
        this.parseData = {};
        this.getStats();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        this.role = localStorage.getItem('role');
        var token = localStorage.getItem('token').split(".");
        this.parseData = JSON.parse(atob(token[1]));
    }
    DashboardPage.prototype.getStats = function () {
        var _this = this;
        var json;
        var token = localStorage.getItem('token').split(".");
        var parseData = JSON.parse(atob(token[1]));
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_stats/' + ((parseData.role == "2") ? parseData.user_id : '');
        console.log(parseData);
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            console.log(_this.role);
            if (_this.role == 1) {
                _this.user = json.data.user;
                _this.nilai = json.data.nilai;
                _this.officer = json.data.officer;
            }
            else if (_this.role == 2) {
                _this.nilai = json.data.nilai_officer.total;
            }
        })
            .subscribe(function (data) { }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    DashboardPage.prototype.goToUser = function () {
        localStorage.setItem('component', 'User');
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_user_user__["a" /* userPage */]);
    };
    DashboardPage.prototype.goToNilai = function () {
        localStorage.setItem('component', 'Penilaian');
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_nilai_nilai__["a" /* nilaiPage */]);
    };
    DashboardPage.prototype.goToNilaiOfMonth = function () {
        localStorage.setItem('component', 'Penilaian Bulan Ini');
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_detail_info_detail_info__["a" /* DetailInfoPage */]);
    };
    DashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dashboard',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\dashboard\dashboard.html"*/'<!-- -->\n\n<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>\n\n            {{title}}\n\n        </ion-title>\n\n        <!-- <ion-buttons end>\n\n            <button ion-button tappable (click)="presentNotifications($event)">\n\n        <ion-icon name="person"></ion-icon>\n\n      </button>\n\n            <button ion-button tappable (click)="goToAccount()">\n\n        <ion-icon name="cog"></ion-icon>\n\n      </button>\n\n        </ion-buttons> -->\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="animated fadeIn common-bg">\n\n\n\n    <ion-card>\n\n        <ion-card-content>\n\n            <label><i>Selamat Datang User</i></label>\n\n            <ion-card-title>\n\n                <h1>{{nama}}</h1>\n\n            </ion-card-title>\n\n\n\n        </ion-card-content>\n\n    </ion-card>\n\n    <ion-card *ngIf="role == 1">\n\n\n\n        <ion-card-content>\n\n            <label><i>Menu</i></label>\n\n        </ion-card-content>\n\n\n\n        <ion-item (click)="goToUser()">\n\n            <ion-icon name=\'person\' item-start style="color: #d03e84"></ion-icon>\n\n            Users\n\n            <ion-badge item-end>{{user}}</ion-badge>\n\n        </ion-item>\n\n\n\n        <ion-item (click)="goToNilai()">\n\n            <ion-icon name=\'stats\' item-start style="color: #55acee"></ion-icon>\n\n            Penilaian\n\n            <ion-badge item-end>{{nilai}}</ion-badge>\n\n        </ion-item>\n\n\n\n    </ion-card>\n\n    <ion-card *ngIf="role==1">\n\n        <ion-card-content>\n\n            <label><i>Petugas yang sudah di nilai bulan ini</i></label>\n\n        </ion-card-content>\n\n        <ion-item (click)="goToNilai()">\n\n            \n\n            <ion-badge block>{{nilai}} / {{officer}} Petugas</ion-badge>\n\n        </ion-item>\n\n    </ion-card>\n\n    <ion-card *ngIf="role==2">\n\n\n\n        <ion-card-content>\n\n            <label><i>Penilaian Anda Bulan Ini</i></label>\n\n        </ion-card-content>\n\n\n\n        <ion-item (click)="goToNilaiOfMonth()">\n\n            <h1>{{nilai}}</h1>\n\n            <!-- <ion-icon name=\'person\' item-start style="color: #d03e84"></ion-icon>\n\n            Users\n\n            <ion-badge item-end>{{user}}</ion-badge> -->\n\n        </ion-item>\n\n\n\n    </ion-card>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\dashboard\dashboard.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_6__services_data_services__["a" /* DataServices */], __WEBPACK_IMPORTED_MODULE_10__app_globalController__["a" /* GlobalController */]])
    ], DashboardPage);
    return DashboardPage;
}());

//
//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListRekapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__notifications_notifications__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_form_rekap_form_rekap__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_globalController__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__trips_trips__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__search_location_search_location__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var ListRekapPage = /** @class */ (function () {
    function ListRekapPage(storage, nav, http, popoverCtrl, global) {
        this.storage = storage;
        this.nav = nav;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.global = global;
        this.page = 1;
        this.perPage = 0;
        this.totalData = 0;
        this.totalPage = 0;
        this.officerList = [];
        this.getOfficers();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
    }
    // getOfficers(q='') {
    //     var json;
    //     var localURL = 'http://localhost/api-v1/request/get_officers/' + q;
    //     var serviceURL = 'http://bms.uphero.com/api-v1/request/get_officers/'+q;
    //     this.http.get(serviceURL)
    //         .map(res => {
    //             //this.global.loader();
    //             json = res.json();
    //             this.officerList = json.data;
    //         })
    //         .subscribe(
    //         data => { },
    //         error => {
    //             var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
    //             this.global.toaster(text);
    //         });
    // }
    ListRekapPage.prototype.getOfficers = function (q) {
        var _this = this;
        if (q === void 0) { q = ''; }
        var json;
        var url = 'http://localhost/api-v1/request/get_officers/' + localStorage.getItem('user_id') + '/' + q;
        //var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers/'+localStorage.getItem('user_id')+'/'+q;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers_page?id_staff=' + localStorage.getItem('user_id') + '&query=' + q;
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            _this.officerList = json.data.data;
            _this.totalData = json.data.total_rows;
            _this.totalPage = json.data.total_page;
        })
            .subscribe(function (data) { }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    ListRekapPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var json;
        this.page = this.page + 1;
        ;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers_page?id_staff=' + localStorage.getItem('user_id') + '&page=' + this.page;
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            //console.log(json.data.total_page);
            for (var i = 0; i < json.data.data.length; i++) {
                _this.officerList.push(json.data.data[i]);
            }
            console.log(_this.page);
            console.log(_this.totalPage);
            _this.totalData = json.data.total_rows;
            _this.totalPage = json.data.total_page;
            setTimeout(function () {
                console.log('Async operation has ended');
                infiniteScroll.complete();
            }, 2000);
        })
            .subscribe(function (data) { }, function (error) {
            //this.global.toaster(error);
            console.log(error);
        });
    };
    ListRekapPage.prototype.searchTerm = function () {
        if (this.q) {
            this.getOfficers(this.q);
        }
        else {
            this.getOfficers();
        }
    };
    ListRekapPage.prototype.valueMean = function (event, criteria) {
        console.log(event);
        console.log(criteria);
        //console.log(this.disiplin);
    };
    // go to result page
    ListRekapPage.prototype.doSearch = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_9__trips_trips__["a" /* TripsPage */]);
    };
    // choose place
    ListRekapPage.prototype.choosePlace = function (from) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_10__search_location_search_location__["a" /* SearchLocationPage */], from);
    };
    // to go account page
    ListRekapPage.prototype.goToForm = function (id) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_7__pages_form_rekap_form_rekap__["a" /* FormRekapPage */], id);
    };
    ListRekapPage.prototype.presentNotifications = function (myEvent) {
        console.log(myEvent);
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_6__notifications_notifications__["a" /* NotificationsPage */]);
        popover.present({
            ev: myEvent
        });
    };
    ListRekapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list-rekap',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\list-rekap\list-rekap.html"*/'<!-- -->\n\n<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>\n\n            {{title}}\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="animated fadeIn common-bg">\n\n\n\n    <ion-list>\n\n        <ion-card>\n\n            <ion-item>\n\n                <ion-icon name="search" item-start class="text-primary"></ion-icon>\n\n                <ion-input type="search" [(ngModel)]="q" (ionChange)="searchTerm()"></ion-input>\n\n            </ion-item>\n\n        </ion-card>\n\n        <ion-card no-margin margin-bottom class="full-width" *ngFor="let user of officerList" (click)="goToForm(user.user_id)">\n\n            <ion-item>\n\n                <h2><strong>{{user.nama}}</strong></h2>\n\n                <h3>{{user.nik}}</h3>\n\n                <h5>Contact Person: {{user.telp}}</h5>\n\n                <button ion-button clear item-end>Detail Rekap</button>\n\n            </ion-item>\n\n        </ion-card>\n\n    </ion-list>\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)" *ngIf="page < totalPage">\n\n        <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data..."></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n    <!-- <ion-card no-margin margin-bottom class="full-width">\n\n        <ion-item tappable (click)="choosePlace(\'from\')" class="border-bottom">\n\n            <ion-icon name="search" color="primary" item-left></ion-icon>\n\n            <span>{{ search.name }}</span>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-icon name="md-calendar" color="primary" item-left></ion-icon>\n\n            <ion-datetime class="no-pl" displayFormat="DD/MM/YYYY" pickerFormat="DD/MM/YYYY" [(ngModel)]="search.date"></ion-datetime>\n\n        </ion-item>\n\n    </ion-card>\n\n\n\n    <button ion-button icon-start block no-margin color="primary" class="round" tappable (click)="doSearch()">\n\n    <ion-icon name="search"></ion-icon> Search\n\n  </button> -->\n\n\n\n</ion-content>'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\list-rekap\list-rekap.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_8__app_globalController__["a" /* GlobalController */]])
    ], ListRekapPage);
    return ListRekapPage;
}());

//
//# sourceMappingURL=list-rekap.js.map

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailRekapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_data_services__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_user_user__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_nilai_nilai__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_globalController__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/* import { NotificationsPage } from "../notifications/notifications";
import { SettingsPage } from "../settings/settings"; */

/* import { TripsPage } from "../trips/trips";
import { SearchLocationPage } from "../search-location/search-location"; */
var DetailRekapPage = /** @class */ (function () {
    function DetailRekapPage(storage, navParams, nav, http, popoverCtrl, data, global) {
        this.storage = storage;
        this.navParams = navParams;
        this.nav = nav;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.data = data;
        this.global = global;
        // search condition
        this.shownGroup = null;
        this.range = [];
        this.detail = [];
        this.getData();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        var params = atob(this.navParams.data).split("|");
        var date1 = params[0].split("-");
        var date2 = params[1].split("-");
        this.month_from = this.global.getMonth(date1[1]);
        this.month_to = this.global.getMonth(date2[1]);
        this.range = this.global.rangeCreator(parseInt(date1[1]), parseInt(date2[1]));
    }
    DetailRekapPage.prototype.getData = function () {
        var _this = this;
        var json;
        var localURL = 'http://localhost/api-v1/request/get_history_rekap/' + this.navParams.data;
        var serviceURL = 'http://bms.uphero.com/api-v1/request/get_history_rekap/' + this.navParams.data;
        this.http.get(serviceURL)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            _this.detail = json.data;
        })
            .subscribe(function (data) { }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    DetailRekapPage.prototype.goToUser = function () {
        localStorage.setItem('component', 'User');
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_user_user__["a" /* userPage */]);
    };
    DetailRekapPage.prototype.goToNilai = function () {
        localStorage.setItem('component', 'Penilaian');
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_nilai_nilai__["a" /* nilaiPage */]);
    };
    DetailRekapPage.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = group;
        }
    };
    DetailRekapPage.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    DetailRekapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detail-rekap',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\detail-rekap\detail-rekap.html"*/'<!-- -->\n\n<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>\n\n            {{detail.nama}}\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="animated fadeIn common-bg">\n\n    <ion-card *ngFor="let item of detail; let i = index">\n\n        <ion-card-content>\n\n            <h2>Penilai : {{item.nama_staff}}</h2>\n\n            <label>Penilaian Bulan {{item.month}}</label>\n\n        </ion-card-content>\n\n        <ion-item title="test" (click)="toggleGroup(i+\'1\')">\n\n            <ion-icon item-start color="success" item-right [name]="isGroupShown(i+\'1\') ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n            Keselamatan Kerja\n\n            <ion-badge item-end>{{item.K3}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item *ngIf="isGroupShown(i+\'1\')" text-wrap>\n\n            <p><small>{{item.K3_ket}}</small></p>\n\n        </ion-item>\n\n        <ion-item (click)="toggleGroup(i+\'2\')">\n\n            <ion-icon item-start color="success" item-right [name]="isGroupShown(i+\'2\') ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n            Disiplin\n\n            <ion-badge item-end>{{item.DIS}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item *ngIf="isGroupShown(i+\'2\')" text-wrap>\n\n            <p><small>{{item.DIS_ket}}</small></p>\n\n        </ion-item>\n\n        <ion-item (click)="toggleGroup(i+\'3\')">\n\n            <ion-icon item-start color="success" item-right [name]="isGroupShown(i+\'3\') ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n            Produktifitas Kerja\n\n            <ion-badge item-end>{{item.PRK}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item *ngIf="isGroupShown(i+\'3\')" text-wrap>\n\n            <p><small>{{item.PRK_ket}}</small></p>\n\n        </ion-item>\n\n        <ion-item (click)="toggleGroup(i+\'4\')">\n\n            <ion-icon item-start color="success" item-right [name]="isGroupShown(i+\'4\') ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n            Kerjasama Tim\n\n            <ion-badge item-end>{{item.TMW}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item *ngIf="isGroupShown(i+\'4\')" text-wrap>\n\n            <p><small>{{item.TMW_ket}}</small></p>\n\n        </ion-item>\n\n        <ion-item (click)="toggleGroup(i+\'5\')">\n\n            <ion-icon item-start color="success" item-right [name]="isGroupShown(i+\'5\') ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n            Etika/Perilaku\n\n            <ion-badge item-end>{{item.ATT}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item *ngIf="isGroupShown(i+\'5\')" text-wrap>\n\n            <p><small>{{item.ATT_ket}}</small></p>\n\n        </ion-item>\n\n        <ion-item (click)="toggleGroup(i+\'0\')">\n\n            <ion-icon item-start color="success" item-right [name]="isGroupShown(i+\'0\') ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n            <strong>Total</strong>\n\n            <ion-badge item-end>{{item.total}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item *ngIf="isGroupShown(i+\'0\')" text-wrap>\n\n            <p><small>{{item.total_ket}}</small></p>\n\n        </ion-item>\n\n    </ion-card>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\detail-rekap\detail-rekap.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_6__services_data_services__["a" /* DataServices */], __WEBPACK_IMPORTED_MODULE_9__app_globalController__["a" /* GlobalController */]])
    ], DetailRekapPage);
    return DetailRekapPage;
}());

//
//# sourceMappingURL=detail-rekap.js.map

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_data_services__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_globalController__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__home_home__ = __webpack_require__(63);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var RegisterPage = /** @class */ (function () {
    function RegisterPage(nav, navParams, formBuilder, http, popoverCtrl, data, global) {
        this.nav = nav;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.data = data;
        this.global = global;
        this.formInstance = [];
        this.dataStaff = [];
        this.id = "";
        this.role = "";
        this.formObj();
        this.title = ((typeof this.navParams.data === 'string') ? 'Ubah User' : 'Tambah User');
        this.nama = localStorage.getItem('name');
        this.form = this.formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            nama: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            nik: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            telp: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            pass: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            confirm_pass: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            birth: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            address: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]
        });
    }
    RegisterPage.prototype.formObj = function () {
        this.formInstance = [
            { label: 'Username', icon: 'at', type: 'email', formCName: 'username' },
            { label: 'Nama', icon: 'person', type: 'text', formCName: 'nama' },
            { label: 'NIK', icon: 'card', type: 'text', formCName: 'nik' },
            { label: 'No Handphone', icon: 'call', type: 'text', formCName: 'telp' },
            { label: 'Password', icon: 'key', type: 'password', formCName: 'pass' },
            { label: 'Confirm Password', icon: 'key', type: 'password', formCName: 'confirm_pass' }
        ];
    };
    RegisterPage.prototype.post_user = function () {
        var _this = this;
        var url = 'http://localhost/api-v1/request/post_register';
        var remoteurl = 'http://bms.uphero.com/api-v1/request/post_register';
        this.http.post(remoteurl, JSON.stringify(this.form.value))
            .timeout(3000)
            .map(function (data) {
            _this.global.loader();
            var res = JSON.parse(data['_body']);
            if (res.status) {
                setTimeout(function () {
                    _this.global.toaster(res.message);
                }, 2000);
                setTimeout(function () {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* LoginPage */]);
                }, 4000);
            }
            else {
                setTimeout(function () {
                    _this.global.toaster(res.message);
                }, 2000);
            }
        })
            .subscribe(function (data) {
        }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    // register and go to home page
    RegisterPage.prototype.register = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__home_home__["a" /* HomePage */]);
    };
    // go to login page
    RegisterPage.prototype.login = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* LoginPage */]);
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\register\register.html"*/'<!-- -->\n\n<ion-content class="auth-page">\n\n  <div class="login-content">\n\n\n\n    <!-- Logo -->\n\n    <div padding text-center>\n\n        <div class="logo"></div>\n\n      <h2 ion-text class="text-primary">\n\n        <strong>Form Registrasi Petugas</strong>\n\n      </h2>\n\n    </div>\n\n\n\n    <form class="list-form" [formGroup]="form">\n\n        <ion-item *ngFor="let form of formInstance; let i=index">\n\n            <ion-label floating>\n\n                <ion-icon [name]="form.icon" item-start class="text-primary"></ion-icon>\n\n                {{form.label}}\n\n            </ion-label>\n\n            <ion-input [type]="form.type" [formControlName]="form.formCName"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>\n\n                <ion-icon name="calendar" item-start class="text-primary"></ion-icon>\n\n                Tanggal Lahir\n\n            </ion-label>\n\n            <ion-datetime displayFormat="DD/MMMM/YYYY" formControlName="birth"></ion-datetime>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-label floating>\n\n                <ion-icon name="card" item-start class="text-primary"></ion-icon>\n\n                Alamat\n\n            </ion-label>\n\n            <ion-textarea formControlName="address">{{form.role}}</ion-textarea>\n\n        </ion-item>\n\n        \n\n        \n\n        <button *ngIf="!id" ion-button icon-start block color="danger" tappable (click)="post_user()" [disabled]="!form.valid"><ion-icon name="send"></ion-icon>\n\n      Simpan</button>\n\n        <button *ngIf="id" ion-button icon-start block color="danger" tappable (click)="post_user()"><ion-icon name="send"></ion-icon>\n\n      Simpan</button>\n\n    </form>\n\n\n\n    <!-- Other links -->\n\n    <div text-center margin-top>\n\n      <span ion-text color="primary" tappable (click)="login()">Saya Sudah Punya Akun</span>\n\n    </div>\n\n\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\register\register.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_6__services_data_services__["a" /* DataServices */], __WEBPACK_IMPORTED_MODULE_7__app_globalController__["a" /* GlobalController */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 271:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_data_services__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_globalController__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ProfilPage = /** @class */ (function () {
    function ProfilPage(nav, navParams, formBuilder, http, popoverCtrl, data, global) {
        this.nav = nav;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.data = data;
        this.global = global;
        this.formInstance = [];
        this.getPerson();
        this.formObj();
        this.title = 'Profil';
        this.edit = false;
        this.nama = localStorage.getItem('name');
        this.form = this.formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            nama: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            telp: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]
        });
    }
    ProfilPage.prototype.formObj = function () {
        this.formInstance = [
            { label: 'Username', icon: 'at', type: 'email', formCName: 'username' },
            { label: 'Nama', icon: 'person', type: 'text', formCName: 'nama' },
            { label: 'No Handphone', icon: 'call', type: 'text', formCName: 'telp' }
        ];
    };
    ProfilPage.prototype.getPerson = function () {
        var _this = this;
        var json;
        var token = localStorage.getItem('token').split('.');
        var info = JSON.parse(atob(token[1]));
        if (typeof info === 'object') {
            var url = 'http://localhost/api-v1/request/get_person/' + info.user_id;
            var remoteurl = 'http://bms.uphero.com/api-v1/request/get_person/' + info.user_id;
            this.http.get(remoteurl)
                .map(function (res) {
                //this.global.loader();
                json = res.json();
                _this.form.controls['username'].setValue(json.data.username);
                _this.form.controls['nama'].setValue(json.data.nama);
                _this.form.controls['telp'].setValue(json.data.telp);
            })
                .subscribe(function (data) { }, function (error) {
                //this.global.toaster(error);
                console.log(error);
            });
        }
    };
    ProfilPage.prototype.canEdit = function (bool) {
        this.edit = bool;
    };
    /* ionViewWillEnter() {
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then((val) => {
            if (val === null) {
                this.search.name = "Rio de Janeiro, Brazil"
            } else {
                this.search.name = val;
            }
        }).catch((err) => {
            console.log(err)
        });
    } */
    ProfilPage.prototype.post_user = function () {
        var _this = this;
        var token = localStorage.getItem('token').split('.');
        var info = JSON.parse(atob(token[1]));
        var url = 'http://localhost/api-v1/request/post_profil/' + info.user_id;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/post_profil/' + info.user_id;
        this.http.post(remoteurl, JSON.stringify(this.form.value))
            .timeout(3000)
            .map(function (data) {
            _this.global.loader();
            var res = JSON.parse(data['_body']);
            if (res.status) {
                setTimeout(function () {
                    _this.global.toaster(res.message);
                }, 2000);
                setTimeout(function () {
                    _this.edit = false;
                }, 4000);
            }
        })
            .subscribe(function (data) {
        }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    ProfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profil',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\profil\profil.html"*/'<!-- -->\n\n<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>\n\n            {{title}}\n\n        </ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button secondary tappable (click)="canEdit(true)" *ngIf="!edit">\n\n                <ion-icon name="checkmark-circle"></ion-icon> Edit\n\n            </button>\n\n            <button ion-button danger tappable (click)="canEdit(false)" *ngIf="edit">\n\n                <ion-icon name="close-circle"></ion-icon> Cancel\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="animated fadeIn common-bg"> \n\n\n\n    <form class="list-form" [formGroup]="form">\n\n        <ion-item *ngFor="let form of formInstance; let i=index">\n\n            <ion-label floating>\n\n                <ion-icon [name]="form.icon" item-start class="text-primary"></ion-icon>\n\n                {{form.label}}\n\n            </ion-label>\n\n            <ion-input [type]="form.type" [formControlName]="form.formCName" [disabled]="!edit"></ion-input>\n\n        </ion-item>\n\n\n\n        <button ion-button icon-start block color="danger" tappable (click)="post_user()" *ngIf="edit"><ion-icon name="send"></ion-icon>\n\n      Ubah</button>\n\n    </form>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\profil\profil.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_6__services_data_services__["a" /* DataServices */], __WEBPACK_IMPORTED_MODULE_7__app_globalController__["a" /* GlobalController */]])
    ], ProfilPage);
    return ProfilPage;
}());

//# sourceMappingURL=profil.js.map

/***/ }),

/***/ 272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EvaluationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_data_services__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__list_petugas_list_petugas__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_globalController__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var EvaluationPage = /** @class */ (function () {
    function EvaluationPage(storage, navParams, nav, http, popoverCtrl, data, global) {
        this.storage = storage;
        this.navParams = navParams;
        this.nav = nav;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.data = data;
        this.global = global;
        // search condition
        this.search = {
            name: "Rio de Janeiro, Brazil",
            date: new Date().toISOString()
        };
        this.post = {
            'date_nilai': '',
            'skala': '',
            'tipe': 'e'
        };
        this.arrUser = this.data.dataUser();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        var btoaaa = btoa(this.navParams.data);
    }
    EvaluationPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then(function (val) {
            if (val === null) {
                _this.search.name = "Rio de Janeiro, Brazil";
            }
            else {
                _this.search.name = val;
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    EvaluationPage.prototype.getListEvaluation = function () {
        if (this.post.date_nilai && this.post.skala) {
            var init = btoa(JSON.stringify(this.post)).replace(new RegExp('=', 'g'), "");
            this.nav.push(__WEBPACK_IMPORTED_MODULE_7__list_petugas_list_petugas__["a" /* ListPetugasPage */], init);
        }
        else {
            this.global.toaster("Please Choose date first");
        }
    };
    EvaluationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-evaluation',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\evaluation\evaluation.html"*/'<!-- -->\n\n<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>\n\n            {{title}}\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="animated fadeIn common-bg">\n\n    <ion-card>\n\n        <ion-item>\n\n            <ion-label floating>\n\n                <ion-icon name="calendar" item-start class="text-primary"></ion-icon>\n\n                Pilih Bulan\n\n            </ion-label>\n\n            <ion-datetime displayFormat="MMMM YYYY" [(ngModel)]="post.date_nilai"></ion-datetime>\n\n        </ion-item>\n\n    </ion-card>\n\n\n\n    <ion-card>\n\n        <ion-item>\n\n            <ion-label floating>\n\n                <ion-icon name="star" item-start class="text-primary"></ion-icon>\n\n                Skala Penilaian\n\n            </ion-label>\n\n            <ion-select [(ngModel)]="post.skala" multiple="true" cancelText="Cancel" okText="Pilih">\n\n              <ion-option value="1">1-5 (Sangat Buruk)</ion-option>\n\n              <ion-option value="2">6-10 (Buruk)</ion-option>\n\n              <ion-option value="3">11-15 (Cukup)</ion-option>\n\n              <ion-option value="4">16-20 (Baik)</ion-option>\n\n              <ion-option value="5">21-25 (Sangat Baik)</ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n    </ion-card>\n\n\n\n    <button ion-button icon-start block color="secondary" tappable (click)="getListEvaluation()">\n\n        Lanjut \n\n        <ion-icon name="arrow-forward"></ion-icon>\n\n      </button>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\evaluation\evaluation.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_6__services_data_services__["a" /* DataServices */], __WEBPACK_IMPORTED_MODULE_8__app_globalController__["a" /* GlobalController */]])
    ], EvaluationPage);
    return EvaluationPage;
}());

//
//# sourceMappingURL=evaluation.js.map

/***/ }),

/***/ 273:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormRankPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_data_services__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__list_petugas_list_petugas__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_globalController__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var FormRankPage = /** @class */ (function () {
    function FormRankPage(storage, navParams, nav, http, popoverCtrl, data, global) {
        this.storage = storage;
        this.navParams = navParams;
        this.nav = nav;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.data = data;
        this.global = global;
        // search condition
        this.search = {
            name: "Rio de Janeiro, Brazil",
            date: new Date().toISOString()
        };
        this.post = {
            'date_nilai': '',
            'tipe': 'r'
        };
        this.arrUser = this.data.dataUser();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        var btoaaa = btoa(this.navParams.data);
    }
    FormRankPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then(function (val) {
            if (val === null) {
                _this.search.name = "Rio de Janeiro, Brazil";
            }
            else {
                _this.search.name = val;
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    FormRankPage.prototype.getKriteria = function () {
        var _this = this;
        var json;
        var url = 'http://localhost/api-v1/request/get_kriteria';
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_kriteria';
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            _this.kriteriaNilai = json.data.data;
            _this.result = json.data.result;
        })
            .subscribe(function (data) { }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    FormRankPage.prototype.getFormKriteria = function () {
        if (this.post.date_nilai) {
            var init = btoa(JSON.stringify(this.post)).replace(new RegExp('=', 'g'), "");
            this.nav.push(__WEBPACK_IMPORTED_MODULE_7__list_petugas_list_petugas__["a" /* ListPetugasPage */], init);
        }
        else {
            this.global.toaster("Please Choose date first");
        }
    };
    FormRankPage.prototype.post_nilai = function () {
        var _this = this;
        if (typeof this.navParams.data === 'string') {
            var url = 'http://localhost/api-v1/request/post_nilai/' + this.navParams.data;
            var remoteurl = 'http://bms.uphero.com/api-v1/request/post_nilai/' + this.navParams.data;
            this.http.post(remoteurl, JSON.stringify(this.result))
                .timeout(3000)
                .map(function (data) {
                _this.global.loader();
                var res = JSON.parse(data['_body']);
            })
                .subscribe(function (data) {
            }, function (error) {
                var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
                _this.global.toaster(text);
            });
        }
        else {
            this.global.toaster("error with id is null");
        }
    };
    FormRankPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-form-rank',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\form-rank\form-rank.html"*/'<!-- -->\n\n<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>\n\n            {{title}}\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="animated fadeIn common-bg">\n\n    <ion-card>\n\n        <ion-item>\n\n            <ion-label floating>\n\n                <ion-icon name="calendar" item-start class="text-primary"></ion-icon>\n\n                Pilih Bulan\n\n            </ion-label>\n\n            <ion-datetime displayFormat="MMMM YYYY" [(ngModel)]="post.date_nilai"></ion-datetime>\n\n        </ion-item>\n\n    </ion-card>\n\n\n\n    <button ion-button icon-start block color="secondary" tappable (click)="getFormKriteria()">\n\n        Lanjut \n\n        <ion-icon name="arrow-forward"></ion-icon>\n\n      </button>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\form-rank\form-rank.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_6__services_data_services__["a" /* DataServices */], __WEBPACK_IMPORTED_MODULE_8__app_globalController__["a" /* GlobalController */]])
    ], FormRankPage);
    return FormRankPage;
}());

//
//# sourceMappingURL=form-rank.js.map

/***/ }),

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(437);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the RestApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RestApiProvider = /** @class */ (function () {
    function RestApiProvider(http) {
        this.http = http;
        this.apiUrl = 'https://reqres.in/api/';
        console.log('Hello RestApiProvider Provider');
    }
    RestApiProvider.prototype.getUsers = function (page) {
        /* return this.http.get(this.apiUrl+"users?page="+page)
                        .map(this.extractData)
                        .catch(this.handleError); */
        return this.http.get(this.apiUrl + "users?page=" + page)
            .map(function (res) {
            //let body = res.json();
            //console.log(res);
            return res;
        })
            .catch(this.handleError);
    };
    RestApiProvider.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    RestApiProvider.prototype.handleError = function (error) {
        var errMsg;
        if (error instanceof __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* Response */]) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(errMsg);
    };
    RestApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], RestApiProvider);
    return RestApiProvider;
}());

//# sourceMappingURL=rest-api.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__(283);



// this is the magic wand
Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_14" /* enableProdMode */])();
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__globalController__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__ = __webpack_require__(242);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_keyboard__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_activity_service__ = __webpack_require__(334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_data_services__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_trip_service__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_weather__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__app_component__ = __webpack_require__(433);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_settings_settings__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_checkout_trip_checkout_trip__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_home_home__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_dashboard_dashboard__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_login_login__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_form_kinerja_form_kinerja__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_form_user_form_user__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_form_rekap_form_rekap__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_form_rank_form_rank__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_list_petugas_list_petugas__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_list_rekap_list_rekap__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_detail_info_detail_info__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_detail_rekap_detail_rekap__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_notifications_notifications__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_register_register__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_search_location_search_location__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_trip_detail_trip_detail__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_trips_trips__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_local_weather_local_weather__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_profil_profil__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_user_user__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_nilai_nilai__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_menu_menu__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_evaluation_evaluation__ = __webpack_require__(272);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_choose_date_choose_date__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__providers_rest_api_rest_api__ = __webpack_require__(274);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









































// import services
// end import services
// end import services
// import pages
// end import pages
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_14__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_15__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_checkout_trip_checkout_trip__["a" /* CheckoutTripPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_dashboard_dashboard__["a" /* DashboardPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_form_kinerja_form_kinerja__["a" /* FormKinerjaPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_form_user_form_user__["a" /* FormUserPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_form_rekap_form_rekap__["a" /* FormRekapPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_form_rank_form_rank__["a" /* FormRankPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_list_petugas_list_petugas__["a" /* ListPetugasPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_list_rekap_list_rekap__["a" /* ListRekapPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_profil_profil__["a" /* ProfilPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_detail_info_detail_info__["a" /* DetailInfoPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_detail_rekap_detail_rekap__["a" /* DetailRekapPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_local_weather_local_weather__["a" /* LocalWeatherPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_user_user__["a" /* userPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_nilai_nilai__["a" /* nilaiPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_choose_date_choose_date__["a" /* ChooseDatePage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_notifications_notifications__["a" /* NotificationsPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_evaluation_evaluation__["a" /* EvaluationPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_search_location_search_location__["a" /* SearchLocationPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_trip_detail_trip_detail__["a" /* TripDetailPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_trips_trips__["a" /* TripsPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_14__app_component__["a" /* MyApp */], {
                    scrollPadding: false,
                    scrollAssist: true,
                    autoFocusAssist: false
                }, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["a" /* IonicStorageModule */].forRoot({
                    name: '__ionic3_start_theme',
                    driverOrder: ['indexeddb', 'sqlite', 'websql']
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_14__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_15__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_checkout_trip_checkout_trip__["a" /* CheckoutTripPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_dashboard_dashboard__["a" /* DashboardPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_form_kinerja_form_kinerja__["a" /* FormKinerjaPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_form_user_form_user__["a" /* FormUserPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_form_rekap_form_rekap__["a" /* FormRekapPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_form_rank_form_rank__["a" /* FormRankPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_list_petugas_list_petugas__["a" /* ListPetugasPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_list_rekap_list_rekap__["a" /* ListRekapPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_detail_info_detail_info__["a" /* DetailInfoPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_detail_rekap_detail_rekap__["a" /* DetailRekapPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_profil_profil__["a" /* ProfilPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_local_weather_local_weather__["a" /* LocalWeatherPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_user_user__["a" /* userPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_nilai_nilai__["a" /* nilaiPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_choose_date_choose_date__["a" /* ChooseDatePage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_notifications_notifications__["a" /* NotificationsPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_evaluation_evaluation__["a" /* EvaluationPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_search_location_search_location__["a" /* SearchLocationPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_trip_detail_trip_detail__["a" /* TripDetailPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_trips_trips__["a" /* TripsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_keyboard__["a" /* Keyboard */],
                __WEBPACK_IMPORTED_MODULE_10__services_activity_service__["a" /* ActivityService */],
                __WEBPACK_IMPORTED_MODULE_11__services_data_services__["a" /* DataServices */],
                __WEBPACK_IMPORTED_MODULE_12__services_trip_service__["a" /* TripService */],
                __WEBPACK_IMPORTED_MODULE_6__globalController__["a" /* GlobalController */],
                __WEBPACK_IMPORTED_MODULE_13__services_weather__["a" /* WeatherProvider */],
                { provide: __WEBPACK_IMPORTED_MODULE_3__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_40__providers_rest_api_rest_api__["a" /* RestApiProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActivityService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mock_activities__ = __webpack_require__(335);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ActivityService = /** @class */ (function () {
    function ActivityService() {
        this.activities = __WEBPACK_IMPORTED_MODULE_1__mock_activities__["a" /* ACTIVITIES */];
    }
    ActivityService.prototype.getAll = function () {
        return this.activities;
    };
    ActivityService.prototype.getItem = function (id) {
        for (var i = 0; i < this.activities.length; i++) {
            if (this.activities[i].id === parseInt(id)) {
                return this.activities[i];
            }
        }
        return null;
    };
    ActivityService.prototype.remove = function (item) {
        this.activities.splice(this.activities.indexOf(item), 1);
    };
    ActivityService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], ActivityService);
    return ActivityService;
}());

//# sourceMappingURL=activity-service.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ACTIVITIES; });
var ACTIVITIES = [];
//# sourceMappingURL=mock-activities.js.map

/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TRIPS; });
var TRIPS = [
    {
        id: 1,
        name: "Copacabana Beach",
        price_adult: 60,
        price_child: 30,
        time: "12h",
        free_cancellation: 1,
        electric_voucher: 1,
        sub_name: "English Commentary Tour",
        thumb: "assets/img/trip/thumb/trip_1.jpg",
        description: "From sexy Ipanema and Copacabana, to more secluded and slightly lesser-known stretches of sand, like Prainha Beach, Brazil's Rio de Janeiro is best known for its beaches. Grab your sunscreen and Brazilian bikinis and head to the sunny shores of Rio's best beaches.",
        location: "Rio de Janeiro, Brazil",
        images: [
            "assets/img/trip/thumb/trip_5.jpg",
            "assets/img/trip/thumb/trip_6.jpg",
            "assets/img/trip/thumb/trip_7.jpg",
            "assets/img/trip/thumb/trip_8.jpg",
        ],
        highlights: [
            "Numerous kiosks",
            "First in a string of Atlantic Ocean-facing beaches",
            "Sand is flanked by mountains in the background",
            "Swing in the turquoise waters",
            "Water Sports",
        ]
    },
    {
        id: 2,
        name: "Christ the Redeemer",
        price_adult: 90,
        price_child: 45,
        time: "4h",
        free_cancellation: 1,
        electric_voucher: 1,
        sub_name: "English Commentary Tour",
        thumb: "assets/img/trip/thumb/trip_2.jpg",
        description: "From sexy Ipanema and Copacabana, to more secluded and slightly lesser-known stretches of sand, like Prainha Beach, Brazil's Rio de Janeiro is best known for its beaches. Grab your sunscreen and Brazilian bikinis and head to the sunny shores of Rio's best beaches.",
        location: "Rio de Janeiro, Brazil",
        images: [],
        highlights: []
    },
    {
        id: 3,
        name: "Ipiranga Museum",
        price_adult: 30,
        price_child: 15,
        time: "6h",
        free_cancellation: 1,
        electric_voucher: 1,
        sub_name: "English Commentary Tour",
        thumb: "assets/img/trip/thumb/trip_3.jpg",
        description: "From sexy Ipanema and Copacabana, to more secluded and slightly lesser-known stretches of sand, like Prainha Beach, Brazil's Rio de Janeiro is best known for its beaches. Grab your sunscreen and Brazilian bikinis and head to the sunny shores of Rio's best beaches.",
        location: "São Paulo, Brazil",
        images: [],
        highlights: []
    },
    {
        id: 4,
        name: "Fernando de Noronha",
        price_adult: 500,
        price_child: 250,
        time: "24h",
        free_cancellation: 1,
        electric_voucher: 1,
        sub_name: "English Commentary Tour",
        thumb: "assets/img/trip/thumb/trip_4.jpg",
        description: "From sexy Ipanema and Copacabana, to more secluded and slightly lesser-known stretches of sand, like Prainha Beach, Brazil's Rio de Janeiro is best known for its beaches. Grab your sunscreen and Brazilian bikinis and head to the sunny shores of Rio's best beaches.",
        location: "Fernando de Noronha, Brazil",
        images: [],
        highlights: []
    }
];
//# sourceMappingURL=mock-trips.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NotificationsPage = /** @class */ (function () {
    function NotificationsPage(viewCtrl) {
        this.viewCtrl = viewCtrl;
    }
    NotificationsPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    NotificationsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-notifications',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\notifications\notifications.html"*/'<ion-list class="no-margin">\n\n  <ion-list-header class="no-margin">\n\n  	<ion-icon name="notifications" color="primary"></ion-icon>\n\n  	<span ion-text color="primary" class="bold">Notifications</span>\n\n  </ion-list-header>\n\n  <button ion-item color="secondary" class="text-1x" tappable (click)="close()">\n\n  	<ion-icon name="mail"></ion-icon>\n\n  	New booking success!\n\n  </button>\n\n  <button ion-item color="secondary" class="text-1x" tappable (click)="close()">\n\n  	<ion-icon name="mail"></ion-icon>\n\n  	Activity rescheduled\n\n  </button>\n\n  <button ion-item class="text-1x" tappable (click)="close()">\n\n  	<ion-icon name="mail-open" color="secondary"></ion-icon>\n\n  	<span ion-text color="secondary">Activity rescheduled</span>\n\n  </button>\n\n  <button ion-item class="text-1x" tappable (click)="close()">\n\n  	<ion-icon name="mail-open" color="secondary"></ion-icon>\n\n  	<span ion-text color="secondary">Activity rescheduled</span>\n\n  </button>\n\n</ion-list>\n\n'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\notifications\notifications.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */]])
    ], NotificationsPage);
    return NotificationsPage;
}());

//# sourceMappingURL=notifications.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TripsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_trip_service__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__trip_detail_trip_detail__ = __webpack_require__(264);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TripsPage = /** @class */ (function () {
    function TripsPage(nav, tripService) {
        this.nav = nav;
        this.tripService = tripService;
        // set sample data
        this.trips = tripService.getAll();
    }
    // view trip detail
    TripsPage.prototype.viewDetail = function (id) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_3__trip_detail_trip_detail__["a" /* TripDetailPage */], { id: id });
    };
    TripsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-trips',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\trips\trips.html"*/'<!-- -->\n\n<ion-header>\n\n  <ion-navbar color="primary">\n\n    <ion-title>\n\n      <span ion-text>Activities</span>\n\n    </ion-title>\n\n  </ion-navbar>\n\n\n\n  <!--  -->\n\n  <ion-toolbar padding color="light">\n\n    <p ion-text no-margin class="text-white">\n\n      <strong>4</strong> results found!\n\n    </p>\n\n  </ion-toolbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding class="trips detail-bg">\n\n  <!--list of trips-->\n\n  <div class="trip card" *ngFor="let trip of trips" tappable (click)="viewDetail(trip.id)" margin-bottom>\n\n    <div class="background border-bottom" [ngStyle]="{\'background-image\': \'url(\' + trip.thumb + \')\'}">\n\n      <div class="background-filter rlt">\n\n        <div class="align-bottom" padding-left padding-right>\n\n          <h6 class="pull-left text-white" ion-text>{{ trip.name }}</h6>\n\n          <h6 class="pull-right text-white" ion-text>{{ trip.price_adult | currency:\'USD\':true }}</h6>\n\n          <div class="clear"></div>\n\n        </div>\n\n      </div>\n\n    </div>\n\n    <div class="padding-sm primary-bg">\n\n      <ion-icon name="time" class="text-white"></ion-icon>\n\n      <span ion-text class="text-white">{{ trip.time }}</span>\n\n      <span class="pull-right" ion-text color="light"><strong>per adult</strong> (childs has <span ion-text  class="text-green bold">50% OFF</span>)</span>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\trips\trips.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_trip_service__["a" /* TripService */]])
    ], TripsPage);
    return TripsPage;
}());

//# sourceMappingURL=trips.js.map

/***/ }),

/***/ 433:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(242);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_keyboard__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(81);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, keyboard) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.keyboard = keyboard;
        /* if(localStorage.getItem('email')){
          rootPage: any = HomePage;
        }else{
          rootPage: any = LoginPage;
        } */
        this.rootPage = this.setRootPage();
        this.user = localStorage.getItem('email');
        this.initializeApp();
        //this.appMenuItems = this.menuGenerated();
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            //*** Control Splash Screen
            // this.splashScreen.show();
            // this.splashScreen.hide();
            //*** Control Status Bar
            _this.statusBar.styleDefault();
            _this.statusBar.overlaysWebView(false);
            //*** Control Keyboard
            _this.keyboard.disableScroll(true);
        });
    };
    MyApp.prototype.setRootPage = function () {
        if (localStorage.getItem('token')) {
            return __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */];
        }
        else {
            return __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */];
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\ionic-project\appsu\src\app\app.html"*/'\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\ionic-project\appsu\src\app\app.html"*/
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicModule */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_keyboard__["a" /* Keyboard */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 435:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocalWeatherPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_weather__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { HttpErrorResponse } from '@angular/common/http';
var LocalWeatherPage = /** @class */ (function () {
    function LocalWeatherPage(navCtrl, weatherProvider, storage) {
        this.navCtrl = navCtrl;
        this.weatherProvider = weatherProvider;
        this.storage = storage;
        this.locationList = [
            { city: 'Los Angeles', state: 'CA' },
            { city: 'Miami', state: 'FL' },
            { city: 'New York', state: 'NY' },
            { city: 'Seattle', state: 'WA' }
        ];
    }
    LocalWeatherPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get('location').then(function (val) {
            if (val != null) {
                _this.location = JSON.parse(val);
            }
            else {
                _this.location = {
                    state: 'NY',
                    city: 'New York'
                };
            }
            _this.getWeather(_this.location);
        });
    };
    LocalWeatherPage.prototype.getWeather = function (location) {
        var _this = this;
        if (typeof location === 'string') {
            this.location = JSON.parse(location);
            console.log(this.location);
        }
        else {
            this.location = location;
        }
        this.weatherProvider.getWeather(this.location.state, this.location.city).subscribe(function (weather) {
            _this.weather = weather.current_observation;
        });
    };
    LocalWeatherPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-local-weather',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\local-weather\local-weather.html"*/'<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>Local Weather</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="common-bg">\n\n  <ion-card class="full-width" no-margin margin-bottom>\n\n    <ion-card-content no-padding>\n\n\n\n      <ion-item>\n\n        <ion-label class="text-1x bold">Select Local</ion-label>\n\n        <ion-select [(ngModel)]="location" (ionChange)="getWeather(location)">\n\n          <ion-option *ngFor="let location of locationList" [value]="location">{{ location.city }}</ion-option>\n\n        </ion-select>\n\n      </ion-item>\n\n\n\n    </ion-card-content>\n\n  </ion-card>\n\n\n\n  <ion-grid class="card" padding *ngIf="weather">\n\n    <ion-row>\n\n        <ion-col width-50 offset-25>\n\n            <h2 class="location text-dark">{{weather.display_location.full}}</h2>\n\n            <div class="icon"><img src="{{weather.icon_url}}" alt="weather"></div>\n\n            <h3 class="desc">{{weather.weather}}</h3>\n\n            <h1 class="temp">{{weather.temp_c}}&deg;</h1>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col width-100>\n\n            <ion-list>\n\n\n\n                <ion-item>\n\n                <strong>Temp:</strong> {{weather.temperature_string}}\n\n                </ion-item>\n\n                 <ion-item>\n\n                    <strong>Relative Humidity:</strong> {{weather.relative_humidity}}\n\n                </ion-item>\n\n                 <ion-item>\n\n                    <strong>Dewpoint:</strong> {{weather.dewpoint_string}}\n\n                </ion-item>\n\n                 <ion-item>\n\n                    <strong>Visibility:</strong> {{weather.visibility_km}}\n\n                </ion-item>\n\n                <ion-item>\n\n                    <strong>Heat Index:</strong> {{weather.heat_index_string}}\n\n                </ion-item>\n\n\n\n            </ion-list>\n\n        </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\local-weather\local-weather.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__services_weather__["a" /* WeatherProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], LocalWeatherPage);
    return LocalWeatherPage;
}());

//# sourceMappingURL=local-weather.js.map

/***/ }),

/***/ 436:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_rest_api_rest_api__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__notifications_notifications__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__form_kinerja_form_kinerja__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_globalController__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__trips_trips__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__search_location_search_location__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var MenuPage = /** @class */ (function () {
    function MenuPage(storage, restApi, nav, http, popoverCtrl, global) {
        this.storage = storage;
        this.restApi = restApi;
        this.nav = nav;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.global = global;
        // search condition
        this.search = {
            name: "Rio de Janeiro, Brazil",
            date: new Date().toISOString()
        };
        this.officerList = [];
        this.showSearch = false;
        this.page = 1;
        this.perpage = 0;
        this.totalData = 0;
        this.totalPage = 0;
        this.getOfficers();
        this.getUsers();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
    }
    MenuPage.prototype.showHideSearch = function () {
        this.showSearch = (this.showSearch) ? false : true;
    };
    MenuPage.prototype.getUsers = function () {
        var _this = this;
        this.restApi.getUsers(this.page)
            .subscribe(function (res) {
            console.log(res);
            _this.data = res;
            _this.users = _this.data.data;
            _this.perpage = _this.data.per_page;
            _this.totalData = _this.data.total;
            _this.totalPage = _this.data.total_pages;
        }, function (error) { return _this.errorMessage = error; });
    };
    MenuPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        this.page = this.page + 1;
        setTimeout(function () {
            _this.restApi.getUsers(_this.page)
                .subscribe(function (res) {
                _this.data = res;
                _this.perpage = _this.data.per_page;
                _this.totalData = _this.data.total;
                _this.totalPage = _this.data.total_pages;
                for (var i = 0; i < _this.data.data.length; i++) {
                    _this.users.push(_this.data.data[i]);
                }
            }, function (error) { return _this.errorMessage = error; });
            console.log('Async operation has ended');
            infiniteScroll.complete();
        }, 1000);
    };
    MenuPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then(function (val) {
            if (val === null) {
                _this.search.name = "Rio de Janeiro, Brazil";
            }
            else {
                _this.search.name = val;
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    MenuPage.prototype.getOfficers = function () {
        var _this = this;
        var json;
        this.http.get('http://localhost/api-v1/request/get_officers')
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            _this.officerList = json.data;
        })
            .subscribe(function (data) { }, function (error) {
            //this.global.toaster(error);
            console.log(error);
        });
    };
    // go to result page
    MenuPage.prototype.doSearch = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_10__trips_trips__["a" /* TripsPage */]);
    };
    // choose place
    MenuPage.prototype.choosePlace = function (from) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_11__search_location_search_location__["a" /* SearchLocationPage */], from);
    };
    // to go account page
    MenuPage.prototype.goToAccount = function (id) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_8__form_kinerja_form_kinerja__["a" /* FormKinerjaPage */], id);
    };
    MenuPage.prototype.presentNotifications = function (myEvent) {
        console.log(myEvent);
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_7__notifications_notifications__["a" /* NotificationsPage */]);
        popover.present({
            ev: myEvent
        });
    };
    MenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-menu',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\menu\menu.html"*/'<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <!-- <ion-title>{{title}}</ion-title> -->\n\n        <ion-searchbar ([ngModel])="terms" *ngIf="!showSearch"></ion-searchbar>\n\n        <ion-buttons end>\n\n        \n\n        <button ion-button tappable (click)="showHideSearch()">\n\n            <ion-icon name="search"></ion-icon>\n\n        </button>\n\n            <!-- <button ion-button tappable (click)="goToAccount()">\n\n        <ion-icon name="cog"></ion-icon>\n\n      </button> -->\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n  <ion-card>\n\n    <ion-card-header>\n\n      Infinity Scroll Example\n\n    </ion-card-header>\n\n    <ion-card-content>\n\n      <p>\n\n        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n\n        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis\n\n        aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint\n\n        occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n\n      </p>\n\n    </ion-card-content>\n\n\n\n  </ion-card>\n\n  <ion-list>\n\n    <ion-item *ngFor="let user of users">\n\n      <ion-avatar item-start>\n\n        <img src="{{user.avatar}}">\n\n      </ion-avatar>\n\n      <h2>{{user.first_name}} {{user.last_name}}</h2>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)" *ngIf="page < totalPage">\n\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data..."></ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\menu\menu.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_6__providers_rest_api_rest_api__["a" /* RestApiProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_9__app_globalController__["a" /* GlobalController */]])
    ], MenuPage);
    return MenuPage;
}());

//
//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchLocationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import {SearchCarsPage} from "../search-cars/search-cars";
var SearchLocationPage = /** @class */ (function () {
    function SearchLocationPage(storage, nav, navParams) {
        this.storage = storage;
        this.nav = nav;
        this.navParams = navParams;
        // places
        this.places = {
            nearby: [
                {
                    id: 1,
                    name: "Current Location"
                },
                {
                    id: 2,
                    name: "Rio de Janeiro, Brazil"
                },
                {
                    id: 3,
                    name: "São Paulo, Brazil"
                },
                {
                    id: 4,
                    name: "New York, United States"
                },
                {
                    id: 5,
                    name: "London, United Kingdom"
                },
                {
                    id: 6,
                    name: "Same as pickup"
                }
            ],
            recent: [
                {
                    id: 1,
                    name: "Rio de Janeiro"
                }
            ]
        };
        this.fromto = this.navParams.data;
    }
    // search by item
    SearchLocationPage.prototype.searchBy = function (item) {
        if (this.fromto === 'from') {
            this.storage.set('pickup', item.name);
        }
        if (this.fromto === 'to') {
            this.storage.set('dropOff', item.name);
        }
        // this.nav.push(SearchCarsPage);
        this.nav.pop();
    };
    SearchLocationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search-location',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\search-location\search-location.html"*/'<!-- # -->\n\n<ion-header>\n\n\n\n  <ion-navbar color="primary">\n\n    <ion-input placeholder="Enter Destination" padding-left autofocus></ion-input>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <div class="list-no-border">\n\n    <!--nearby places-->\n\n    <ion-item *ngFor="let item of places.nearby" tappable (click)="searchBy(item)">\n\n      <ion-icon name="md-locate" item-left color="primary"></ion-icon>\n\n      <span ion-text color="primary">{{ item.name }}</span>\n\n    </ion-item>\n\n    <!--recent places-->\n\n    <ion-item *ngFor="let item of places.recent" tappable (click)="searchBy(item)">\n\n      <ion-icon name="md-time" item-left color="primary"></ion-icon>\n\n      <span ion-text color="primary">{{ item.name }}</span>\n\n    </ion-item>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\search-location\search-location.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], SearchLocationPage);
    return SearchLocationPage;
}());

//# sourceMappingURL=search-location.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return userPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_weather__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__settings_settings__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__form_user_form_user__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_globalController__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










// import { HttpErrorResponse } from '@angular/common/http';
var userPage = /** @class */ (function () {
    function userPage(nav, weatherProvider, storage, http, global) {
        this.nav = nav;
        this.weatherProvider = weatherProvider;
        this.storage = storage;
        this.http = http;
        this.global = global;
        this.page = 1;
        this.perPage = 0;
        this.totalData = 0;
        this.totalPage = 0;
        this.title = global.getComponentInitial();
        this.getUsers();
    }
    userPage.prototype.getUsers = function (q) {
        var _this = this;
        if (q === void 0) { q = ''; }
        var json;
        var url = 'http://localhost/api-v1/request/get_users/' + q;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_users_page?query=' + q;
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            //console.log(json.data.total_page);
            _this.userList = json.data.data;
            console.log(_this.userList);
            _this.totalData = json.data.total_rows;
            _this.totalPage = json.data.total_page;
        })
            .subscribe(function (data) { }, function (error) {
            //this.global.toaster(error);
            console.log(error);
        });
    };
    userPage.prototype.searchTerm = function () {
        if (this.q) {
            this.getUsers(this.q);
        }
        else {
            this.getUsers();
        }
    };
    userPage.prototype.goToForm = function (id) {
        console.log(this.q);
        this.nav.push(__WEBPACK_IMPORTED_MODULE_8__form_user_form_user__["a" /* FormUserPage */], id);
    };
    userPage.prototype.goToView = function (id) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_7__settings_settings__["a" /* SettingsPage */], id);
    };
    userPage.prototype.getWeather = function (location) {
        var _this = this;
        if (typeof location === 'string') {
            this.location = JSON.parse(location);
            console.log(this.location);
        }
        else {
            this.location = location;
        }
        this.weatherProvider.getWeather(this.location.state, this.location.city).subscribe(function (weather) {
            _this.weather = weather.current_observation;
        });
    };
    userPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var json;
        this.page = this.page + 1;
        var url = 'http://localhost/api-v1/request/get_users/';
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_users_page?page=' + this.page;
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            //console.log(json.data.total_page);
            //this.userList = json.data.data;
            for (var i = 0; i < json.data.data.length; i++) {
                _this.userList.push(json.data.data[i]);
            }
            console.log(_this.page);
            console.log(_this.totalPage);
            _this.totalData = json.data.total_rows;
            _this.totalPage = json.data.total_page;
            setTimeout(function () {
                console.log('Async operation has ended');
                infiniteScroll.complete();
            }, 2000);
        })
            .subscribe(function (data) { }, function (error) {
            //this.global.toaster(error);
            console.log(error);
        });
    };
    userPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'user',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\user\user.html"*/'<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>{{title}}</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button tappable (click)="goToForm()">\n\n                <ion-icon name="add"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<!-- button -->\n\n<!-- <ion-fab right bottom #fab2>\n\n    <button ion-fab color="light"><ion-icon name="menu"></ion-icon></button>\n\n    <ion-fab-list side="top">\n\n        <button ion-fab (click)="goToForm()"><ion-icon name="add"></ion-icon></button>\n\n    </ion-fab-list>\n\n</ion-fab> -->\n\n\n\n<ion-content padding class="common-bg">\n\n    <ion-list>\n\n        <ion-card>\n\n            <ion-item>\n\n                <ion-icon name="search" item-start class="text-primary"></ion-icon>\n\n                <ion-input type="search" [(ngModel)]="q" (ionChange)="searchTerm()" ></ion-input>\n\n            </ion-item>\n\n        </ion-card>\n\n        <ion-card no-margin margin-bottom class="full-width" *ngFor="let user of userList" (click)="goToView(user.user_id)">\n\n            <ion-item>\n\n                <!-- <ion-icon name="person"></ion-icon> -->\n\n                \n\n                <h2>{{user.nama}}</h2>\n\n                <small>\n\n                    <ion-badge block>{{user.nama_jabatan}}</ion-badge>\n\n                    <ion-badge color="{{(user.is_active == \'1\') ? \'secondary\' : \'danger\'}}">{{(user.is_active == \'1\') ? \'Approve\' : \'Belum di Approve\'}}</ion-badge>\n\n                </small>\n\n                <br>\n\n                <label>\n\n                    <small>{{user.nik}}</small>\n\n                </label>\n\n                <p>Contact Person : {{user.telp}}</p>\n\n                <button ion-button clear tappable item-end>Lihat</button>\n\n            </ion-item>\n\n        </ion-card>\n\n    </ion-list>\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)" *ngIf="page < totalPage">\n\n        <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data..."></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n   \n\n</ion-content>'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\user\user.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__services_weather__["a" /* WeatherProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_9__app_globalController__["a" /* GlobalController */]])
    ], userPage);
    return userPage;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_globalController__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__form_user_form_user__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__user_user__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SettingsPage = /** @class */ (function () {
    function SettingsPage(nav, alertCtrl, navParams, http, global) {
        this.nav = nav;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.http = http;
        this.global = global;
        /* perlu di set tampungannya dlu */
        this.user = {};
        this.getPerson();
    }
    SettingsPage.prototype.getPerson = function () {
        var _this = this;
        var json;
        var url = 'http://localhost/api-v1/request/get_person/' + this.navParams.data;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_person/' + this.navParams.data;
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            _this.user = json.data;
        })
            .subscribe(function (data) { }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    SettingsPage.prototype.editUser = function (id) {
        console.log('test');
        this.nav.push(__WEBPACK_IMPORTED_MODULE_6__form_user_form_user__["a" /* FormUserPage */], id);
    };
    SettingsPage.prototype.hapusUser = function () {
        var _this = this;
        //this.global.toaster('hapus');
        var url = 'http://localhost/api-v1/request/delete_user/' + this.navParams.data;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/delete_user/' + this.navParams.data;
        this.http.post(remoteurl, null)
            .timeout(3000)
            .map(function (data) {
            _this.global.loader();
            var res = JSON.parse(data['_body']);
            if (res.status) {
                setTimeout(function () {
                    _this.global.toaster(res.message);
                }, 2000);
                setTimeout(function () {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__user_user__["a" /* userPage */]);
                }, 4000);
            }
            else {
                setTimeout(function () {
                    _this.global.toaster(res.message);
                }, 2000);
            }
        })
            .subscribe(function (data) {
        }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    SettingsPage.prototype.approveUser = function (id) {
        var _this = this;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/approve_user/' + id;
        this.http.post(remoteurl, null)
            .timeout(3000)
            .map(function (data) {
            _this.global.loader();
            var res = JSON.parse(data['_body']);
            if (res.status) {
                setTimeout(function () {
                    _this.global.toaster(res.message);
                }, 2000);
                setTimeout(function () {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__user_user__["a" /* userPage */]);
                }, 4000);
            }
            else {
                setTimeout(function () {
                    _this.global.toaster(res.message);
                }, 2000);
            }
        })
            .subscribe(function (data) {
        }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    SettingsPage.prototype.doConfirm = function (id, act) {
        var _this = this;
        if (act == 'hapusUser') {
            var text = 'Hapus data user';
            var msg = 'Apakah anda akan menghapus user ini?';
        }
        else if (act == 'approveUser') {
            var text = 'Approve user';
            var msg = 'Approve user ini?';
        }
        var alert = this.alertCtrl.create({
            title: text,
            message: msg,
            buttons: [
                {
                    text: 'Tidak',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Ya',
                    handler: function () {
                        console.log('Agree clicked');
                        if (act == 'hapusUser') {
                            _this.hapusUser();
                        }
                        else if (act == 'approveUser') {
                            _this.approveUser(id);
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    /* reserved */
    SettingsPage.prototype.setStatusUser = function (id, stat) {
    };
    SettingsPage.prototype.deleteUser = function (id) {
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-settings',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\settings\settings.html"*/'<!-- -->\n\n<!-- <ion-header class="no-shadow">\n\n  <ion-navbar class="no-border">\n\n    <ion-title>\n\n      <ion-icon name="person" class="text-primary"></ion-icon>\n\n      <span class="text-primary">{{user.nama}}</span>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header> -->\n\n<ion-header>\n\n  <ion-navbar color="primary">\n\n    <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n    <ion-title>\n\n      <ion-icon name="person" class="text-primary"></ion-icon>\n\n      <span>{{user.nama}}</span>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="common-bg">\n\n  \n\n  <ion-grid class="card" padding>\n\n    <ion-row>\n\n      <ion-col width-50 offset-25>\n\n        <!-- <h2 class="location text-dark"></h2>\n\n        <div class="icon"><img src="" alt="weather"></div>\n\n        <h3 class="desc"></h3> -->\n\n        <h1 class="temp" text-center>Data User</h1>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col width-100>\n\n        <ion-list>\n\n          <ion-item>\n\n            <div text-left>\n\n              <strong>Nama: {{user.nama}}</strong> \n\n            </div>\n\n          </ion-item>\n\n          <ion-item>\n\n            <div text-left>\n\n              <strong>TTL: {{user.tgl_lahir}}</strong>\n\n            </div>\n\n          </ion-item>\n\n          <ion-item>\n\n            <div text-left>\n\n              <strong>NIK: {{user.nik}}</strong> \n\n            </div>\n\n          </ion-item>\n\n          <ion-item>\n\n            <div text-left>\n\n              <strong>Alamat: {{user.alamat}}</strong> \n\n            </div>\n\n          </ion-item>\n\n          <ion-item>\n\n            <div text-left>\n\n              <strong>telp: {{user.telp}}</strong> \n\n            </div>\n\n          </ion-item>\n\n          <ion-item>\n\n            <button *ngIf="user.is_active == \'0\'" ion-button color="light" (click)="doConfirm(user.user_id,\'approveUser\')">Approve</button>\n\n            <button ion-button color="secondary" text-center tappable (click)="editUser(user.user_id)"><ion-icon name="construct"></ion-icon> Edit\n\n            </button>\n\n            <button ion-button color="danger" (click)="doConfirm(user.user_id, \'hapusUser\')">Hapus</button>\n\n          </ion-item>\n\n        </ion-list>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  \n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\settings\settings.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_5__app_globalController__["a" /* GlobalController */]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return nilaiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__notifications_notifications__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__choose_date_choose_date__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_globalController__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__trips_trips__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__search_location_search_location__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var nilaiPage = /** @class */ (function () {
    function nilaiPage(storage, nav, http, popoverCtrl, global) {
        this.storage = storage;
        this.nav = nav;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.global = global;
        this.page = 1;
        this.perPage = 0;
        this.totalData = 0;
        this.totalPage = 0;
        this.officerList = [];
        this.getOfficers();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
    }
    nilaiPage.prototype.getOfficers = function (q) {
        var _this = this;
        if (q === void 0) { q = ''; }
        var json;
        var url = 'http://localhost/api-v1/request/get_officers/' + localStorage.getItem('user_id') + '/' + q;
        //var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers/'+localStorage.getItem('user_id')+'/'+q;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers_page?id_staff=' + localStorage.getItem('user_id') + '&query=' + q;
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            _this.officerList = json.data.data;
            _this.totalData = json.data.total_rows;
            _this.totalPage = json.data.total_page;
        })
            .subscribe(function (data) { }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    nilaiPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var json;
        this.page = this.page + 1;
        ;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers_page?id_staff=' + localStorage.getItem('user_id') + '&page=' + this.page;
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            //console.log(json.data.total_page);
            for (var i = 0; i < json.data.data.length; i++) {
                _this.officerList.push(json.data.data[i]);
            }
            console.log(_this.page);
            console.log(_this.totalPage);
            _this.totalData = json.data.total_rows;
            _this.totalPage = json.data.total_page;
            setTimeout(function () {
                console.log('Async operation has ended');
                infiniteScroll.complete();
            }, 2000);
        })
            .subscribe(function (data) { }, function (error) {
            //this.global.toaster(error);
            console.log(error);
        });
    };
    nilaiPage.prototype.searchTerm = function () {
        console.log(this.q);
        if (this.q) {
            this.getOfficers(this.q);
        }
        else {
            this.getOfficers();
        }
    };
    nilaiPage.prototype.valueMean = function (event, criteria) {
        console.log(event);
        console.log(criteria);
    };
    // go to result page
    nilaiPage.prototype.doSearch = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_9__trips_trips__["a" /* TripsPage */]);
    };
    // choose place
    nilaiPage.prototype.choosePlace = function (from) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_10__search_location_search_location__["a" /* SearchLocationPage */], from);
    };
    // to go account page
    nilaiPage.prototype.goToForm = function (id) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_7__choose_date_choose_date__["a" /* ChooseDatePage */], id);
    };
    nilaiPage.prototype.presentNotifications = function (myEvent) {
        console.log(myEvent);
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_6__notifications_notifications__["a" /* NotificationsPage */]);
        popover.present({
            ev: myEvent
        });
    };
    nilaiPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-nilai',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\nilai\nilai.html"*/'<!-- -->\n\n<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>\n\n            {{title}}\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="animated fadeIn common-bg">\n\n\n\n    <ion-list>    \n\n        <ion-card>\n\n            <ion-item>\n\n                <ion-icon name="search" item-start class="text-primary"></ion-icon>\n\n                <ion-input type="search" [(ngModel)]="q" (ionChange)="searchTerm()"></ion-input>\n\n            </ion-item>\n\n        </ion-card>\n\n        <ion-card no-margin margin-bottom class="full-width" *ngFor="let user of officerList" (click)="goToForm(user.user_id)">\n\n            <ion-item>\n\n                <h2><strong>{{user.nama}}</strong></h2>\n\n                <h3>{{user.nik}}</h3>\n\n                <h5>Contact Person: {{user.telp}}</h5>\n\n                <span><ion-icon name="pie"></ion-icon> <small>Penilaian Bulan ini: </small><strong>{{(user.total)?user.total:\'-\'}}</strong></span>\n\n                <h5><ion-icon name="person"></ion-icon> <small>Penilai: </small><strong>{{(user.nama_staff)?user.nama_staff:\'-\'}}</strong></h5>\n\n                <button ion-button clear item-end>{{(user.total)?\'Ubah Nilai\':\'Beri Nilai\'}}</button>\n\n            </ion-item>\n\n        </ion-card>\n\n    </ion-list>\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)" *ngIf="page < totalPage">\n\n        <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data..."></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\nilai\nilai.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_8__app_globalController__["a" /* GlobalController */]])
    ], nilaiPage);
    return nilaiPage;
}());

//
//# sourceMappingURL=nilai.js.map

/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__notifications_notifications__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__settings_settings__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_data_services__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_globalController__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__trips_trips__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__search_location_search_location__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_nilai_nilai__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_dashboard_dashboard__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_list_rekap_list_rekap__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_detail_info_detail_info__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_login_login__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_profil_profil__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_user_user__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_evaluation_evaluation__ = __webpack_require__(272);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_form_rank_form_rank__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_form_rekap_form_rekap__ = __webpack_require__(147);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






















var HomePage = /** @class */ (function () {
    function HomePage(storage, global, nav, popoverCtrl, http, data) {
        this.storage = storage;
        this.global = global;
        this.nav = nav;
        this.popoverCtrl = popoverCtrl;
        this.http = http;
        this.data = data;
        this.search = {
            name: "Rio de Janeiro, Brazil",
            date: new Date().toISOString()
        };
        this.user = localStorage.getItem('email');
        //this.rootPage = DashboardPage;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_13__pages_dashboard_dashboard__["a" /* DashboardPage */];
        this.nilaiPage = __WEBPACK_IMPORTED_MODULE_12__pages_nilai_nilai__["a" /* nilaiPage */];
        this.userPage = __WEBPACK_IMPORTED_MODULE_18__pages_user_user__["a" /* userPage */];
        this.menuGenerated();
    }
    HomePage_1 = HomePage;
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then(function (val) {
            if (val === null) {
                _this.search.name = "Rio de Janeiro, Brazil";
            }
            else {
                _this.search.name = val;
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    // go to result page
    HomePage.prototype.doSearch = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_10__trips_trips__["a" /* TripsPage */]);
    };
    // choose place
    HomePage.prototype.choosePlace = function (from) {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_11__search_location_search_location__["a" /* SearchLocationPage */], from);
    };
    // to go account page
    HomePage.prototype.goToAccount = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_7__settings_settings__["a" /* SettingsPage */]);
    };
    HomePage.prototype.presentNotifications = function (myEvent) {
        console.log(myEvent);
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_6__notifications_notifications__["a" /* NotificationsPage */]);
        popover.present({
            ev: myEvent
        });
    };
    HomePage.prototype.setRootPage = function () {
        if (!localStorage.getItem('email')) {
            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_16__pages_login_login__["a" /* LoginPage */]);
            //return LoginPage;
        }
    };
    /* routes */
    HomePage.prototype.openPage = function (page) {
        var component;
        if (page === "HomePage") {
            page = { menu: 'Home', component: 'HomePage' };
        }
        localStorage.setItem('component', page.menu);
        component = this.determineFn(page.component);
        if (component) {
            this.rootPage = component;
        }
        else {
            this.global.toaster('cannot decided component!');
        }
    };
    /* logout */
    HomePage.prototype.logout = function () {
        localStorage.clear();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_16__pages_login_login__["a" /* LoginPage */]);
    };
    HomePage.prototype.menuGenerated = function () {
        var _this = this;
        var json;
        var role = localStorage.getItem('role');
        var url = 'http://localhost/api-v1/request/get_menus/' + role;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_menus/' + role;
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            _this.appMenuItems = json.data;
        })
            .subscribe(function (data) { }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    HomePage.prototype.determineFn = function (str) {
        var component;
        if (str == 'HomePage') {
            component = HomePage_1;
        }
        else if (str == 'userPage') {
            component = __WEBPACK_IMPORTED_MODULE_18__pages_user_user__["a" /* userPage */];
        }
        else if (str == 'nilaiPage') {
            component = __WEBPACK_IMPORTED_MODULE_12__pages_nilai_nilai__["a" /* nilaiPage */];
        }
        else if (str == 'ProfilPage') {
            component = __WEBPACK_IMPORTED_MODULE_17__pages_profil_profil__["a" /* ProfilPage */];
        }
        else if (str == 'FormRankPage') {
            component = __WEBPACK_IMPORTED_MODULE_20__pages_form_rank_form_rank__["a" /* FormRankPage */];
        }
        else if (str == 'ListRekapPage') {
            component = __WEBPACK_IMPORTED_MODULE_14__pages_list_rekap_list_rekap__["a" /* ListRekapPage */];
        }
        else if (str == 'FormRekapPage') {
            component = __WEBPACK_IMPORTED_MODULE_21__pages_form_rekap_form_rekap__["a" /* FormRekapPage */];
        }
        else if (str == 'DetailInfoPage') {
            component = __WEBPACK_IMPORTED_MODULE_15__pages_detail_info_detail_info__["a" /* DetailInfoPage */];
        }
        else if (str == 'EvaluationPage') {
            component = __WEBPACK_IMPORTED_MODULE_19__pages_evaluation_evaluation__["a" /* EvaluationPage */];
        }
        else {
            component = false;
        }
        return component;
    };
    HomePage = HomePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\home\home.html"*/'<ion-menu side="left" id="authenticated" [content]="content">\n\n  \n\n  <ion-content color="primary">\n\n\n\n    <ion-list class="user-list">\n\n      <button ion-item menuClose class="text-1x" (click)="openPage(\'HomePage\')">\n\n        <ion-icon item-left name="home" color="primary"></ion-icon>\n\n        <span ion-text color="primary">Home</span>\n\n      </button>\n\n      <button ion-item menuClose class="text-1x" *ngFor="let menuItem of appMenuItems" (click)="openPage(menuItem)">\n\n        <ion-icon item-left [name]="menuItem.icon" color="primary"></ion-icon>\n\n        <span ion-text color="primary">{{menuItem.menu}}</span>\n\n      </button>\n\n      <button ion-item menuClose class="text-1x" (click)="logout()">\n\n        <ion-icon item-left name="log-out" color="primary"></ion-icon>\n\n        <span ion-text color="primary">Log Out</span>\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n\n\n\n'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_9__app_globalController__["a" /* GlobalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_8__services_data_services__["a" /* DataServices */]])
    ], HomePage);
    return HomePage;
    var HomePage_1;
}());

//
//# sourceMappingURL=home.js.map

/***/ }),

/***/ 76:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TripService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mock_trips__ = __webpack_require__(336);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TripService = /** @class */ (function () {
    function TripService() {
        this.trips = __WEBPACK_IMPORTED_MODULE_1__mock_trips__["a" /* TRIPS */];
    }
    TripService.prototype.getAll = function () {
        return this.trips;
    };
    TripService.prototype.getItem = function (id) {
        for (var i = 0; i < this.trips.length; i++) {
            if (this.trips[i].id === parseInt(id)) {
                return this.trips[i];
            }
        }
        return null;
    };
    TripService.prototype.remove = function (item) {
        this.trips.splice(this.trips.indexOf(item), 1);
    };
    TripService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], TripService);
    return TripService;
}());

//# sourceMappingURL=trip-service.js.map

/***/ }),

/***/ 80:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailInfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_data_services__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_user_user__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_nilai_nilai__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_globalController__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/* import { NotificationsPage } from "../notifications/notifications";
import { SettingsPage } from "../settings/settings"; */

/* import { TripsPage } from "../trips/trips";
import { SearchLocationPage } from "../search-location/search-location"; */
var DetailInfoPage = /** @class */ (function () {
    function DetailInfoPage(storage, navParams, nav, http, popoverCtrl, data, global) {
        this.storage = storage;
        this.navParams = navParams;
        this.nav = nav;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.data = data;
        this.global = global;
        // search condition
        this.shownGroup = null;
        this.detail = {};
        this.getData();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        this.param = JSON.parse(atob(this.navParams.data));
        console.log(this.param);
        if (localStorage.getItem('role') == '2') {
            var d = new Date();
            this.month = this.global.getMonth(d.getMonth());
        }
        else {
            var date = this.param.date_nilai.split('-');
            this.month = this.global.getMonth(date[1]);
        }
    }
    DetailInfoPage.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        }
        else {
            this.shownGroup = group;
        }
    };
    DetailInfoPage.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    DetailInfoPage.prototype.getData = function () {
        var _this = this;
        var json;
        var params;
        if (localStorage.getItem('role') == '2') {
            var d = new Date();
            //params = btoa(d.getFullYear() + '-' + this.global.setMonth(d.getMonth())+'|'+localStorage.getItem('user_id')).replace("=","");
            var content = {
                'date_nilai': d.getFullYear() + '-' + this.global.setMonth(d.getMonth()),
                'id': localStorage.getItem('user_id')
            };
            params = btoa(JSON.stringify(params));
        }
        else {
            params = this.navParams.data;
        }
        var url = 'http://localhost/api-v1/request/get_history/' + params;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_history/' + params;
        this.http.get(remoteurl)
            .map(function (res) {
            //this.global.loader();
            json = res.json();
            _this.detail = json.data;
            /* if (localStorage.getItem('role') == '2') {
                this.detail.nama = localStorage.getItem('component');
            } */
        })
            .subscribe(function (data) { }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    DetailInfoPage.prototype.goToUser = function () {
        localStorage.setItem('component', 'User');
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_user_user__["a" /* userPage */]);
    };
    DetailInfoPage.prototype.goToNilai = function () {
        localStorage.setItem('component', 'Penilaian');
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_nilai_nilai__["a" /* nilaiPage */]);
    };
    DetailInfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dashboard',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\detail-info\detail-info.html"*/'<!-- -->\n\n<ion-header>\n\n    <ion-navbar color="primary">\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>\n\n            {{detail.nama}}\n\n        </ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="animated fadeIn common-bg">\n\n    <ion-card>\n\n        <ion-card-content>\n\n            <label>Penilaian Bulan {{month}}</label>\n\n        </ion-card-content>\n\n        <ion-item (click)="toggleGroup(1)" [ngClass]="{active : isGroupShown(1)}">\n\n            <ion-icon item-start color="success" item-right [name]="isGroupShown(1) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n            Keselamatan Kerja\n\n            <ion-badge item-end>{{detail.K3}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item *ngIf="isGroupShown(1)" text-wrap>\n\n            <p><small>{{detail.K3_ket}}</small></p>\n\n        </ion-item>\n\n        <ion-item (click)="toggleGroup(2)" [ngClass]="{active : isGroupShown(2)}">\n\n            <ion-icon item-start color="success" item-right [name]="isGroupShown(2) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n            Disiplin\n\n            <ion-badge item-end>{{detail.DIS}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item *ngIf="isGroupShown(2)" text-wrap>\n\n            <p><small>{{detail.DIS_ket}}</small></p>\n\n        </ion-item>\n\n        <ion-item (click)="toggleGroup(3)" [ngClass]="{active : isGroupShown(3)}">\n\n            <ion-icon item-start color="success" item-right [name]="isGroupShown(3) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n            Produktifitas Kerja\n\n            <ion-badge item-end>{{detail.PRK}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item *ngIf="isGroupShown(3)" text-wrap>\n\n            <p><small>{{detail.PRK_ket}}</small></p>\n\n        </ion-item>\n\n        <ion-item (click)="toggleGroup(4)" [ngClass]="{active : isGroupShown(4)}">\n\n            <ion-icon item-start color="success" item-right [name]="isGroupShown(4) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n            Kerjasama Tim\n\n            <ion-badge item-end>{{detail.TMW}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item *ngIf="isGroupShown(4)" text-wrap>\n\n            <p><small>{{detail.TMW_ket}}</small></p>\n\n        </ion-item>\n\n        <ion-item (click)="toggleGroup(5)" [ngClass]="{active : isGroupShown(5)}">\n\n            <ion-icon item-start color="success" item-right [name]="isGroupShown(5) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n            Etika/Perilaku\n\n            <ion-badge item-end>{{detail.ATT}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item *ngIf="isGroupShown(5)" text-wrap>\n\n            <p><small>{{detail.ATT_ket}}</small></p>\n\n        </ion-item>\n\n        <ion-item (click)="toggleGroup(0)" [ngClass]="{active : isGroupShown(0)}">\n\n            <ion-icon item-start color="success" item-right [name]="isGroupShown(0) ? \'arrow-dropdown\' : \'arrow-dropright\'"></ion-icon>\n\n            <strong>Total</strong>\n\n            <ion-badge item-end>{{detail.total}}</ion-badge>\n\n        </ion-item>\n\n        <ion-item *ngIf="isGroupShown(0)" text-wrap>\n\n            <p><small>{{detail.total_ket}}</small></p>\n\n        </ion-item>\n\n\n\n    </ion-card>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\detail-info\detail-info.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* PopoverController */], __WEBPACK_IMPORTED_MODULE_6__services_data_services__["a" /* DataServices */], __WEBPACK_IMPORTED_MODULE_9__app_globalController__["a" /* GlobalController */]])
    ], DetailInfoPage);
    return DetailInfoPage;
}());

//
//# sourceMappingURL=detail-info.js.map

/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_globalController__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__home_home__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__register_register__ = __webpack_require__(270);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var LoginPage = /** @class */ (function () {
    /* private loginForm:any; */
    function LoginPage(nav, http, forgotCtrl, menu, formBuilder, toastCtrl, global) {
        this.nav = nav;
        this.http = http;
        this.forgotCtrl = forgotCtrl;
        this.menu = menu;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.global = global;
        this.loginForm = {};
        this.menu.swipeEnable(false);
        /*
         * validator
         */
        this.logins = this.formBuilder.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            pass: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
        });
        // rule but not mandatory
        /* this.loginForm = {
          email:'',
          pass:''
        } */
        /* this.loginForm = {}; */
    }
    // go to register page
    LoginPage.prototype.register = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__register_register__["a" /* RegisterPage */]);
    };
    // login and go to home page
    LoginPage.prototype.login = function () {
        var _this = this;
        var url = 'http://localhost/api-v1/auth/verify';
        var remoteurl = 'http://bms.uphero.com/api-v1/auth/verify';
        var header = '';
        var data = this.logins.value;
        var res;
        this.http.post(remoteurl, JSON.stringify(data))
            .timeout(3000)
            .map(function (data) {
            _this.global.loader();
            res = JSON.parse(data['_body']);
            if (res.status) {
                setTimeout(function () {
                    _this.global.toaster(res.message);
                }, 2000);
                localStorage.setItem('name', res.data.name);
                localStorage.setItem('user_id', res.data.user_id);
                localStorage.setItem('nip', res.data.nip);
                localStorage.setItem('role', res.data.role);
                localStorage.setItem('telp', res.data.telp);
                localStorage.setItem('token', res.data.token);
                setTimeout(function () {
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__home_home__["a" /* HomePage */]);
                }, 4000);
            }
            else {
                setTimeout(function () {
                    _this.global.toaster(res.message);
                }, 2000);
            }
        })
            .subscribe(function (data) {
        }, function (error) {
            var text = (!error.status) ? '500 Internal Server Error' : 'Unexpected Error Found';
            _this.global.toaster(text);
        });
    };
    LoginPage.prototype.forgotPass = function () {
        var _this = this;
        var forgot = this.forgotCtrl.create({
            title: 'Forgot Password?',
            message: "Enter you email address to send a reset link password.",
            inputs: [
                {
                    name: 'email',
                    placeholder: 'Email',
                    type: 'email'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Send',
                    handler: function (data) {
                        console.log('Send clicked');
                        var toast = _this.toastCtrl.create({
                            message: 'Email was sended successfully',
                            duration: 3000,
                            position: 'top',
                            cssClass: 'dark-trans',
                            closeButtonText: 'OK',
                            showCloseButton: true
                        });
                        toast.present();
                    }
                }
            ]
        });
        forgot.present();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\ionic-project\appsu\src\pages\login\login.html"*/'<!-- -->\n\n<ion-content padding class="animated fadeIn login auth-page">\n\n  <div class="login-content">\n\n\n\n    <!-- Logo -->\n\n    <div padding-horizontal text-center class="animated fadeInDown">\n\n      <div class="logo"></div>\n\n      <h2 ion-text class="text-primary">\n\n        <strong>Penilaian Kinerja Petugas PPSU</strong>\n\n      </h2>\n\n    </div>\n\n\n\n    <form class="list-form" [formGroup]="logins">\n\n      <ion-item>\n\n        <ion-label floating>\n\n          <ion-icon name="person" item-start class="text-primary"></ion-icon>\n\n          Username\n\n        </ion-label>\n\n        <ion-input type="email" formControlName="email"></ion-input>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <ion-label floating>\n\n          <ion-icon name="lock" item-start class="text-primary"></ion-icon>\n\n          Password\n\n        </ion-label>\n\n        <ion-input type="password" formControlName="pass" ></ion-input>\n\n      </ion-item>\n\n      <button ion-button icon-start block color="dark" tappable (click)="login()" [disabled]="!logins.valid"><ion-icon name="log-in"></ion-icon>\n\n      SIGN IN</button>\n\n    </form>\n\n    \n\n\n\n    <!-- Login form -->\n\n    <!-- <form class="list-form">\n\n      <ion-item>\n\n        <ion-label floating>\n\n          <ion-icon name="mail" item-start class="text-primary"></ion-icon>\n\n          Email\n\n        </ion-label>\n\n        <ion-input type="email" name="email" [(ngModel)]="loginForm.email"></ion-input>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <ion-label floating>\n\n          <ion-icon name="lock" item-start class="text-primary"></ion-icon>\n\n          Password\n\n        </ion-label>\n\n        <ion-input type="password" name="pass" [(ngModel)]="loginForm.pass"></ion-input>\n\n      </ion-item>\n\n    </form>\n\n\n\n    <p text-right ion-text color="secondary" tappable (click)="forgotPass()"><strong>Forgot Password?</strong></p>\n\n\n\n    <div>\n\n      <button ion-button icon-start block color="dark" tappable (click)="login()">\n\n        <ion-icon name="log-in"></ion-icon>\n\n        SIGN IN\n\n      </button> -->\n\n\n\n      <!-- <p text-center ion-text color="secondary">Or Sign in with:</p>\n\n\n\n      <ion-grid>\n\n        <ion-row>\n\n          <ion-col col-4>\n\n            <button ion-button icon-only block class="btn-facebook">\n\n              <ion-icon name="logo-facebook"></ion-icon>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col col-4>\n\n            <button ion-button icon-only block class="btn-twitter">\n\n              <ion-icon name="logo-twitter"></ion-icon>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col col-4>\n\n            <button ion-button icon-only block class="btn-gplus">\n\n              <ion-icon name="logo-googleplus"></ion-icon>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid> \n\n\n\n    </div>-->\n\n\n\n\n\n    <!-- Other links -->\n\n    <div text-center margin-top>\n\n      <span ion-text color="secondary" tappable (click)="register()">Petugas Baru? <strong>Sign up</strong></span>\n\n    </div>\n\n\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\ionic-project\appsu\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* ToastController */], __WEBPACK_IMPORTED_MODULE_6__app_globalController__["a" /* GlobalController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[275]);
//# sourceMappingURL=main.js.map