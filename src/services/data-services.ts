import { Injectable } from "@angular/core";

import { TripsPage } from "../pages/trips/trips";
import { SearchLocationPage } from "../pages/search-location/search-location";
import { nilaiPage } from "../pages/nilai/nilai";
import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { userPage } from "../pages/user/user";

@Injectable()
export class DataServices {
    private activities: any;

    constructor() {
        
    }

    dataUser(){
        return [
            { id: 1, name: 'Bima Jaya', sex: 'L', address: 'Jl. Merdeka no 17' },
            { id: 2, name: 'Andre Taulany', sex: 'L', address: 'Jl. Maju mundur no 13' }
        ];
    }

    /* menuGenerated() {
        var menuArray = [];

        if (localStorage.getItem('email') == 'admin') {
            menuArray = [
                { title: 'Home', component: HomePage, icon: 'home' },
                { title: 'User', component: userPage, icon: 'person' },
                { title: 'Menu', component: userPage, icon: 'list' },
                { title: 'Jabatan', component: userPage, icon: 'person' },
                { title: 'Penilaian', component: nilaiPage, icon: 'stats' }
            ];
        } else if (localStorage.getItem('email') == 'staff') {
            menuArray = [
                { title: 'Home', component: HomePage, icon: 'home' },
                { title: 'Profil', component: HomePage, icon: 'home' },
                { title: 'Penilaian', component: nilaiPage, icon: 'stats' }
            ];
        } else if (localStorage.getItem('email') == 'petugas') {
            menuArray = [
                { title: 'Home', component: HomePage, icon: 'home' },
                { title: 'Profil', component: HomePage, icon: 'home' },
                { title: 'Penilaian', component: nilaiPage, icon: 'stats' }
            ];
        } else if (localStorage.getItem('email') == 'pimpinan') {
            menuArray = [
                { title: 'Home', component: HomePage, icon: 'home' },
                { title: 'Profil', component: HomePage, icon: 'home' },
                { title: 'Penilaian', component: nilaiPage, icon: 'stats' }
            ];
        }

        return menuArray;
    } */
}
