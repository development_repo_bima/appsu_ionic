import { Injectable } from '@angular/core';
import { ToastController, AlertController, Platform, LoadingController } from 'ionic-angular';
/* import { AppConfig } from './appconfig'; */

@Injectable()
export class GlobalController {
    constructor(private toast:ToastController, private loading:LoadingController){}

    toaster(msg){
        let toast = this.toast.create({
            message: msg,
            duration: 3000,
            position: 'top',
            cssClass: 'dark-trans',
            closeButtonText: 'OK',
            showCloseButton: true
        });
        toast.present();
    }

    loader(){
        let loader = this.loading.create({
            content: "Tunggu Sebentar",
            duration: 3000
        })
        loader.present();
    }

    getComponentInitial(){
        if(localStorage.getItem('component')){
            return localStorage.getItem('component')
        }else{
            return "Home";
        }
    }

    getMonth(init){
        if(typeof init == 'string'){
            var month = {
                "01":"Januari",
                "02":"Februari",
                "03":"Maret",
                "04":"April",
                "05":"Mei",
                "06":"Juni",
                "07":"Juli",
                "08":"Agustus",
                "09":"September",
                "10":"Oktober",
                "11":"November",
                "12":"Desember",
            };
            return month[init];
        }else{
            var month2 = [];
            month2 = [
                "Januari",
                "Februari",
                "Maret",
                "April",
                "Mei",
                "Juni",
                "Juli",
                "Agustus",
                "September",
                "Oktober",
                "November",
                "Desember",
            ];
            return month2[init]
        }
    }

    rangeCreator(month1 = 1, month2 = 1){
        var range = [];
        for(var i = month1; i<= month2; i++){
            var obj = {
                month:this.getMonth((i-1)),
                init : i
            }
            range.push(obj); 
        }
        return range;
    }

    setMonth(str){
        var actualMonth = str + 1;
        
        if(actualMonth.toString().length == 1){
            actualMonth = '0'+actualMonth.toString();
        }
        
        return actualMonth;
    }
    
}