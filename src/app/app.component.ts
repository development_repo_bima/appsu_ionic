import { Component, ViewChild } from "@angular/core";
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Platform, Nav } from "ionic-angular";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';

import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { DashboardPage } from "../pages/dashboard/dashboard";
import { LocalWeatherPage } from "../pages/local-weather/local-weather";
import { userPage } from "../pages/user/user";
import { nilaiPage } from "../pages/nilai/nilai";

export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}

@Component({
  templateUrl: 'app.html'
})

@NgModule({
  imports: [IonicModule]
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  /* if(localStorage.getItem('email')){
    rootPage: any = HomePage;  
  }else{
    rootPage: any = LoginPage;
  } */
  
  rootPage: any = this.setRootPage();
  user: any = localStorage.getItem('email');
  appMenuItems: Array<MenuItem>;
  

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public keyboard: Keyboard
  ) {
    this.initializeApp();

    //this.appMenuItems = this.menuGenerated();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.

      //*** Control Splash Screen
      // this.splashScreen.show();
      // this.splashScreen.hide();

      //*** Control Status Bar
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);

      //*** Control Keyboard
      this.keyboard.disableScroll(true);
    });
  }

  setRootPage(){
    if (localStorage.getItem('token')){
      return HomePage;
    }else{
      return LoginPage;
    }
  }

  /* openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout() {
    localStorage.clear();
    this.nav.setRoot(LoginPage);
  } */

  /* menuGenerated(){
    var menuArray = [];
    if (localStorage.getItem('email') == 'admin'){
      menuArray = [
        { title: 'Home', component: HomePage, icon: 'home' },
        { title: 'Local Weather', component: LocalWeatherPage, icon: 'partly-sunny' },
        { title: 'Petugas', component: userPage, icon: 'person' },
        { title: 'Penilaian', component: nilaiPage, icon: 'stats' }
      ];
    } else if (localStorage.getItem('email') == 'staff'){
      menuArray = [
        { title: 'Home', component: HomePage, icon: 'home' },
        { title: 'Penilaian', component: nilaiPage, icon: 'stats' }
      ];
    }
    console.log(menuArray);
    return menuArray;
  } */

}
