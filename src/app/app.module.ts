import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';

import {GlobalController} from './globalController';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Keyboard} from '@ionic-native/keyboard';

import {ActivityService} from "../services/activity-service";
import {DataServices} from "../services/data-services";
import {TripService} from "../services/trip-service";
import {WeatherProvider} from "../services/weather";

import {MyApp} from "./app.component";

import {SettingsPage} from "../pages/settings/settings";
import {CheckoutTripPage} from "../pages/checkout-trip/checkout-trip";
import {HomePage} from "../pages/home/home";
import {DashboardPage} from "../pages/dashboard/dashboard";
import {LoginPage} from "../pages/login/login";
import {FormKinerjaPage} from "../pages/form-kinerja/form-kinerja";
import {FormUserPage} from "../pages/form-user/form-user";
import {FormRekapPage} from "../pages/form-rekap/form-rekap";
import {FormRankPage} from "../pages/form-rank/form-rank";
import {ListPetugasPage} from "../pages/list-petugas/list-petugas";
import { ListRekapPage } from "../pages/list-rekap/list-rekap";
import { DetailInfoPage } from "../pages/detail-info/detail-info";
import { DetailRekapPage } from "../pages/detail-rekap/detail-rekap";
import {NotificationsPage} from "../pages/notifications/notifications";
import {RegisterPage} from "../pages/register/register";
import {SearchLocationPage} from "../pages/search-location/search-location";
import {TripDetailPage} from "../pages/trip-detail/trip-detail";
import {TripsPage} from "../pages/trips/trips";
import {LocalWeatherPage} from "../pages/local-weather/local-weather";
import { ProfilPage } from "../pages/profil/profil";
import {userPage} from "../pages/user/user";
import {nilaiPage} from "../pages/nilai/nilai";
import {MenuPage} from "../pages/menu/menu";
import {EvaluationPage} from "../pages/evaluation/evaluation";
import {ChooseDatePage} from "../pages/choose-date/choose-date";
import { RestApiProvider } from '../providers/rest-api/rest-api';

// import services
// end import services
// end import services

// import pages
// end import pages

@NgModule({
  declarations: [
    MyApp,
    SettingsPage,
    CheckoutTripPage,
    HomePage,
    DashboardPage,
    LoginPage,
    FormKinerjaPage,
    FormUserPage,
    FormRekapPage,
    FormRankPage,
    ListPetugasPage,
    ListRekapPage,
    ProfilPage,
    DetailInfoPage,
    DetailRekapPage,
    LocalWeatherPage,
    userPage,
    nilaiPage,
    MenuPage,
    ChooseDatePage,
    NotificationsPage,
    RegisterPage,
    EvaluationPage,
    SearchLocationPage,
    TripDetailPage,
    TripsPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    IonicStorageModule.forRoot({
      name: '__ionic3_start_theme',
        driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SettingsPage,
    CheckoutTripPage,
    HomePage,
    DashboardPage,
    LoginPage,
    FormKinerjaPage,
    FormUserPage,
    FormRekapPage,
    FormRankPage,
    ListPetugasPage,
    ListRekapPage,
    DetailInfoPage,
    DetailRekapPage,
    ProfilPage,
    LocalWeatherPage,
    userPage,
    nilaiPage,
    MenuPage,
    ChooseDatePage,
    NotificationsPage,
    RegisterPage,
    EvaluationPage,
    SearchLocationPage,
    TripDetailPage,
    TripsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    ActivityService,
    DataServices,
    TripService,
    GlobalController,
    WeatherProvider,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    RestApiProvider
  ]
})

export class AppModule {
}
