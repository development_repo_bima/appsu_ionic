import { Component } from "@angular/core";
import { NavController, NavParams, PopoverController } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { DataServices } from '../../services/data-services';

import { NotificationsPage } from "../notifications/notifications";
import { SettingsPage } from "../settings/settings";
import { FormKinerjaPage } from "../form-kinerja/form-kinerja";
import { DetailInfoPage } from "../detail-info/detail-info";
import { ChooseDatePage } from "../choose-date/choose-date";
import { GlobalController } from "../../app/globalController";
import { TripsPage } from "../trips/trips";
import { SearchLocationPage } from "../search-location/search-location";


@Component({
    selector: 'page-list-petugas',
    templateUrl: 'list-petugas.html'
})

export class ListPetugasPage {

    data: any;
    users: string[];
    errorMessage: string;
    page = 1;
    perPage = 0;
    totalData = 0;
    totalPage = 0;

    private q;
    private month;
    private officerList = [];
    private arrUser;
    private title;
    private nama;
    private param;
    constructor(private storage: Storage, public navParams: NavParams, public nav: NavController, public http: Http, public popoverCtrl: PopoverController, public global: GlobalController) {
        this.getOfficers();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        this.param = JSON.parse(atob(this.navParams.data));

        var date = this.param.date_nilai.split('-');
        this.month = this.global.getMonth(date[1]);
    }

    getOfficers(q = '') {
        var json;
        var params = this.navParams.data.replace(new RegExp('=', 'g'), "");
        var url = 'http://localhost/api-v1/request/get_officers_param/' + params +'/'+ q;
        //var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers_param/' + params +'/'+ q;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers_param_page?param=' + params +'&query='+ q+'&id_staff='+localStorage.getItem('user_id');
        this.http.get(remoteurl)
            .map(res => {
                //this.global.loader();
                json = res.json();
                
                this.officerList = json.data.data;

                this.totalData = json.data.total_rows;
                this.totalPage = json.data.total_page;

            })
            .subscribe(
            data => { },
            error => {
                var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
                this.global.toaster(text);
            });
    }

    doInfinite(infiniteScroll){
        var json;
        this.page = this.page+1;
        var params = this.navParams.data.replace(new RegExp('=', 'g'), "");
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers_param_page?param=' + params +'&page='+ this.page+'&id_staff='+localStorage.getItem('user_id');
        
        this.http.get(remoteurl)
            .map(res => {
                //this.global.loader();
                json = res.json();
                //console.log(json.data.total_page);
                for(let i=0; i<json.data.data.length; i++) {
                    this.officerList.push(json.data.data[i]);
                  }
                  console.log(this.page);
                  console.log(this.totalPage);
                this.totalData = json.data.total_rows;
                this.totalPage = json.data.total_page;
                setTimeout(()=>{
                    console.log('Async operation has ended');
                    infiniteScroll.complete();
                }, 2000)
            })
            .subscribe(
            data => { },
            error => {
                //this.global.toaster(error);
                console.log(error);
            });
            
        
    }

    searchTerm() {
        console.log(this.q);
        if (this.q) {
            this.getOfficers(this.q);
        } else {
            this.getOfficers();
        }
    }

    // to go account page
    goToView(id) {
        this.param.id = id;
        var params = btoa(JSON.stringify(this.param)).replace(new RegExp('=', 'g'), "");
        this.nav.push(DetailInfoPage, params);
    }

}

//
