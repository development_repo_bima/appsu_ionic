import { Component } from "@angular/core";
import { NavController, NavParams, PopoverController } from "ionic-angular";
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { DataServices } from '../../services/data-services';
import { GlobalController } from "../../app/globalController";
import { userPage } from "../../pages/user/user";


@Component({
    selector: 'page-form-user',
    templateUrl: 'form-user.html'
})

export class FormUserPage {
    private title;
    private nama;
    private formInstance = [];
    private form:FormGroup;
    private dataStaff = [];
    private id = "";
    private role = "";
    constructor( public nav: NavController, public navParams:NavParams, public formBuilder: FormBuilder, public http: Http, public popoverCtrl: PopoverController, public data: DataServices, public global: GlobalController) {
        this.getPerson();
        this.getStaff();
        this.formObj();
        this.title = ((typeof this.navParams.data === 'string') ? 'Ubah User' : 'Tambah User');
        this.nama = localStorage.getItem('name');

        this.form = this.formBuilder.group({
            username: ['', Validators.required],
            nama: ['', Validators.required],
            nik: ['', Validators.required],
            telp: ['', Validators.required],
            pass: ['', Validators.required],
            confirm_pass: ['', Validators.required],
            birth: ['', Validators.required],
            address: ['', Validators.required],
            role: ['', Validators.required],
            disposisi: ['']
        })
        
    }

    formObj(){
        this.formInstance = [
            { label: 'Username', icon: 'at', type: 'email', formCName: 'username' },
            { label: 'Nama', icon: 'person', type: 'text', formCName: 'nama' },
            { label: 'NIK', icon: 'card', type: 'text', formCName: 'nik' },
            { label: 'No Handphone', icon: 'call', type: 'text', formCName: 'telp' },
            { label: 'Password', icon: 'key', type: 'password', formCName: 'pass' },
            { label: 'Confirm Password', icon: 'key', type: 'password', formCName: 'confirm_pass' }
        ];
        
        if(typeof this.navParams.data === 'string'){
            this.formInstance.splice(4,2);
            console.log(this.formInstance);
        }
    }

    getPerson() {
        var json;
        console.log(this.navParams.data);
        if (typeof this.navParams.data === 'string'){
            var url = 'http://localhost/api-v1/request/get_person/' + this.navParams.data;
            var remoteurl = 'http://bms.uphero.com/api-v1/request/get_person/' + this.navParams.data;
            this.http.get(remoteurl)
                .map(res => {
                    //this.global.loader();
                    json = res.json();
                    this.id = json.data.user_id;
                    this.form.controls['username'].setValue(json.data.username);
                    this.form.controls['nama'].setValue(json.data.nama);
                    this.form.controls['nik'].setValue(json.data.nik);
                    this.form.controls['telp'].setValue(json.data.telp);
                    this.form.controls['birth'].setValue(json.data.tgl_lahir);
                    this.form.controls['address'].setValue(json.data.alamat);
                    this.form.controls['role'].setValue(json.data.id_jabatan);
                    this.form.controls['disposisi'].setValue(json.data.id_staff);
                })
                .subscribe(
                data => { },
                error => {
                    var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
                    this.global.toaster(text);
                });
        }
    }

    checkRole(){
        this.role = this.form.value['role'];
    }

    getStaff(){
        var json;
        var url = 'http://localhost/api-v1/request/get_staff';
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_staff';
        this.http.get(remoteurl)
        .map(res => {
            json = res.json();
            this.dataStaff = json.data;
            console.log(this.dataStaff);
        })
        .subscribe(
            data => { },
            error => {
                console.log(error);
            }
        )
    }

    /* ionViewWillEnter() {
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then((val) => {
            if (val === null) {
                this.search.name = "Rio de Janeiro, Brazil"
            } else {
                this.search.name = val;
            }
        }).catch((err) => {
            console.log(err)
        });
    } */

    post_user() {

        var url = 'http://localhost/api-v1/request/post_user' + ((typeof this.navParams.data === 'string') ? '/' + this.navParams.data : '');
        var remoteurl = 'http://bms.uphero.com/api-v1/request/post_user' + ((typeof this.navParams.data === 'string') ? '/' + this.navParams.data : '');
        this.http.post(remoteurl, JSON.stringify(this.form.value))
            .timeout(3000)
            .map(data => {
                this.global.loader();
                var res = JSON.parse(data['_body']);
                if(res.status){
                    setTimeout(() => {
                        this.global.toaster(res.message);
                    }, 2000);
                    setTimeout(() => {
                        this.nav.setRoot(userPage);
                    }, 4000);
                }else{
                    setTimeout(() => {
                        this.global.toaster(res.message);
                    }, 2000);
                }
            })
            .subscribe(
            data => {

            },
            error => {
                var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
                this.global.toaster(text);
            });
    }
}