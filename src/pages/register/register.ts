import {Component} from "@angular/core";
import { NavController, NavParams, PopoverController } from "ionic-angular";
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { DataServices } from '../../services/data-services';
import { GlobalController } from "../../app/globalController";
import {LoginPage} from "../login/login";
import {HomePage} from "../home/home";


@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  private title;
  private nama;
  private formInstance = [];
  private form:FormGroup;
  private dataStaff = [];
  private id = "";
  private role = "";
  constructor(public nav: NavController, public navParams:NavParams, public formBuilder: FormBuilder, public http: Http, public popoverCtrl: PopoverController, public data: DataServices, public global: GlobalController) {
    this.formObj();
    this.title = ((typeof this.navParams.data === 'string') ? 'Ubah User' : 'Tambah User');
    this.nama = localStorage.getItem('name');

    this.form = this.formBuilder.group({
        username: ['', Validators.required],
        nama: ['', Validators.required],
        nik: ['', Validators.required],
        telp: ['', Validators.required],
        pass: ['', Validators.required],
        confirm_pass: ['', Validators.required],
        birth: ['', Validators.required],
        address: ['', Validators.required]
    })
  }

  formObj(){
      this.formInstance = [
          { label: 'Username', icon: 'at', type: 'email', formCName: 'username' },
          { label: 'Nama', icon: 'person', type: 'text', formCName: 'nama' },
          { label: 'NIK', icon: 'card', type: 'text', formCName: 'nik' },
          { label: 'No Handphone', icon: 'call', type: 'text', formCName: 'telp' },
          { label: 'Password', icon: 'key', type: 'password', formCName: 'pass' },
          { label: 'Confirm Password', icon: 'key', type: 'password', formCName: 'confirm_pass' }
      ];
      
  }

  post_user() {

      var url = 'http://localhost/api-v1/request/post_register';
      var remoteurl = 'http://bms.uphero.com/api-v1/request/post_register';
      this.http.post(remoteurl, JSON.stringify(this.form.value))
          .timeout(3000)
          .map(data => {
              this.global.loader();
              var res = JSON.parse(data['_body']);
              if(res.status){
                  setTimeout(() => {
                      this.global.toaster(res.message);
                  }, 2000);
                  setTimeout(() => {
                      this.nav.setRoot(LoginPage);
                  }, 4000);
              }else{
                  setTimeout(() => {
                      this.global.toaster(res.message);
                  }, 2000);
              }
          })
          .subscribe(
          data => {

          },
          error => {
              var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
              this.global.toaster(text);
          });
  }

  // register and go to home page
  register() {
    this.nav.setRoot(HomePage);
  }

  // go to login page
  login() {
    this.nav.setRoot(LoginPage);
  }
}
