import { Component } from "@angular/core";
import { NavController, NavParams, PopoverController } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { DataServices } from '../../services/data-services';

import { NotificationsPage } from "../notifications/notifications";
import { SettingsPage } from "../settings/settings";
import { FormKinerjaPage } from "../form-kinerja/form-kinerja";
import { ListPetugasPage } from "../list-petugas/list-petugas";
import { GlobalController } from "../../app/globalController";
import { TripsPage } from "../trips/trips";
import { SearchLocationPage } from "../search-location/search-location";

@Component({
    selector: 'page-evaluation',
    templateUrl: 'evaluation.html'
})

export class EvaluationPage {
    // search condition
    public search = {
        name: "Rio de Janeiro, Brazil",
        date: new Date().toISOString()
    }
    public disiplin;
    private arrUser;
    private kriteriaNilai;
    private title;
    private nama;
    private result;
    private post = {
        'date_nilai' : '',
        'skala'      : '',
        'tipe'       : 'e'
    };
    constructor(private storage: Storage, public navParams: NavParams, public nav: NavController, public http: Http, public popoverCtrl: PopoverController, public data: DataServices, public global: GlobalController) {
        this.arrUser = this.data.dataUser();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        var btoaaa = btoa(this.navParams.data);
        
    }

    ionViewWillEnter() {
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then((val) => {
            if (val === null) {
                this.search.name = "Rio de Janeiro, Brazil"
            } else {
                this.search.name = val;
            }
        }).catch((err) => {
            console.log(err)
        });
    }

    getListEvaluation() {
        if (this.post.date_nilai && this.post.skala) {
            var init = btoa(JSON.stringify(this.post)).replace(new RegExp('=', 'g'), "");
            this.nav.push(ListPetugasPage, init);
            
        } else {
            this.global.toaster("Please Choose date first");
        }
    }

}

//
