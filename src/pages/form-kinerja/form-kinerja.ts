import { Component } from "@angular/core";
import { NavController, NavParams, PopoverController } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { DataServices } from '../../services/data-services';

import { NotificationsPage } from "../notifications/notifications";
import { SettingsPage } from "../settings/settings";
import { GlobalController } from "../../app/globalController";
import { nilaiPage } from "../../pages/nilai/nilai";
import { TripsPage } from "../trips/trips";
import { SearchLocationPage } from "../search-location/search-location";


@Component({
    selector: 'page-form-kinerja',
    templateUrl: 'form-kinerja.html'
})

export class FormKinerjaPage {
    // search condition
    public search = {
        name: "Rio de Janeiro, Brazil",
        date: new Date().toISOString()
    }
    public disiplin;
    private kriteriaNilai;
    private title;
    private nama;
    private result;
    private idPost;
    private push = {};
    
    constructor(private storage: Storage, public navParams:NavParams, public nav: NavController, public http: Http, public popoverCtrl: PopoverController, public data: DataServices, public global: GlobalController) {
        this.getKriteria();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        this.idPost = this.navParams.data;
        
    }


    ionViewWillEnter() {
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then((val) => {
            if (val === null) {
                this.search.name = "Rio de Janeiro, Brazil"
            } else {
                this.search.name = val;
            }
        }).catch((err) => {
            console.log(err)
        });
    }

    getKriteria(){
        var json;
        var params = this.navParams.data.replace('==','').replace('=','');
        var url = 'http://localhost/api-v1/request/get_kriteria/'+params;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_kriteria/'+params;
        this.http.get(remoteurl)
            .map(res => {
                //this.global.loader();
                json = res.json();
                this.kriteriaNilai = json.data.data;
                this.result = json.data.result;
                if(json.data.dataEdit){
                    /* push to array ngModel */
                    this.push['K3'] = json.data.dataEdit.K3;
                    this.push['DIS'] = json.data.dataEdit.DIS;
                    this.push['PRK'] = json.data.dataEdit.PRK;
                    this.push['TMW'] = json.data.dataEdit.TMW;
                    this.push['ATT'] = json.data.dataEdit.ATT;
                    
                    var decode = atob(this.navParams.data);
                    this.idPost = btoa(decode + '|' + json.data.dataEdit.nilai_id).replace('=','');
                    
                }
                
            })
            .subscribe(
            data => { },
            error => {
                var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
                this.global.toaster(text);
            });
    }

    valueMean(event, criteria) {
        console.log(this.push);
        criteria.default = criteria.detail[event['_value']-1].keterangan;
        //criteria.result = criteria.detail[event['_value']-1].angka;
        this.result[criteria.id_kriteria] = criteria.detail[event['_value']-1].angka;
        
    }

    post_nilai() {
        if (typeof this.navParams.data === 'string'){
            var params = this.idPost.replace('==','').replace('=','');
            var url = 'http://localhost/api-v1/request/post_nilai/' + params+'/'+localStorage.getItem('user_id');
            var remoteurl = 'http://bms.uphero.com/api-v1/request/post_nilai/' + params+'/'+localStorage.getItem('user_id');
            this.http.post(remoteurl, JSON.stringify(this.push))
            .timeout(3000)
            .map(data => {
                this.global.loader();
                var res = JSON.parse(data['_body']);
                if (res.status) {
                    setTimeout(() => {
                        this.global.toaster(res.message);
                    }, 2000);
                    setTimeout(() => {
                        this.nav.setRoot(nilaiPage);
                    }, 4000);
                } else {
                    setTimeout(() => {
                        this.global.toaster(res.message);
                    }, 2000);
                }
            })
            .subscribe(
            data => {

            },
            error => {
                
                var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
                this.global.toaster(text);
            });
        }else{
            this.global.toaster("error with id is null");
        }
    }

    // go to result page
    doSearch() {
        this.nav.push(TripsPage);
    }

    // choose place
    choosePlace(from) {
        this.nav.push(SearchLocationPage, from);
    }

    // to go account page
    goToAccount(id) {
        this.nav.push(SettingsPage, id);
    }

    presentNotifications(myEvent) {
        console.log(myEvent);
        let popover = this.popoverCtrl.create(NotificationsPage);
        popover.present({
            ev: myEvent
        });
    }

}

//
