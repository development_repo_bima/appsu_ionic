import {Component} from "@angular/core";
import {NavController, NavParams, AlertController } from "ionic-angular";
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { GlobalController } from "../../app/globalController";
import { FormUserPage } from "../form-user/form-user";
import { userPage } from "../user/user";


@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  
  /* perlu di set tampungannya dlu */
  private user = {};
  constructor(public nav: NavController, public alertCtrl: AlertController, public navParams:NavParams, public http:Http, public global:GlobalController) {  
    this.getPerson();
  }

  getPerson() {
    var json;
    var url = 'http://localhost/api-v1/request/get_person/'+this.navParams.data;
    var remoteurl = 'http://bms.uphero.com/api-v1/request/get_person/'+this.navParams.data;
    this.http.get(remoteurl)
      .map(res => {
        //this.global.loader();
        json = res.json();
        this.user = json.data;
      })
      .subscribe(
      data => { },
      error => {
        var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
        this.global.toaster(text);
      });
  }
  
  editUser(id) {
    console.log('test');
    this.nav.push(FormUserPage, id);
  }

  hapusUser(){
    //this.global.toaster('hapus');
    var url = 'http://localhost/api-v1/request/delete_user/' + this.navParams.data;
    var remoteurl = 'http://bms.uphero.com/api-v1/request/delete_user/' + this.navParams.data;
    this.http.post(remoteurl, null)
        .timeout(3000)
        .map(data => {
            this.global.loader();
            var res = JSON.parse(data['_body']);
            if(res.status){
                setTimeout(() => {
                    this.global.toaster(res.message);
                }, 2000);
                setTimeout(() => {
                    this.nav.setRoot(userPage);
                }, 4000);
            }else{
                setTimeout(() => {
                    this.global.toaster(res.message);
                }, 2000);
            }
        })
        .subscribe(
        data => {

        },
        error => {
            var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
            this.global.toaster(text);
        });
  }

  approveUser(id){
    var remoteurl = 'http://bms.uphero.com/api-v1/request/approve_user/' + id;
    this.http.post(remoteurl, null)
        .timeout(3000)
        .map(data => {
            this.global.loader();
            var res = JSON.parse(data['_body']);
            if(res.status){
                setTimeout(() => {
                    this.global.toaster(res.message);
                }, 2000);
                setTimeout(() => {
                    this.nav.setRoot(userPage);
                }, 4000);
            }else{
                setTimeout(() => {
                    this.global.toaster(res.message);
                }, 2000);
            }
        })
        .subscribe(
        data => {

        },
        error => {
            var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
            this.global.toaster(text);
        });
  }

  doConfirm(id, act){
    if(act == 'hapusUser'){
      var text = 'Hapus data user';
      var msg = 'Apakah anda akan menghapus user ini?';
    }else if(act == 'approveUser'){
      var text = 'Approve user';
      var msg = 'Approve user ini?';
    }
    let alert = this.alertCtrl.create({
      title: text,
      message: msg,
      buttons: [
        {
          text: 'Tidak',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ya',
          handler: () => {
            console.log('Agree clicked');
            if(act == 'hapusUser'){
              this.hapusUser();
            }else if(act == 'approveUser'){
              this.approveUser(id);
            }
          }
        }
      ]
    });

    alert.present();
  }

  
  /* reserved */
  setStatusUser(id, stat){

  }

  deleteUser(id){

  }
}
