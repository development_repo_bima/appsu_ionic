import {Component} from "@angular/core";
import {NavController, PopoverController} from "ionic-angular";
import {Storage} from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import {NotificationsPage} from "../notifications/notifications";
import {SettingsPage} from "../settings/settings";
import { DataServices } from '../../services/data-services';
import { GlobalController } from "../../app/globalController";

import {TripsPage} from "../trips/trips";
import {SearchLocationPage} from "../search-location/search-location";
import {nilaiPage} from "../../pages/nilai/nilai";
import {DashboardPage} from "../../pages/dashboard/dashboard";
import { ListRekapPage } from "../../pages/list-rekap/list-rekap";
import { DetailInfoPage } from "../../pages/detail-info/detail-info";
import {LoginPage} from "../../pages/login/login";
import {ProfilPage} from "../../pages/profil/profil";
import {userPage} from "../../pages/user/user";
import {MenuPage} from "../../pages/menu/menu";
import {EvaluationPage} from "../../pages/evaluation/evaluation";
import { FormKinerjaPage } from "../../pages/form-kinerja/form-kinerja";
import { FormRankPage } from "../../pages/form-rank/form-rank";
import { FormRekapPage } from "../../pages/form-rekap/form-rekap";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  // search condition
  private appMenuItems;
  private rootPage;
  private DashboardPage;
  private FormKinerjaPage;
  private FormRekapPage;
  private FormRankPage;
  private nilaiPage;
  private userPage;
  public search = {
    name: "Rio de Janeiro, Brazil",
    date: new Date().toISOString()
  }
  user: any = localStorage.getItem('email');
  constructor(private storage: Storage, public global:GlobalController, public nav: NavController, public popoverCtrl: PopoverController, public http:Http, public data: DataServices) {
    //this.rootPage = DashboardPage;
    this.rootPage = DashboardPage;
    this.nilaiPage = nilaiPage;
    this.userPage = userPage;
    this.menuGenerated();
    
  }

  ionViewWillEnter() {
    // this.search.pickup = "Rio de Janeiro, Brazil";
    // this.search.dropOff = "Same as pickup";
    this.storage.get('pickup').then((val) => {
      if (val === null) {
        this.search.name = "Rio de Janeiro, Brazil"
      } else {
        this.search.name = val;
      }
    }).catch((err) => {
      console.log(err)
    });
  }

  // go to result page
  doSearch() {
    this.nav.push(TripsPage);
  }

  // choose place
  choosePlace(from) {
    this.nav.push(SearchLocationPage, from);
  }

  // to go account page
  goToAccount() {
    this.nav.push(SettingsPage);
  }

  presentNotifications(myEvent) {
    console.log(myEvent);
    let popover = this.popoverCtrl.create(NotificationsPage);
    popover.present({
      ev: myEvent
    });
  }

  setRootPage() {
    if (!localStorage.getItem('email')) {
      this.nav.setRoot(LoginPage);
      //return LoginPage;
    }
  }

  /* routes */
  openPage(page) {
    
    var component;
    if(page === "HomePage"){
      page = {menu:'Home', component:'HomePage'};
    }
    localStorage.setItem('component', page.menu);
    component = this.determineFn(page.component);
    if(component){
      this.rootPage = component;
    }else{
      this.global.toaster('cannot decided component!');
    }
  }

  /* logout */
  logout() {
    localStorage.clear();
    this.nav.setRoot(LoginPage);
  }

  menuGenerated() {
    var json;
    var role = localStorage.getItem('role');
    var url = 'http://localhost/api-v1/request/get_menus/'+role;
    var remoteurl = 'http://bms.uphero.com/api-v1/request/get_menus/'+role;
    this.http.get(remoteurl)
      .map(res => {
        //this.global.loader();
        json = res.json();
        this.appMenuItems = json.data;
      })
      .subscribe(
      data => {},
      error => {
        var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
        this.global.toaster(text);
      });
  }

  determineFn(str){
    var component;
    
    if(str == 'HomePage'){
      component = HomePage;
    }else if(str == 'userPage'){
      component = userPage;
    }else if(str == 'nilaiPage'){
      component = nilaiPage;
    } else if (str == 'ProfilPage') {
      component = ProfilPage;
    } else if (str == 'FormRankPage'){
      component = FormRankPage;
    }else if(str == 'ListRekapPage'){
      component = ListRekapPage;
    } else if (str == 'FormRekapPage'){
      component = FormRekapPage;
    }else if(str == 'DetailInfoPage'){
      component = DetailInfoPage;
    }else if(str == 'EvaluationPage'){
      component = EvaluationPage;
    }else{
      component = false;
    }
    return component;
  }

}

//
