import { Component } from "@angular/core";
import { NavController, PopoverController } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { DataServices } from '../../services/data-services';
import { RestApiProvider } from '../../providers/rest-api/rest-api';

import { NotificationsPage } from "../notifications/notifications";
import { SettingsPage } from "../settings/settings";
import { FormKinerjaPage } from "../form-kinerja/form-kinerja";
import { GlobalController } from "../../app/globalController";
import { TripsPage } from "../trips/trips";
import { SearchLocationPage } from "../search-location/search-location";


@Component({
    selector: 'page-menu',
    templateUrl: 'menu.html'
})

export class MenuPage {
    // search condition
    public search = {
        name: "Rio de Janeiro, Brazil",
        date: new Date().toISOString()
    }
    public disiplin;
    private officerList = [];
    private arrUser;
    private title;
    private nama;
    private showSearch = false;

    data:any;
    users: string[];
    errorMessage: string;
    page = 1;
    perpage = 0;
    totalData = 0;
    totalPage = 0;
    constructor(private storage: Storage, public restApi:RestApiProvider, public nav: NavController, public http: Http, public popoverCtrl: PopoverController, public global: GlobalController) {
        this.getOfficers();
        this.getUsers();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
    }

    showHideSearch(){
        this.showSearch = (this.showSearch) ? false:true; 
    }

    getUsers(){
        this.restApi.getUsers(this.page)
            .subscribe(
                res => {
                    console.log(res);
                    this.data = res;
                    this.users = this.data.data;
                    this.perpage = this.data.per_page;
                    this.totalData = this.data.total;
                    this.totalPage = this.data.total_pages;
                },
                error => this.errorMessage = <any>error);
    }

    doInfinite(infiniteScroll){
        this.page = this.page+1;
        setTimeout(() => {
            this.restApi.getUsers(this.page)
                .subscribe(
                res => {
                    this.data = res;
                    this.perpage = this.data.per_page;
                    this.totalData = this.data.total;
                    this.totalPage = this.data.total_pages;
                    for(let i=0; i<this.data.data.length; i++){
                        this.users.push(this.data.data[i]);
                    }
                },
                error => this.errorMessage = <any>error);
        console.log('Async operation has ended');
        infiniteScroll.complete();
        }, 1000);
    }

    ionViewWillEnter() {
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then((val) => {
            if (val === null) {
                this.search.name = "Rio de Janeiro, Brazil"
            } else {
                this.search.name = val;
            }
        }).catch((err) => {
            console.log(err)
        });
    }

    getOfficers() {
        var json;
        this.http.get('http://localhost/api-v1/request/get_officers')
            .map(res => {
                //this.global.loader();
                json = res.json();
                this.officerList = json.data;
            })
            .subscribe(
            data => { },
            error => {
                //this.global.toaster(error);
                console.log(error);
            });
    }

    

    // go to result page
    doSearch() {
        this.nav.push(TripsPage);
    }

    // choose place
    choosePlace(from) {
        this.nav.push(SearchLocationPage, from);
    }

    // to go account page
    goToAccount(id) {
        this.nav.push(FormKinerjaPage, id);
    }

    presentNotifications(myEvent) {
        console.log(myEvent);
        let popover = this.popoverCtrl.create(NotificationsPage);
        popover.present({
            ev: myEvent
        });
    }

}

//
