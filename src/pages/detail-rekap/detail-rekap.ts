import { Component } from "@angular/core";
import { NavController, NavParams, PopoverController } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { DataServices } from '../../services/data-services';
import { userPage } from "../../pages/user/user";
import { nilaiPage } from "../../pages/nilai/nilai";
/* import { NotificationsPage } from "../notifications/notifications";
import { SettingsPage } from "../settings/settings"; */
import { GlobalController } from "../../app/globalController";
/* import { TripsPage } from "../trips/trips";
import { SearchLocationPage } from "../search-location/search-location"; */


@Component({
    selector: 'page-detail-rekap',
    templateUrl: 'detail-rekap.html'
})

export class DetailRekapPage {
    // search condition
    private shownGroup = null;
    private month_from;
    private month_to;
    private range = [];
    private detail = [];
    private nilai;
    private officer;
    private title;
    private nama;
    constructor(private storage: Storage, public navParams: NavParams, public nav: NavController, public http: Http, public popoverCtrl: PopoverController, public data: DataServices, public global: GlobalController) {
        this.getData();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        
        var params = atob(this.navParams.data).split("|");
        var date1 = params[0].split("-");
        var date2 = params[1].split("-");
        this.month_from = this.global.getMonth(date1[1]);
        this.month_to = this.global.getMonth(date2[1]);
        this.range = this.global.rangeCreator(parseInt(date1[1]), parseInt(date2[1]));
    }

    getData() {
        var json;
        var localURL = 'http://localhost/api-v1/request/get_history_rekap/' + this.navParams.data;
        var serviceURL = 'http://bms.uphero.com/api-v1/request/get_history_rekap/' + this.navParams.data; 
        this.http.get(serviceURL)
            .map(res => {
                //this.global.loader();
                json = res.json();

                this.detail = json.data;
                
            })
            .subscribe(
            data => { },
            error => {
                var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
                this.global.toaster(text);
            });
    }

    goToUser() {
        localStorage.setItem('component', 'User');
        this.nav.setRoot(userPage);
    }

    goToNilai() {
        localStorage.setItem('component', 'Penilaian');
        this.nav.setRoot(nilaiPage);
    }

    toggleGroup(group){
        if(this.isGroupShown(group)){
            this.shownGroup = null;
        }else{
            this.shownGroup = group;
        }
    }

    isGroupShown(group){
        return this.shownGroup === group;
    }

}

//
