import { Component } from "@angular/core";
import { NavController, NavParams, PopoverController } from "ionic-angular";
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { DataServices } from '../../services/data-services';
import { GlobalController } from "../../app/globalController";
import { userPage } from "../../pages/user/user";


@Component({
    selector: 'page-profil',
    templateUrl: 'profil.html'
})

export class ProfilPage {
    private title;
    private nama;
    private edit;
    private formInstance = [];
    private form: FormGroup;
    constructor(public nav: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public http: Http, public popoverCtrl: PopoverController, public data: DataServices, public global: GlobalController) {
        this.getPerson();
        this.formObj();
        this.title = 'Profil';
        this.edit = false;
        this.nama = localStorage.getItem('name');

        this.form = this.formBuilder.group({
            username: ['', Validators.required],
            nama: ['', Validators.required],
            telp: ['', Validators.required]
        })

    }

    formObj() {
        this.formInstance = [
            { label: 'Username', icon: 'at', type: 'email', formCName: 'username' },
            { label: 'Nama', icon: 'person', type: 'text', formCName: 'nama' },
            { label: 'No Handphone', icon: 'call', type: 'text', formCName: 'telp' }
        ];

    }

    getPerson() {
        var json;
        var token = localStorage.getItem('token').split('.');
        var info = JSON.parse(atob(token[1]));
        
        if (typeof info === 'object') {
            var url = 'http://localhost/api-v1/request/get_person/' + info.user_id;
            var remoteurl = 'http://bms.uphero.com/api-v1/request/get_person/' + info.user_id;
            this.http.get(remoteurl)
                .map(res => {
                    //this.global.loader();
                    json = res.json();
                    this.form.controls['username'].setValue(json.data.username);
                    this.form.controls['nama'].setValue(json.data.nama);
                    this.form.controls['telp'].setValue(json.data.telp);
                })
                .subscribe(
                data => { },
                error => {
                    //this.global.toaster(error);
                    console.log(error);
                });
        }
    }

    canEdit(bool){
        this.edit = bool;
    }

    /* ionViewWillEnter() {
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then((val) => {
            if (val === null) {
                this.search.name = "Rio de Janeiro, Brazil"
            } else {
                this.search.name = val;
            }
        }).catch((err) => {
            console.log(err)
        });
    } */

    post_user() {
        var token = localStorage.getItem('token').split('.');
        var info = JSON.parse(atob(token[1]));
        var url = 'http://localhost/api-v1/request/post_profil/' + info.user_id;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/post_profil/' + info.user_id;
        this.http.post(remoteurl, JSON.stringify(this.form.value))
            .timeout(3000)
            .map(data => {
                this.global.loader();
                var res = JSON.parse(data['_body']);
                if (res.status) {
                    setTimeout(() => {
                        this.global.toaster(res.message);
                    }, 2000);
                    setTimeout(() => {
                        this.edit = false;
                    }, 4000);
                }
            })
            .subscribe(
            data => {

            },
            error => {
                var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
                this.global.toaster(text);
            });
    }
}