import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { WeatherProvider } from '../../services/weather';
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { SettingsPage } from "../settings/settings";
import { FormUserPage } from "../form-user/form-user";
import { GlobalController } from "../../app/globalController";
// import { HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'user',
    templateUrl: 'user.html'
})
export class userPage {
    weather: any;
    location: {
        state: string,
        city: string
    }

    data: any;
    users: string[];
    errorMessage: string;
    page = 1;
    perPage = 0;
    totalData = 0;
    totalPage = 0;

    private title;
    private q;
    private userList;
    constructor(
        public nav: NavController,
        private weatherProvider: WeatherProvider,
        private storage: Storage,
        private http:Http,
        private global:GlobalController) {
            this.title = global.getComponentInitial();
            this.getUsers();
            
    }

    getUsers(q = '') {
        var json;
        var url = 'http://localhost/api-v1/request/get_users/'+q;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_users_page?query='+q;
        this.http.get(remoteurl)
            .map(res => {
                //this.global.loader();
                json = res.json();
                //console.log(json.data.total_page);
                this.userList = json.data.data;
                console.log(this.userList);
                this.totalData = json.data.total_rows;
                this.totalPage = json.data.total_page;
            })
            .subscribe(
            data => { },
            error => {
                //this.global.toaster(error);
                console.log(error);
            });
    }

    searchTerm(){
        if(this.q){
            this.getUsers(this.q);
        }else{
            this.getUsers();
        }
    }

    goToForm(id) {
        console.log(this.q);
        this.nav.push(FormUserPage, id);
        
    }

    goToView(id) {
        this.nav.push(SettingsPage, id);
    }

    public getWeather(location) {
        if (typeof location === 'string') {
            this.location = JSON.parse(location);
            console.log(this.location);
        } else {
            this.location = location;
        }

        this.weatherProvider.getWeather(this.location.state, this.location.city).subscribe((weather: any) => {
            this.weather = weather.current_observation;
        });
    }

    doInfinite(infiniteScroll){
        var json;
        this.page = this.page+1;
        var url = 'http://localhost/api-v1/request/get_users/';
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_users_page?page='+this.page;
        
        this.http.get(remoteurl)
            .map(res => {
                //this.global.loader();
                json = res.json();
                //console.log(json.data.total_page);
                //this.userList = json.data.data;
                for(let i=0; i<json.data.data.length; i++) {
                    this.userList.push(json.data.data[i]);
                  }
                  console.log(this.page);
                  console.log(this.totalPage);
                this.totalData = json.data.total_rows;
                this.totalPage = json.data.total_page;
                setTimeout(()=>{
                    console.log('Async operation has ended');
                    infiniteScroll.complete();
                }, 2000)
            })
            .subscribe(
            data => { },
            error => {
                //this.global.toaster(error);
                console.log(error);
            });
            
        
    }
    /* doInfinite(infiniteScroll) {
        this.page = this.page+1;
        setTimeout(() => {
          this.getUsers(this.page)
             .subscribe(
               res => {
                 this.data = res;
                 this.perPage = this.data.per_page;
                 this.totalData = this.data.total;
                 this.totalPage = this.data.total_pages;
                 for(let i=0; i<this.data.data.length; i++) {
                   this.users.push(this.data.data[i]);
                 }
               },
               error =>  this.errorMessage = <any>error);
      
          console.log('Async operation has ended');
          infiniteScroll.complete();
        }, 1000);
      } */

}
