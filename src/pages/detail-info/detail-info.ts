import { Component } from "@angular/core";
import { NavController, NavParams, PopoverController } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { DataServices } from '../../services/data-services';
import { userPage } from "../../pages/user/user";
import { nilaiPage } from "../../pages/nilai/nilai";
/* import { NotificationsPage } from "../notifications/notifications";
import { SettingsPage } from "../settings/settings"; */
import { GlobalController } from "../../app/globalController";
/* import { TripsPage } from "../trips/trips";
import { SearchLocationPage } from "../search-location/search-location"; */


@Component({
    selector: 'page-dashboard',
    templateUrl: 'detail-info.html'
})

export class DetailInfoPage {
    // search condition
    shownGroup = null;
    private month;
    private detail = {};
    private nilai;
    private officer;
    private title;
    private nama;
    private param;
    constructor(private storage: Storage, public navParams:NavParams, public nav: NavController, public http: Http, public popoverCtrl: PopoverController, public data: DataServices, public global: GlobalController) {
        this.getData();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        this.param = JSON.parse(atob(this.navParams.data));
        console.log(this.param);
        if(localStorage.getItem('role') == '2'){
            var d = new Date();
            this.month = this.global.getMonth(d.getMonth());
        }else{
            var date = this.param.date_nilai.split('-');
            this.month = this.global.getMonth(date[1]);
        }
    }

    toggleGroup(group){
        if(this.isGroupShown(group)){
            this.shownGroup = null;
        }else{
            this.shownGroup = group;
        }
    }

    isGroupShown(group){
        return this.shownGroup === group;
    }

    getData() {
        var json;
        var params;
        if (localStorage.getItem('role') == '2'){
            var d = new Date();
            //params = btoa(d.getFullYear() + '-' + this.global.setMonth(d.getMonth())+'|'+localStorage.getItem('user_id')).replace("=","");
            var content = {
                'date_nilai' : d.getFullYear() + '-' + this.global.setMonth(d.getMonth()),
                'id'         : localStorage.getItem('user_id')
            }
            params = btoa(JSON.stringify(params));
        }else{
            params = this.navParams.data;
        }
        var url = 'http://localhost/api-v1/request/get_history/' + params;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_history/' + params;
        this.http.get(remoteurl)
            .map(res => {
                //this.global.loader();
                json = res.json();
                
                this.detail = json.data;

                /* if (localStorage.getItem('role') == '2') {
                    this.detail.nama = localStorage.getItem('component');
                } */
            })
            .subscribe(
            data => { },
            error => {
                var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
                this.global.toaster(text);
            });
    }

    goToUser() {
        localStorage.setItem('component', 'User');
        this.nav.setRoot(userPage);
    }

    goToNilai() {
        localStorage.setItem('component', 'Penilaian');
        this.nav.setRoot(nilaiPage);
    }

}

//
