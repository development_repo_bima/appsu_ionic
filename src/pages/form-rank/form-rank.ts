import { Component } from "@angular/core";
import { NavController, NavParams, PopoverController } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { DataServices } from '../../services/data-services';

import { NotificationsPage } from "../notifications/notifications";
import { SettingsPage } from "../settings/settings";
import { FormKinerjaPage } from "../form-kinerja/form-kinerja";
import { ListPetugasPage } from "../list-petugas/list-petugas";
import { GlobalController } from "../../app/globalController";
import { TripsPage } from "../trips/trips";
import { SearchLocationPage } from "../search-location/search-location";

@Component({
    selector: 'page-form-rank',
    templateUrl: 'form-rank.html'
})

export class FormRankPage {
    // search condition
    public search = {
        name: "Rio de Janeiro, Brazil",
        date: new Date().toISOString()
    }
    public disiplin;
    private arrUser;
    private kriteriaNilai;
    private title;
    private nama;
    private result;
    private date_nilai;
    private post = {
        'date_nilai'    : '',
        'tipe'          : 'r'
    };
    constructor(private storage: Storage, public navParams: NavParams, public nav: NavController, public http: Http, public popoverCtrl: PopoverController, public data: DataServices, public global: GlobalController) {
        this.arrUser = this.data.dataUser();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        var btoaaa = btoa(this.navParams.data);
        
    }

    ionViewWillEnter() {
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then((val) => {
            if (val === null) {
                this.search.name = "Rio de Janeiro, Brazil"
            } else {
                this.search.name = val;
            }
        }).catch((err) => {
            console.log(err)
        });
    }

    getKriteria() {
        var json;
        var url = 'http://localhost/api-v1/request/get_kriteria';
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_kriteria';
        this.http.get(remoteurl)
            .map(res => {
                //this.global.loader();
                json = res.json();
                this.kriteriaNilai = json.data.data;
                this.result = json.data.result;
            })
            .subscribe(
            data => { },
            error => {
                var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
                this.global.toaster(text);
            });
    }

    getFormKriteria() {
        if (this.post.date_nilai) {
            var init = btoa(JSON.stringify(this.post)).replace(new RegExp('=', 'g'), "");
            this.nav.push(ListPetugasPage, init);
        } else {
            this.global.toaster("Please Choose date first");
        }
    }

    post_nilai() {
        if (typeof this.navParams.data === 'string') {
            var url = 'http://localhost/api-v1/request/post_nilai/' + this.navParams.data;
            var remoteurl = 'http://bms.uphero.com/api-v1/request/post_nilai/' + this.navParams.data;
            this.http.post(remoteurl, JSON.stringify(this.result))
                .timeout(3000)
                .map(data => {
                    this.global.loader();
                    var res = JSON.parse(data['_body']);

                })
                .subscribe(
                data => {

                },
                error => {
                    var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
                    this.global.toaster(text);
                });
        } else {
            this.global.toaster("error with id is null");
        }
    }

}

//
