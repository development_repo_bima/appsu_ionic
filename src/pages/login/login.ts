import {Component} from "@angular/core";
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavController, AlertController, ToastController, MenuController } from "ionic-angular";
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import {GlobalController} from "../../app/globalController";
import {HomePage} from "../home/home";
import {RegisterPage} from "../register/register";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  private logins:FormGroup;
  /* private loginForm:any; */
  constructor(public nav: NavController, public http: Http, public forgotCtrl: AlertController, public menu: MenuController, public formBuilder:FormBuilder, public toastCtrl: ToastController, public global:GlobalController) {
    this.menu.swipeEnable(false);

    /*
     * validator 
     */
    this.logins = this.formBuilder.group({
      email: ['', Validators.required],
      pass: ['', Validators.required],
    });
    // rule but not mandatory
    /* this.loginForm = {
      email:'',
      pass:''
    } */

    /* this.loginForm = {}; */
    
  }
  loginForm = {};
  // go to register page
  register() {
    this.nav.setRoot(RegisterPage);
  }

  // login and go to home page
  login() {
    var url = 'http://localhost/api-v1/auth/verify';
    var remoteurl = 'http://bms.uphero.com/api-v1/auth/verify';
    var header = '';
    var data = this.logins.value;
    var res;
    
    this.http.post(remoteurl, JSON.stringify(data))
      .timeout(3000)
      .map(data=>{
        this.global.loader();
        res = JSON.parse(data['_body']);
        if(res.status){
          setTimeout(() => {
            this.global.toaster(res.message);
          }, 2000);
          localStorage.setItem('name', res.data.name);
          localStorage.setItem('user_id', res.data.user_id);
          localStorage.setItem('nip', res.data.nip);
          localStorage.setItem('role', res.data.role);
          localStorage.setItem('telp', res.data.telp);
          localStorage.setItem('token', res.data.token);
          setTimeout(() => {
            this.nav.setRoot(HomePage);
          }, 4000);
        }else{
          setTimeout(() => {
            this.global.toaster(res.message);
          }, 2000);
        }
      })
      .subscribe(
        data=>{
        
        }, 
        error=> {
          var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
          this.global.toaster(text);
        });
  }

  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
            let toast = this.toastCtrl.create({
              message: 'Email was sended successfully',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

}
