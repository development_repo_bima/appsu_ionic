import { Component } from "@angular/core";
import { NavController, PopoverController } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { DataServices } from '../../services/data-services';

import { NotificationsPage } from "../notifications/notifications";
import { SettingsPage } from "../settings/settings";
import { FormRekapPage } from "../../pages/form-rekap/form-rekap";
import { ChooseDatePage } from "../choose-date/choose-date";
import { GlobalController } from "../../app/globalController";
import { TripsPage } from "../trips/trips";
import { SearchLocationPage } from "../search-location/search-location";


@Component({
    selector: 'page-list-rekap',
    templateUrl: 'list-rekap.html'
})

export class ListRekapPage {
    
    data: any;
    users: string[];
    errorMessage: string;
    page = 1;
    perPage = 0;
    totalData = 0;
    totalPage = 0;

    private q;
    private officerList = [];
    private arrUser;
    private title;
    private nama;
    constructor(private storage: Storage, public nav: NavController, public http: Http, public popoverCtrl: PopoverController, public global: GlobalController) {
        this.getOfficers();
        
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
    }

    // getOfficers(q='') {
    //     var json;
    //     var localURL = 'http://localhost/api-v1/request/get_officers/' + q;
    //     var serviceURL = 'http://bms.uphero.com/api-v1/request/get_officers/'+q;
    //     this.http.get(serviceURL)
    //         .map(res => {
    //             //this.global.loader();
    //             json = res.json();
    //             this.officerList = json.data;
    //         })
    //         .subscribe(
    //         data => { },
    //         error => {
    //             var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
    //             this.global.toaster(text);
    //         });
    // }

    getOfficers(q='') {
        var json;
        var url = 'http://localhost/api-v1/request/get_officers/'+localStorage.getItem('user_id')+'/'+q;
        //var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers/'+localStorage.getItem('user_id')+'/'+q;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers_page?id_staff='+localStorage.getItem('user_id')+'&query='+q;
        this.http.get(remoteurl)
            .map(res => {
                //this.global.loader();
                json = res.json();
                this.officerList = json.data.data;

                this.totalData = json.data.total_rows;
                this.totalPage = json.data.total_page;
            })
            .subscribe(
            data => { },
            error => {
                var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
                this.global.toaster(text);
            });
    }

    doInfinite(infiniteScroll){
        var json;
        this.page = this.page+1;;
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_officers_page?id_staff='+localStorage.getItem('user_id')+'&page='+this.page;
        
        this.http.get(remoteurl)
            .map(res => {
                //this.global.loader();
                json = res.json();
                //console.log(json.data.total_page);
                for(let i=0; i<json.data.data.length; i++) {
                    this.officerList.push(json.data.data[i]);
                  }
                  console.log(this.page);
                  console.log(this.totalPage);
                this.totalData = json.data.total_rows;
                this.totalPage = json.data.total_page;
                setTimeout(()=>{
                    console.log('Async operation has ended');
                    infiniteScroll.complete();
                }, 2000)
            })
            .subscribe(
            data => { },
            error => {
                //this.global.toaster(error);
                console.log(error);
            });
            
        
    }

    searchTerm() {
        if (this.q) {
            this.getOfficers(this.q);
        } else {
            this.getOfficers();
        }
    }

    valueMean(event, criteria) {
        console.log(event);
        console.log(criteria);
        //console.log(this.disiplin);
    }

    // go to result page
    doSearch() {
        this.nav.push(TripsPage);
    }

    // choose place
    choosePlace(from) {
        this.nav.push(SearchLocationPage, from);
    }

    // to go account page
    goToForm(id) {
        this.nav.push(FormRekapPage, id);
    }

    presentNotifications(myEvent) {
        console.log(myEvent);
        let popover = this.popoverCtrl.create(NotificationsPage);
        popover.present({
            ev: myEvent
        });
    }

}

//
