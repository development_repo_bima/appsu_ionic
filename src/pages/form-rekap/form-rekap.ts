import { Component } from "@angular/core";
import { NavController, NavParams, PopoverController } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { DataServices } from '../../services/data-services';

import { NotificationsPage } from "../notifications/notifications";
import { SettingsPage } from "../settings/settings";

import { DetailRekapPage } from "../detail-rekap/detail-rekap";
import { GlobalController } from "../../app/globalController";
import { TripsPage } from "../trips/trips";
import { SearchLocationPage } from "../search-location/search-location";


@Component({
    selector: 'page-form-rekap',
    templateUrl: 'form-rekap.html'
})

export class FormRekapPage {
    // search condition
    public search = {
        name: "Rio de Janeiro, Brazil",
        date: new Date().toISOString()
    }
    private arrUser;
    private kriteriaNilai;
    private title;
    private nama;
    private result;
    private formdata = {};
    private date_from;
    private date_to;
    constructor(private storage: Storage, public navParams: NavParams, public nav: NavController, public http: Http, public popoverCtrl: PopoverController, public data: DataServices, public global: GlobalController) {
        this.arrUser = this.data.dataUser();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        
    }

    ionViewWillEnter() {
        // this.search.pickup = "Rio de Janeiro, Brazil";
        // this.search.dropOff = "Same as pickup";
        this.storage.get('pickup').then((val) => {
            if (val === null) {
                this.search.name = "Rio de Janeiro, Brazil"
            } else {
                this.search.name = val;
            }
        }).catch((err) => {
            console.log(err)
        });
    }

    getKriteria() {
        var json;
        this.http.get('http://localhost/api-v1/request/get_kriteria')
            .map(res => {
                //this.global.loader();
                json = res.json();
                this.kriteriaNilai = json.data.data;
                this.result = json.data.result;
            })
            .subscribe(
            data => { },
            error => {
                var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
                this.global.toaster(text);
            });
    }

    getListOfficer() {
        
        if(this.date_from && this.date_to){
            if (localStorage.getItem('user_id') == '2'){
                var params = btoa(this.date_from+'|'+this.date_to+'|'+localStorage.getItem('user_id')).replace('=','');
            }else{
                var params = btoa(this.date_from+'|'+this.date_to+'|'+this.navParams.data).replace('=','');
            }
            this.nav.push(DetailRekapPage, params);
        }else{
            this.global.toaster("Please Choose date first");
        }
    }

    post_nilai() {
        if (typeof this.navParams.data === 'string') {
            var url = 'http://localhost/api-v1/request/post_nilai/' + this.navParams.data;
            var remoteurl = 'http://bms.uphero.com/api-v1/request/post_nilai/' + this.navParams.data;
            this.http.post(remoteurl, JSON.stringify(this.result))
                .timeout(3000)
                .map(data => {
                    this.global.loader();
                    var res = JSON.parse(data['_body']);

                })
                .subscribe(
                data => {

                },
                error => {
                    var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
                    this.global.toaster(text);
                });
        } else {
            this.global.toaster("error with id is null");
        }
    }

    // go to result page
    doSearch() {
        this.nav.push(TripsPage);
    }

    // choose place
    choosePlace(from) {
        this.nav.push(SearchLocationPage, from);
    }

    // to go account page
    goToAccount(id) {
        this.nav.push(SettingsPage, id);
    }

    presentNotifications(myEvent) {
        console.log(myEvent);
        let popover = this.popoverCtrl.create(NotificationsPage);
        popover.present({
            ev: myEvent
        });
    }

}

//
