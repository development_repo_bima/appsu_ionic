import { Component } from "@angular/core";
import { NavController, PopoverController } from "ionic-angular";
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { DataServices } from '../../services/data-services';
import { userPage } from "../../pages/user/user";
import { nilaiPage } from "../../pages/nilai/nilai";
import { DetailInfoPage } from "../../pages/detail-info/detail-info";
/* import { NotificationsPage } from "../notifications/notifications";
import { SettingsPage } from "../settings/settings"; */
import { GlobalController } from "../../app/globalController";
/* import { TripsPage } from "../trips/trips";
import { SearchLocationPage } from "../search-location/search-location"; */


@Component({
    selector: 'page-dashboard',
    templateUrl: 'dashboard.html'
})

export class DashboardPage {
    // search condition
    private role;
    private user;
    private nilai;
    private parseData = {};
    private officer;
    private title;
    private nama;
    constructor(private storage: Storage, public nav: NavController, public http: Http, public popoverCtrl: PopoverController, public data: DataServices, public global: GlobalController) {
        this.getStats();
        this.title = global.getComponentInitial();
        this.nama = localStorage.getItem('name');
        this.role = localStorage.getItem('role');
        var token = localStorage.getItem('token').split(".");
        this.parseData = JSON.parse(atob(token[1]));
        
    }

    getStats() {
        var json;
        var token = localStorage.getItem('token').split(".");
        var parseData = JSON.parse(atob(token[1]));
        var remoteurl = 'http://bms.uphero.com/api-v1/request/get_stats/'+ ((parseData.role == "2") ? parseData.user_id : '');
        console.log(parseData);
        this.http.get(remoteurl)
            .map(res => {
                //this.global.loader();
                json = res.json();
                console.log(this.role);
                if(this.role == 1){
                    this.user = json.data.user;
                    this.nilai = json.data.nilai;
                    this.officer = json.data.officer;
                }else if(this.role == 2){
                    this.nilai = json.data.nilai_officer.total;
                }
            })
            .subscribe(
            data => { },
            error => {
                var text = (!error.status)?'500 Internal Server Error' : 'Unexpected Error Found';
                this.global.toaster(text);
            });
    }

    goToUser(){
        localStorage.setItem('component', 'User');
        this.nav.setRoot(userPage);
    }

    goToNilai() {
        localStorage.setItem('component', 'Penilaian');
        this.nav.setRoot(nilaiPage);
    }

    goToNilaiOfMonth(){
        localStorage.setItem('component', 'Penilaian Bulan Ini');
        this.nav.setRoot(DetailInfoPage);
    }
}

//
